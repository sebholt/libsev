
#include <sev/assert.hpp>
#include <sev/mem/stack_fixed.hpp>
#include <sev/mem/stack_static.hpp>
#include <algorithm>
#include <array>
#include <cstdint>
#include <deque>
#include <iomanip>
#include <iostream>
#include <list>
#include <vector>

template class sev::mem::Stack_Fixed< std::uint32_t >;
template class sev::mem::Stack_Fixed< std::uint64_t >;
template class sev::mem::Stack_Static< std::uint32_t, 64 >;
template class sev::mem::Stack_Static< std::uint64_t, 64 >;

bool
test_static ( std::size_t bsize_n )
{
  std::cout << "test_static < " << bsize_n << " >\n";

  typedef sev::mem::Stack_Fixed< std::uint64_t > Stack;

  std::uint64_t value_in ( 0 );

  Stack stack_0 ( bsize_n );
  for ( std::size_t kk = 0; kk != bsize_n; ++kk ) {
    const std::size_t num ( kk + 1 );
    // Push
    for ( std::size_t ii = 0; ii != num; ++ii ) {
      ++value_in;
      stack_0.push_not_full ( value_in );
    }
    CASSERT ( stack_0.size () == num );
    // Pop
    for ( std::size_t ii = 0; ii != num; ++ii ) {
      CASSERT ( stack_0.top () == value_in );
      stack_0.pop_not_empty ();
      --value_in;
    }
  }

  return true;
}

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  std::vector< std::size_t > lengths (
      { 1, 7, 8, 9, 255, 256, 257, 511, 512, 513, 1023, 1024, 1025 } );

  for ( auto length : lengths ) {
    if ( !test_static ( length ) ) {
      return -1;
    }
  }

  return 0;
}
