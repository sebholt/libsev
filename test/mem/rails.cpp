
#include <sev/assert.hpp>
#include <sev/mem/rails/rails.hpp>
#include <iostream>
#include <vector>

void
iterate ( const sev::mem::rails::Rails & rails_n )
{
  // Forward iteration
  std::size_t count_forward = 0;
  std::size_t size_forward = 0;
  {
    auto it_cur = rails_n.begin ();
    auto it_end = rails_n.end ();
    for ( ; it_cur != it_end; ++it_cur ) {
      size_forward += it_cur.size ();
      ++count_forward;
    }
  }

  // Backward iteration
  std::size_t count_backward = 0;
  std::size_t size_backward = 0;
  {
    auto it_cur = rails_n.rbegin ();
    auto it_end = rails_n.rend ();
    for ( ; it_cur != it_end; ++it_cur ) {
      size_backward += it_cur.size ();
      ++count_backward;
    }
  }

  std::cout << "iterate:\n"
            << "  rails.size(): " << rails_n.size ()
            << " count_forward: " << count_forward << " count_backward "
            << count_backward << "\n";
  std::cout << "  size_forward: " << size_forward << " size_backward "
            << size_backward << "\n";

  // Test counts
  if ( count_forward != rails_n.size () ) {
    throw std::runtime_error (
        "Iterative forward count computation missmatch" );
  }
  if ( count_backward != rails_n.size () ) {
    throw std::runtime_error (
        "Iterative backward count computation missmatch" );
  }
  if ( count_forward != count_backward ) {
    throw std::runtime_error ( "Iterative count computation count missmatch" );
  }
  // Test sizes
  if ( size_forward != size_backward ) {
    throw std::runtime_error ( "Iterative size computation size missmatch" );
  }

  // Iterator increment / decrement
  const std::size_t size = rails_n.size ();
  if ( size != 0 ) {
    std::vector< std::size_t > indices;
    for ( std::size_t dist : { 0, 1, 2, 3 } ) {
      if ( size >= ( dist + 1 ) ) {
        indices.push_back ( dist );
        indices.push_back ( size - 1 - dist );
      }
    }
    std::cout << "iterator distances: ";
    for ( std::size_t ii : indices ) {
      std::cout << ii << ", ";
    }
    std::cout << "\n";
    for ( std::size_t ii : indices ) {
      auto fitb = rails_n.begin ();
      auto fite = rails_n.end ();
      auto ritb = rails_n.rbegin ();
      auto rite = rails_n.rend ();

      fitb += ii;
      fite -= ( size - ii );
      ritb += ( size - 1 - ii );
      rite -= ii + 1;

      if ( fitb.data () != fite.data () ) {
        throw std::runtime_error ( "Iterator fite data missmatch" );
      }
      if ( fitb.data () != ritb.data () ) {
        throw std::runtime_error ( "Iterator ritb data missmatch" );
      }
      if ( fitb.data () != rite.data () ) {
        throw std::runtime_error ( "Iterator rite data missmatch" );
      }
    }
  }
}

void
same_size_alloc ( std::size_t item_size_n,
                  std::size_t total_size_n,
                  bool allocate_front_n,
                  bool pop_front_n )
{
  std::cout << "same_size_alloc: item_size: " << item_size_n
            << " total_size: " << total_size_n
            << " allocate_front_n: " << allocate_front_n
            << " pop_front_n: " << pop_front_n << "\n";

  sev::mem::rails::Rails rails;

  // Fill
  std::size_t total_size = 0;
  while ( total_size < total_size_n ) {
    std::byte * data = nullptr;
    bool check_front = false;
    bool check_back = false;
    if ( allocate_front_n ) {
      data = rails.allocate_front ( item_size_n );
      check_front = true;
      check_back = ( rails.size () == 1 );
    } else {
      data = rails.allocate_back ( item_size_n );
      check_front = ( rails.size () == 1 );
      check_back = true;
    }
    total_size += item_size_n;

    // Checks
    if ( check_front ) {
      if ( data != rails.front () ) {
        throw std::runtime_error ( "Bad front()" );
      }
      if ( data != rails.front_view ().data () ) {
        throw std::runtime_error ( "Bad front_view()" );
      }
      if ( rails.front_view ().size () < item_size_n ) {
        throw std::runtime_error ( "Bad front allocation size" );
      }
    }
    if ( check_back ) {
      if ( data != rails.back () ) {
        throw std::runtime_error ( "Bad back()" );
      }
      if ( data != rails.back_view ().data () ) {
        throw std::runtime_error ( "Bad back_view()" );
      }
      if ( rails.back_view ().size () < item_size_n ) {
        throw std::runtime_error ( "Bad back allocation size" );
      }
    }
  }

  // Test iteration
  iterate ( rails );

  // Pop
  std::size_t num = rails.size ();
  while ( num != 0 ) {
    if ( pop_front_n ) {
      rails.pop_front ();
    } else {
      rails.pop_back ();
    }
    --num;
    if ( num != rails.size () ) {
      throw std::runtime_error ( "Size missmatch" );
    }
  }

  // Test iteration
  iterate ( rails );
}

void
pool_size ( std::size_t rail_capacity_n, std::size_t pool_capacity_n )
{
  std::cout << "pool_size: rail_capacity: " << rail_capacity_n
            << " pool_capacity: " << pool_capacity_n << "\n";

  sev::mem::rails::Rails rails;

  rails.set_rail_capacity ( rail_capacity_n );
  if ( rails.rail_capacity () < rail_capacity_n ) {
    throw std::runtime_error ( "Rail capacity too small" );
  }

  rails.set_pool_capacity ( pool_capacity_n );
  if ( rails.pool_capacity () != pool_capacity_n ) {
    throw std::runtime_error ( "Pool capacity setting failed" );
  }
  if ( rails.pool_size () != pool_capacity_n ) {
    throw std::runtime_error ( "Pool rail allocation failed" );
  }

  std::size_t rc2 = rail_capacity_n * 2 / 3;

  for ( bool alloc_back : { true, false } ) {
    for ( bool pop_back : { true, false } ) {
      for ( std::size_t num : { 1, 2, 3, 8, 32 } ) {
        // Allocate
        for ( std::size_t ii = 0; ii != num; ++ii ) {
          if ( alloc_back ) {
            rails.allocate_back ( rc2 );
          } else {
            rails.allocate_front ( rc2 );
          }
        }

        if ( rails.size () != num ) {
          throw std::runtime_error ( "Too few entries" );
        }
        if ( rails.rails () != num ) {
          throw std::runtime_error ( "Not enough active rails" );
        }
        if ( rails.pool_size () !=
             pool_capacity_n - std::min ( num, pool_capacity_n ) ) {
          throw std::runtime_error ( "Pool size error" );
        }

        // Pop
        for ( std::size_t ii = 0; ii != num; ++ii ) {
          if ( pop_back ) {
            rails.pop_back ();
          } else {
            rails.pop_front ();
          }
        }

        if ( rails.size () != 0 ) {
          throw std::runtime_error ( "Should be empty" );
        }
        if ( rails.rails () != 0 ) {
          throw std::runtime_error ( "Should be empty" );
        }
        if ( rails.pool_size () != pool_capacity_n ) {
          throw std::runtime_error ( "Pool rail allocation failed" );
        }
      }
    }
  }
}

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  {
    std::vector< std::size_t > total_sizes{ 0, 1, 1024 * 64 };

    std::size_t block_size = sizeof ( double );
    std::vector< std::size_t > item_sizes;
    item_sizes.push_back ( 1 );
    for ( std::size_t ii : { 1, 2, 3, 4, 5 } ) {
      item_sizes.push_back ( ii * block_size - 1 );
      item_sizes.push_back ( ii * block_size );
      item_sizes.push_back ( ii * block_size + 1 );
    }

    for ( auto total_size : total_sizes ) {
      for ( auto size : item_sizes ) {
        same_size_alloc ( size, total_size, true, true );
        same_size_alloc ( size, total_size, true, false );
        same_size_alloc ( size, total_size, false, true );
        same_size_alloc ( size, total_size, false, false );
      }
    }
  }

  {
    std::vector< std::size_t > rail_capa = {
        256, 1023, 1024, 1025, 1024 * 1024 };
    std::vector< std::size_t > pool_capa = { 0, 1, 2, 32 };
    for ( auto rc : rail_capa ) {
      for ( auto pc : pool_capa ) {
        pool_size ( rc, pc );
      }
    }
  }

  return 0;
}
