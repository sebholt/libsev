
#include <sev/assert.hpp>
#include <sev/mem/ring_fixed.hpp>
#include <sev/mem/ring_static.hpp>
#include <algorithm>
#include <cstdint>
#include <deque>
#include <functional>
#include <future>
#include <iomanip>
#include <iostream>
#include <list>
#include <vector>

template class sev::mem::Ring_Fixed< std::uint32_t >;
template class sev::mem::Ring_Fixed< std::uint64_t >;
template class sev::mem::Ring_Static< std::uint32_t, 64 >;
template class sev::mem::Ring_Static< std::uint64_t, 64 >;

template < unsigned int BSize >
bool
test_rb ()
{
  typedef sev::mem::Ring_Spans< std::uint64_t > Ring_Spans;
  typedef sev::mem::Ring_Static< std::uint64_t, BSize > Ring_Static;
  typedef sev::mem::Ring_Fixed< std::uint64_t > Ring_Fixed;

  std::cout << "test_rb < " << BSize << " >\n";

  Ring_Static ring_0;
  Ring_Fixed ring_1 ( BSize );

  std::uint64_t value_in ( 0 );
  std::uint64_t value_out ( 0 );
  for ( std::size_t kk = 0; kk != BSize; ++kk ) {
    for ( std::size_t tt = 0; tt != 100; ++tt ) {
      // Push
      for ( std::size_t ii = 0; ii != kk; ++ii ) {
        ring_1.push_back_not_full ( value_in );
        ++value_in;
      }
      for ( std::size_t ii = 0; ii != kk; ++ii ) {
        ring_0.push_back_not_full ( value_in );
        ++value_in;
      }
      // Pop
      for ( std::size_t ii = 0; ii != kk; ++ii ) {
        if ( ring_1.front () != value_out ) {
          return false;
        }
        ring_1.pop_front_not_empty ();
        ++value_out;
      }
      for ( std::size_t ii = 0; ii != kk; ++ii ) {
        if ( ring_0.front () != value_out ) {
          return false;
        }
        ring_0.pop_front_not_empty ();
        ++value_out;
      }

      // Push num items onto onto ring 0
      for ( std::size_t ii = 0; ii != kk; ++ii ) {
        ring_0.push_back_not_full ( value_in );
        ++value_in;
      }
      if ( ( ring_0.size () != kk ) || ( ring_1.size () != 0 ) ) {
        return false;
      }
      {
        Ring_Spans spans_src ( ring_0.spans () );
        Ring_Spans spans_dst ( ring_1.spans_free () );
        std::uint64_t num_trans = spans_src.move_items_to ( spans_dst );
        ring_0.decrement_size_count_front ( num_trans );
        ring_1.increment_size_count_back ( num_trans );
      }
      if ( ( ring_0.size () != 0 ) || ( ring_1.size () != kk ) ) {
        return false;
      }
      {
        Ring_Spans spans_src ( ring_1.spans () );
        Ring_Spans spans_dst ( ring_0.spans_free () );
        std::uint64_t num_trans = spans_src.move_items_to ( spans_dst );
        ring_1.decrement_size_count_front ( num_trans );
        ring_0.increment_size_count_back ( num_trans );
      }
      if ( ( ring_0.size () != kk ) || ( ring_1.size () != 0 ) ) {
        return false;
      }
      for ( std::size_t ii = 0; ii != kk; ++ii ) {
        if ( ring_0.front () != value_out ) {
          return false;
        }
        ring_0.pop_front_not_empty ();
        ++value_out;
      }
    }
  }

  return true;
}

template < unsigned int BSize >
bool
test_append ()
{
  std::cout << "test_append < " << BSize << " >\n";

  std::vector< unsigned int > data;
  for ( unsigned int ii = 0; ii != BSize; ++ii ) {
    data.push_back ( ii );
  }

  // -- Ring fixed
  for ( unsigned int offset = 0; offset != BSize; ++offset ) {
    {
      sev::mem::Ring_Fixed< unsigned int > ring ( BSize );
      ring.append_copy_not_full ( data.begin (), data.begin () );
      ring.append_copy_not_full ( data.begin () + offset, data.end () );
      ring.prepend_copy_not_full ( data.begin (), data.begin () + offset );
      ring.prepend_copy_not_full ( data.begin (), data.begin () );
      if ( ring.size () != BSize ) {
        return false;
      }
      for ( unsigned int ii = 0; ii != BSize; ++ii ) {
        if ( ring[ ii ] != ii ) {
          return false;
        }
      }
    }
  }

  // -- Ring static
  for ( unsigned int offset = 0; offset != BSize; ++offset ) {
    {
      sev::mem::Ring_Static< unsigned int, BSize > ring;
      ring.append_copy_not_full ( data.begin (), data.begin () );
      ring.append_copy_not_full ( data.begin () + offset, data.end () );
      ring.prepend_copy_not_full ( data.begin (), data.begin () + offset );
      ring.prepend_copy_not_full ( data.begin (), data.begin () );
      if ( ring.size () != BSize ) {
        return false;
      }
      for ( unsigned int ii = 0; ii != BSize; ++ii ) {
        if ( ring[ ii ] != ii ) {
          return false;
        }
      }
    }
  }

  return true;
}

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  typedef std::function< bool () > Func;
  std::vector< std::future< bool > > futures;
  std::vector< Func > funcs;

  funcs.push_back ( &test_append< 1 > );
  funcs.push_back ( &test_append< 2 > );
  funcs.push_back ( &test_append< 12 > );

  funcs.push_back ( &test_append< 1 > );

  funcs.push_back ( &test_rb< 1 > );
  funcs.push_back ( &test_rb< 7 > );
  funcs.push_back ( &test_rb< 8 > );
  funcs.push_back ( &test_rb< 9 > );

  funcs.push_back ( &test_rb< 255 > );
  funcs.push_back ( &test_rb< 256 > );
  funcs.push_back ( &test_rb< 257 > );

  funcs.push_back ( &test_rb< 511 > );
  funcs.push_back ( &test_rb< 512 > );
  funcs.push_back ( &test_rb< 513 > );

  for ( auto const & func : funcs ) {
    futures.push_back ( std::async ( std::launch::async, func ) );
  }

  for ( auto & future : futures ) {
    future.wait ();
    if ( !future.get () ) {
      return -1;
    }
  }
  return 0;
}
