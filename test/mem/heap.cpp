
#include <sev/assert.hpp>
#include <sev/mem/heap.hpp>
#include <algorithm>
#include <array>
#include <cstdint>
#include <deque>
#include <iomanip>
#include <iostream>
#include <list>
#include <vector>

template class sev::mem::Heap< std::uint32_t, void * >;
template class sev::mem::Heap< std::uint64_t, void * >;

template < class T >
void
test_heap ( T & heap_n )
{
  const std::size_t num ( 1024 );
  heap_n.ensure_minimum_capacity ( num );
  for ( std::size_t ii = 0; ii != num; ++ii ) {
    heap_n.insert_not_full ( typename T::Item ( ii, 0 ) );
  }
}

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  sev::mem::Heap< std::uint32_t, void * > heap32;
  sev::mem::Heap< std::uint64_t, void * > heap64;

  test_heap ( heap32 );
  test_heap ( heap64 );

  return 0;
}
