
#include <sev/assert.hpp>
#include <sev/json/composer.hpp>
#include <algorithm>
#include <cstdint>
#include <deque>
#include <iomanip>
#include <iostream>
#include <list>
#include <sstream>
#include <string>
#include <vector>

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  std::cout << "Empty composer" << std::endl;
  {
    sev::json::composer::Composer comp;
    std::cout << comp.string () << std::endl;
  }

  std::cout << "Empty object" << std::endl;
  {
    sev::json::composer::Composer comp;
    {
      sev::json::composer::Object obj ( comp );
    }
    std::cout << comp.string () << std::endl;
  }

  std::cout << "Single value" << std::endl;
  {
    sev::json::composer::Composer comp;
    {
      sev::json::composer::Object obj ( comp );
      obj.value ( "Zero", "Value of \"Zero\"" );
    }
    std::cout << comp.string () << std::endl;
  }

  std::cout << "Value types" << std::endl;
  {
    sev::json::composer::Composer comp;
    {
      sev::json::composer::Object obj ( comp );
      obj.value ( "String", "A wonderful day that is" );
      obj.value ( "Double", 3.1415 );
      obj.value ( "Bool", false );
      {
        sev::json::composer::Object obj2 ( obj, "Second object" );
        obj2.value ( "Color", "Green" );
      }
      obj.value ( "Bool2", true );
      {
        sev::json::composer::Object obj3 ( obj, "Third object" );
      }
      obj.value ( "Double2", 2.74 );
    }
    std::cout << comp.string () << std::endl;
  }

  return 0;
}
