
#include <sev/assert.hpp>
#include <sev/json/importer.hpp>
#include <sev/unicode/convert.hpp>
#include <algorithm>
#include <cstdint>
#include <deque>
#include <iomanip>
#include <iostream>
#include <list>
#include <sstream>
#include <string>
#include <vector>

using Json_List = std::vector< std::string >;

class Json_Imp : public sev::json::Importer
{
  // Protected methods
  protected:
  void
  ji_error ( const sev::json::importer::Error & error_n ) override;

  void
  ji_object_begin () override;

  void
  ji_object_end () override;

  void
  ji_array_begin () override;

  void
  ji_array_end () override;

  void
  ji_object_key_begin () override;

  void
  ji_object_key_data ( std::u32string_view utf32_n ) override;

  void
  ji_object_key_end () override;

  void
  ji_object_value_plain ( std::u32string_view utf32_n,
                          sev::json::Plain_Value value_n ) override;

  void
  ji_value_string_begin () override;

  void
  ji_value_string_data ( std::u32string_view utf32_n ) override;

  void
  ji_value_string_end () override;
};

void
Json_Imp::ji_error ( const sev::json::importer::Error & error_n )
{
  std::cout << "ji_error: Type=";
  std::cout << static_cast< std::uint32_t > ( error_n.type () );
  std::cout << "\n";
}

void
Json_Imp::ji_object_begin ()
{
  std::cout << "ji_object_begin\n";
}

void
Json_Imp::ji_object_end ()
{
  std::cout << "ji_object_end\n";
}

void
Json_Imp::ji_array_begin ()
{
  std::cout << "ji_array_begin\n";
}

void
Json_Imp::ji_array_end ()
{
  std::cout << "ji_array_end\n";
}

void
Json_Imp::ji_object_key_begin ()
{
  std::cout << "ji_key_begin\n";
}

void
Json_Imp::ji_object_key_data ( std::u32string_view utf32_n )
{
  std::cout << "ji_object_key_data: ";
  std::cout << sev::unicode::utf8 ( utf32_n );
  std::cout << "\n";
}

void
Json_Imp::ji_object_key_end ()
{
  std::cout << "ji_key_end\n";
}

void
Json_Imp::ji_object_value_plain ( std::u32string_view utf32_n,
                                  sev::json::Plain_Value value_n )
{
  std::cout << "ji_object_value_plain ";
  switch ( value_n.type () ) {
  case sev::json::Plain_Value::Type::T_NULL:
    std::cout << "null";
    break;
  case sev::json::Plain_Value::Type::T_BOOL:
    std::cout << ( value_n.as_bool () ? "true" : "false" );
    break;
  case sev::json::Plain_Value::Type::T_DOUBLE:
    std::cout << sev::unicode::utf8 ( utf32_n ) << " -> "
              << value_n.as_double ();
    break;
  }
  std::cout << "\n";
}

void
Json_Imp::ji_value_string_begin ()
{
  std::cout << "ji_value_string_begin\n";
}

void
Json_Imp::ji_value_string_data ( std::u32string_view utf32_n )
{
  std::cout << "ji_value_string_data: ";
  std::cout << sev::unicode::utf8 ( utf32_n );
  std::cout << "\n";
}

void
Json_Imp::ji_value_string_end ()
{
  std::cout << "ji_value_string_end\n";
}

void
test_import ( const std::string & json_n )
{
  std::cout << "Json: " << json_n << "\n";

  Json_Imp imp;
  std::u32string string32 = sev::unicode::utf32 ( json_n );

  imp.feed ( { &string32[ 0 ], string32.size () } );
  if ( imp.error ().is_valid () ) {
    std::terminate ();
  }
}

void
get_jsons ( Json_List & list_n )
{
  list_n.push_back ( "" );
  list_n.push_back ( "{}" );
  list_n.push_back ( "{ }" );
  list_n.push_back ( "[]" );
  list_n.push_back ( "[ ]" );
  list_n.push_back ( "{\"single\":\"value\"}" );
  list_n.push_back ( "{ \"single\":\"value\"}" );
  list_n.push_back ( "{ \"single\" :\"value\"}" );
  list_n.push_back ( "{ \"single\" : \"value\"}" );
  list_n.push_back ( "{ \"single\" : \"value\" }" );
  list_n.push_back ( "{ \"value_null\" : null }" );
  list_n.push_back ( "{ \"value_false\" : false }" );
  list_n.push_back ( "{ \"value_true\" : true }" );
  list_n.push_back ( "{ \"value_double_0\" : 0 }" );
  list_n.push_back ( "{ \"value_double_0.0\" : 0.0 }" );
  list_n.push_back ( "{ \"value_double_-1.0\" : -1.0 }" );
  list_n.push_back ( "{ \"value_double_-1.0e-2\" : -1.0e-2 }" );
  list_n.push_back ( "{ \"value_double_-1.0e-2\" : -1.0e-2 }" );

  list_n.push_back ( "{ \"value_object\" : {} }" );
  list_n.push_back ( "{ \"value_object2\" : { } }" );
  list_n.push_back ( "{ \"value_array\" : [] }" );
  list_n.push_back ( "{ \"value_array2\" : [] }" );

  list_n.push_back ( "{ \"v0\":[],\"v1\":[] }" );
  list_n.push_back ( "{ \"v0\":true,\"v1\":0.0,\"v2\":\"str\" }" );
  list_n.push_back ( "{ \"v0\":[],\"v1\":{},\"v2\":\"str\" }" );
  list_n.push_back ( "{ \"v0\": [], \"v1\" : {}, \"v2\" : \"str\" }" );

  list_n.push_back ( "[\"aval\",\"aval2\",\"aval3\"]" );
  list_n.push_back ( "[null,true,false,-3.1,3.2,3.3e2,\"val\"]" );
  list_n.push_back ( "[null, true, false, -3.1, 3.2, 3.3e2,\"val\"]" );
  list_n.push_back ( "{"
                     "	\"value_object_1\" : {"
                     "		\"value1\" : \"the first value\","
                     "		\"value2\" : \"the second value\""
                     "	},"
                     "	\"value_object_2\" : {"
                     "		\"value3\" : \"the third value\","
                     "		\"value4\" : \"the fourth value\""
                     "	}"
                     "}" );
  list_n.push_back ( "[\"valÄÖÜÐ\\u00d0€\\u20ac\"]" );
}

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  Json_List list;
  get_jsons ( list );

  for ( const std::string & json : list ) {
    test_import ( json );
  }

  return 0;
}
