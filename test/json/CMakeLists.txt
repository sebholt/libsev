
# --- Include directories

include_directories ( ${CMAKE_CURRENT_SOURCE_DIR} )

# --- Programs

# -- Json

add_executable ( importer importer.cpp )
target_link_libraries ( importer ${SEV_LIBRARIES} )

add_executable ( composer composer.cpp )
target_link_libraries ( composer ${SEV_LIBRARIES} )

