#include <sev/lag/vector3.hpp>
#include <iostream>
#include <utility>
#include <vector>

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  sev::lag::Vector3< float > v3_float;
  sev::lag::Vector3< double > v3_double;
  sev::lag::Vector3f v3f;
  sev::lag::Vector3d v3d;

  return 0;
}
