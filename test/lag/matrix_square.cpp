#include <sev/lag/matrix_square.hpp>
#include <sev/lag/vector.hpp>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

// -- Instantiation
template class sev::lag::Matrix_Square< float, 1 >;
template class sev::lag::Matrix_Square< float, 2 >;
template class sev::lag::Matrix_Square< float, 3 >;
template class sev::lag::Matrix_Square< float, 4 >;

template class sev::lag::Matrix_Square< double, 1 >;
template class sev::lag::Matrix_Square< double, 2 >;
template class sev::lag::Matrix_Square< double, 3 >;
template class sev::lag::Matrix_Square< double, 4 >;

template < std::size_t DIM >
void
test_features ()
{
  typedef sev::lag::Matrix_Square< double, DIM > Mat;

  std::cout << "--- Test features " << DIM << " ---\n";
  if ( std::is_trivial< Mat >::value ) {
    throw std::runtime_error ( "is_trivial" );
  }
  if ( !std::is_trivially_copyable< Mat >::value ) {
    throw std::runtime_error ( "!is_trivially_copyable" );
  }
  if ( !std::is_trivially_destructible< Mat >::value ) {
    throw std::runtime_error ( "!is_trivially_destructible" );
  }
  if ( !std::is_standard_layout< Mat >::value ) {
    throw std::runtime_error ( "!is_standard_layout" );
  }
  if ( !std::is_copy_constructible< Mat >::value ) {
    throw std::runtime_error ( "!is_copy_constructible" );
  }
  if ( !std::is_move_constructible< Mat >::value ) {
    throw std::runtime_error ( "!is_move_constructible" );
  }
}

template < std::size_t DIM >
bool
test_default_init ()
{
  typedef sev::lag::Vector< double, DIM > Mat;
  Mat mat_default;

  std::cout << "--- Test default init " << DIM << " ---\n";
  if ( mat_default != Mat ( sev::lag::init::zero ) ) {
    std::cerr << "mat_default " << mat_default << " is not zero initialized!\n";
    return false;
  }

  return true;
}

template < std::size_t DIM >
bool
test_fill_init ()
{
  typedef sev::lag::Matrix_Square< double, DIM > Mat;
  Mat mat_identity ( sev::lag::init::identity );
  Mat mat_zero ( sev::lag::init::zero );
  Mat mat_one ( sev::lag::init::one );
  Mat mat_fill_twelve ( sev::lag::init::Fill< double > ( 12.0 ) );

  std::cout << "--- Test fill init " << DIM << " ---\n";
  std::cout << "mat_identity " << mat_identity << "\n";
  std::cout << "mat_zero " << mat_zero << "\n";
  std::cout << "mat_one " << mat_one << "\n";
  std::cout << "mat_fill_twelve " << mat_fill_twelve << "\n";

  return true;
}

template < std::size_t DIM >
bool
test_array_init ()
{
  typedef sev::lag::Matrix_Square< double, DIM > Mat;

  double array[ DIM * DIM ];

  for ( unsigned int ii = 0; ii != DIM * DIM; ++ii ) {
    array[ ii ] = double ( ii );
  }

  Mat mat_array ( array );

  std::cout << "--- Test fill init " << DIM << " ---\n";
  std::cout << "mat_array " << mat_array << "\n";

  return true;
}

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  test_features< 1 > ();
  test_features< 2 > ();
  test_features< 3 > ();
  test_features< 4 > ();

  test_default_init< 1 > ();
  test_default_init< 2 > ();
  test_default_init< 3 > ();
  test_default_init< 4 > ();

  test_fill_init< 1 > ();
  test_fill_init< 2 > ();
  test_fill_init< 3 > ();
  test_fill_init< 4 > ();

  test_array_init< 1 > ();
  test_array_init< 2 > ();
  test_array_init< 3 > ();
  test_array_init< 4 > ();

  return 0;
}
