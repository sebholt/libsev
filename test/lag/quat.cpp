#include <sev/lag/quat.hpp>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

// -- Instantiation

template class sev::lag::Quat< float >;
template class sev::lag::Quat< double >;

// -- Tests

template < typename T >
void
test_features ()
{
  std::cout << "--- Test features ---\n";

  using Quat = sev::lag::Quat< T >;

  if ( std::is_trivial< Quat >::value ) {
    throw std::runtime_error ( "is_trivial" );
  }
  if ( !std::is_trivially_copyable< Quat >::value ) {
    throw std::runtime_error ( "!is_trivially_copyable" );
  }
  if ( !std::is_trivially_destructible< Quat >::value ) {
    throw std::runtime_error ( "!is_trivially_destructible" );
  }
  if ( !std::is_standard_layout< Quat >::value ) {
    throw std::runtime_error ( "!is_standard_layout" );
  }
  if ( !std::is_copy_constructible< Quat >::value ) {
    throw std::runtime_error ( "!is_copy_constructible" );
  }
  if ( !std::is_move_constructible< Quat >::value ) {
    throw std::runtime_error ( "!is_move_constructible" );
  }
}

template < typename T >
void
test_default_init ()
{
  std::cout << "--- Test default init ---\n";

  using Quat = sev::lag::Quat< T >;

  Quat quat_default;
  if ( quat_default != Quat ( sev::lag::init::zero ) ) {
    std::ostringstream ostr;
    ostr << "quat_default " << quat_default << " is not zero initialized!";
    throw std::runtime_error ( ostr.str () );
  }
}

template < typename T >
void
test_fill_init ()
{
  std::cout << "--- Test fill init ---\n";
  using Quat = sev::lag::Quat< T >;

  Quat quat_zero{ sev::lag::init::zero };
  Quat quat_one ( sev::lag::init::one );
  Quat quat_identity ( sev::lag::init::identity );
  constexpr T ival = static_cast< T > ( 12.0 );
  Quat quat_fill ( sev::lag::init::Fill< T >{ ival } );

  for ( std::size_t ii = 0; ii != 4; ++ii ) {
    if ( quat_zero[ ii ] != 0.0 ) {
      throw std::runtime_error ( "Bad zero init" );
    }
  }
  for ( std::size_t ii = 0; ii != 4; ++ii ) {
    if ( quat_one[ ii ] != 1.0 ) {
      throw std::runtime_error ( "Bad one init" );
    }
  }
  {
    if ( quat_identity[ 0 ] != T ( 1.0 ) ) {
      throw std::runtime_error ( "Bad identity init" );
    }
    for ( std::size_t ii = 1; ii != 4; ++ii ) {
      if ( quat_identity[ ii ] != T ( 0.0 ) ) {
        throw std::runtime_error ( "Bad identity init" );
      }
    }
  }
  for ( std::size_t ii = 0; ii != 4; ++ii ) {
    if ( quat_fill[ ii ] != ival ) {
      throw std::runtime_error ( "Bad fill init" );
    }
  }
}

template < typename T >
void
test_array_init ()
{
  std::cout << "--- Test array init ---\n";

  using Quat = sev::lag::Quat< T >;

  T plain_array[ 4 ];
  std::array< T, 4 > std_array;

  for ( std::size_t ii = 0; ii != 4; ++ii ) {
    plain_array[ ii ] = T ( ii );
    std_array[ ii ] = T ( ii );
  }

  {
    Quat quat_plain_array ( plain_array );
    for ( std::size_t ii = 0; ii != 4; ++ii ) {
      if ( quat_plain_array[ ii ] != plain_array[ ii ] ) {
        throw std::runtime_error ( "Bad plain array init" );
      }
    }
  }

  {
    Quat quat_std_array ( std_array );
    for ( std::size_t ii = 0; ii != 4; ++ii ) {
      if ( quat_std_array[ ii ] != std_array[ ii ] ) {
        throw std::runtime_error ( "Bad std::array init" );
      }
    }
  }
}

// -- Main

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  test_features< float > ();
  test_features< double > ();
  test_default_init< float > ();
  test_default_init< double > ();
  test_fill_init< float > ();
  test_fill_init< double > ();
  test_array_init< float > ();
  test_array_init< double > ();

  return 0;
}
