#include <sev/lag/vector2.hpp>
#include <iostream>
#include <utility>
#include <vector>

int
main ( int argc [[maybe_unused]], char * argv[] [[maybe_unused]] )
{
  sev::lag::Vector2< float > v2_float;
  sev::lag::Vector2< double > v2_double;
  sev::lag::Vector2f v2f;
  sev::lag::Vector2d v2d;

  return 0;
}
