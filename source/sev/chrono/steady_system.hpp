/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <chrono>

namespace sev::chrono
{

/** @brief Carries a std::steady_clock and std::system_clock time point
 *
 */
class Steady_System
{
  public:
  // -- Types

  using Clock_Steady = std::chrono::steady_clock;
  using Clock_System = std::chrono::system_clock;
  using Time_Steady = Clock_Steady::time_point;
  using Time_System = Clock_System::time_point;

  // -- Construction and setup

  /// @brief Time points are default constructed and is_valid() is false.
  Steady_System () = default;

  /// @brief Reset to default construction state
  void
  reset ();

  /// @brief Acquire steady and system times and set is_valid() to true.
  void
  now ();

  bool
  is_valid () const
  {
    return _is_valid;
  }

  // -- Steady time

  const Time_Steady &
  steady () const
  {
    return _steady;
  }

  // -- System time

  const Time_System &
  system () const
  {
    return _system;
  }

  // -- Comparison operators

  bool
  operator== ( const Steady_System & time_n ) const
  {
    return ( _is_valid == time_n._is_valid ) && ( _steady == time_n._steady ) &&
           ( _system == time_n._system );
  }

  bool
  operator!= ( const Steady_System & time_n ) const
  {
    return ( _is_valid != time_n._is_valid ) || ( _steady != time_n._steady ) ||
           ( _system != time_n._system );
  }

  private:
  // -- Attributes
  bool _is_valid = false;
  Time_Steady _steady;
  Time_System _system;
};

} // namespace sev::chrono
