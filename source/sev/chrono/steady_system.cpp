/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "steady_system.hpp"

namespace sev::chrono
{

void
Steady_System::reset ()
{
  _is_valid = false;
  _steady = Time_Steady ();
  _system = Time_System ();
}

void
Steady_System::now ()
{
  _is_valid = true;
  _steady = Clock_Steady::now ();
  _system = Clock_System::now ();
}

} // namespace sev::chrono
