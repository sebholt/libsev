/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstddef>
#include <string>

namespace sev
{

/// @brief Return the cache line size of cpu0
///
std::size_t
cache_line_size ();

/// @brief Reads strerror_r messages
std::string
strerror_string ( int error_code_n );

} // namespace sev
