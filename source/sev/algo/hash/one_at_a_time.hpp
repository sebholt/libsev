/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::algo::hash
{

/// @brief Feedable hash accumulator
///
template < typename Int_Type = std::uintptr_t >
class One_At_A_Time_Accu
{
  public:
  // -- Constructors

  One_At_A_Time_Accu ()
  : _accu ( 0 )
  {
  }

  // -- Interface

  /// @brief Reset to construction state
  void
  reset ()
  {
    _accu = 0;
  }

  /// @brief Push repeatedly
  void
  push ( Int_Type value_n )
  {
    _accu += value_n;
    _accu += ( _accu << 10 );
    _accu ^= ( _accu >> 6 );
  }

  /// @brief Finalizes the hash value and returns it
  /// @return The hash value
  Int_Type
  finish ()
  {
    _accu += ( _accu << 3 );
    _accu ^= ( _accu >> 11 );
    _accu += ( _accu << 15 );
    return _accu;
  }

  private:
  Int_Type _accu;
};

/// @brief Functor style hash generator class
///
template < typename Int_Type, class T >
class One_At_A_Time
{
  public:
  Int_Type
  operator() ( const T & container_n ) const;
};

template < typename Int_Type, class T >
Int_Type
One_At_A_Time< Int_Type, T >::operator() ( const T & container_n ) const
{
  Int_Type res ( 0 );
  for ( const auto & item : container_n ) {
    res += static_cast< Int_Type > ( item );
    res += ( res << 10 );
    res ^= ( res >> 6 );
  }
  res += ( res << 3 );
  res ^= ( res >> 11 );
  res += ( res << 15 );
  return res;
}

} // namespace sev::algo::hash
