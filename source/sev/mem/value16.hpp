/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstddef>
#include <cstdint>

namespace sev::mem
{

/// @brief 16 bit wide container for pod types
///
class Value16
{
  // Public methods
  public:
  /// @brief Leaves the value uninitialized
  ///
  Value16 () = default;

  explicit Value16 ( std::uint8_t val0_n, std::uint8_t val1_n )
  {
    // Static checks
    static_assert ( sizeof ( Value16 ) == 2, "Value16 must 2 bytes in size" );
    _value.vuint8[ 0 ] = val0_n;
    _value.vuint8[ 1 ] = val1_n;
  }

  explicit Value16 ( std::uint16_t val0_n ) { _value.vuint16 = val0_n; }

  std::uint8_t &
  ref_uint8 ( std::size_t index_n = 0 )
  {
    return _value.vuint8[ index_n ];
  }

  std::uint8_t
  get_uint8 ( std::size_t index_n = 0 ) const
  {
    return _value.vuint8[ index_n ];
  }

  void
  set_uint8 ( std::size_t index_n, std::uint8_t value_n )
  {
    _value.vuint8[ index_n ] = value_n;
  }

  std::uint16_t &
  ref_uint16 ()
  {
    return _value.vuint16;
  }

  std::uint16_t
  get_uint16 () const
  {
    return _value.vuint16;
  }

  void
  set_uint16 ( std::uint16_t value_n )
  {
    _value.vuint16 = value_n;
  }

  // -- Operators

  bool
  operator== ( const Value16 & value_n ) const
  {
    return ( _value.vuint16 == value_n._value.vuint16 );
  }

  bool
  operator!= ( const Value16 & value_n ) const
  {
    return ( _value.vuint16 != value_n._value.vuint16 );
  }

  // Private attributes
  public:
  union
  {
    std::uint8_t vuint8[ 2 ];
    std::uint16_t vuint16;
  } _value;
};

} // namespace sev::mem
