/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

namespace sev::mem::list::se
{

/// @brief Singly linked items base class
///
class Item
{
  public:
  // -- Constructors

  Item () = default;

  Item ( Item * link_n )
  : _link ( link_n )
  {
  }

  Item ( Item const & item_n ) = delete;
  Item ( Item && item_n ) = delete;

  ~Item () = default;

  // -- Link pointer

  bool
  links () const
  {
    return ( _link != nullptr );
  }

  Item *
  link () const
  {
    return _link;
  }

  // -- Comparison operators

  bool
  operator== ( Item const & item_n ) const
  {
    return ( _link == item_n._link );
  }

  bool
  operator!= ( Item const & item_n ) const
  {
    return ( _link != item_n._link );
  }

  private:
  // -- Attributes
  friend class Basic_List;
  Item * _link = nullptr;
};

} // namespace sev::mem::list::se
