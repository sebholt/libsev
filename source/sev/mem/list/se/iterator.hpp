/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>
#include <iterator>

namespace sev::mem::list::se
{

// -- Forward declaration
template < class T >
class List;

/// @brief Iterator class
///
template < class T, bool Const >
class Iterator
{
  // -- Types
  public:
  using item_type = typename std::conditional< Const, const Item, Item >::type;
  using value_type = typename std::conditional< Const, const T, T >::type;
  using pointer = value_type *;
  using reference = value_type &;
  using iterator_category = std::forward_iterator_tag;
  using difference_type = std::ptrdiff_t;

  // -- Construction

  private:
  friend class List< T >;

  Iterator ( item_type * item_n )
  : _item ( item_n )
  {
  }

  public:
  Iterator () = default;

  Iterator ( Iterator const & iter_n ) = default;

  ~Iterator () = default;

  // -- Dereferencing operators

  pointer
  operator-> ()
  {
    return static_cast< pointer > ( _item );
  }

  reference
  operator* ()
  {
    return *static_cast< pointer > ( _item );
  }

  // -- Incrementation operators

  Iterator &
  operator++ ()
  {
    _item = _item->link ();
    return *this;
  }

  Iterator
  operator++ ( int )
  {
    Iterator res ( *this );
    _item = _item->link ();
    return res;
  }

  Iterator &
  operator+= ( std::size_t increment_n )
  {
    for ( ; increment_n != 0; --increment_n ) {
      _item = _item->link ();
    }
    return *this;
  }

  // -- Comparison operators

  bool
  operator== ( Iterator const & iter_n ) const
  {
    return ( _item == iter_n._item );
  }

  bool
  operator!= ( Iterator const & iter_n ) const
  {
    return ( _item != iter_n._item );
  }

  private:
  // -- Attributes
  item_type * _item = nullptr;
};

} // namespace sev::mem::list::se
