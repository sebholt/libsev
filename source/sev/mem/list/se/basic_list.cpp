/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/mem/list/se/basic_list.hpp>
#include <sev/mem/list/se/item.hpp>

namespace sev::mem::list::se
{

std::size_t
Basic_List::count () const
{
  std::size_t res = 0;
  Item const * itc = _front.link ();
  Item const * end = &_back;
  while ( itc != end ) {
    itc = itc->link ();
    ++res;
  };
  return res;
}

void
Basic_List::swap ( Basic_List & list_n )
{
  std::swap ( _front._link, list_n._front._link );
  std::swap ( _back._link, list_n._back._link );
  if ( list_n.is_empty () ) {
    list_n.reset ();
  }
  if ( is_empty () ) {
    reset ();
  }
}

} // namespace sev::mem::list::se
