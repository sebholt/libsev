/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/mem/list/se/basic_list.hpp>
#include <sev/mem/list/se/iterator.hpp>
#include <cstdint>
#include <utility>

namespace sev::mem::list::se
{

/// @brief Wrapper for a singely linked list of classes that derive from Item.
///
template < class T >
class List : private Basic_List
{
  public:
  // -- Types

  using value_type = T;
  using iterator = Iterator< value_type, false >;
  using const_iterator = Iterator< value_type, true >;

  // -- Constructors

  List () = default;

  List ( const List< T > & list_n ) = delete;

  List ( List< T > && list_n )
  : Basic_List ( std::move ( list_n ) )
  {
  }

  ~List () = default;

  // -- Setup

  void
  clear ()
  {
    Basic_List::reset ();
  }

  // -- First event accessor

  T *
  front () const
  {
    return static_cast< T * > ( Basic_List::front () );
  }

  T *
  back () const
  {
    return static_cast< T * > ( Basic_List::back () );
  }

  bool
  is_empty () const
  {
    return Basic_List::is_empty ();
  }

  std::size_t
  count () const
  {
    return Basic_List::count ();
  }

  // -- Push

  void
  push_back ( T * item_n )
  {
    Basic_List::push_back ( item_n );
  }

  void
  push_front ( T * item_n )
  {
    Basic_List::push_front ( item_n );
  }

  // -- Pop

  T *
  pop_front_not_empty ()
  {
    return static_cast< T * > ( Basic_List::pop_front_not_empty () );
  }

  T *
  pop_front ()
  {
    if ( is_empty () ) {
      return nullptr;
    }
    return static_cast< T * > ( Basic_List::pop_front_not_empty () );
  }

  // -- Extract

  template < class UnaryPredicate >
  T *
  extract_if ( UnaryPredicate up_n )
  {
    Item * prev = &_front;
    Item * item = prev->link ();
    while ( item != &_back ) {
      T * citem = static_cast< T * > ( item );
      if ( up_n ( *citem ) ) {
        Basic_List::extract_item ( *prev, *item );
        return citem;
      }
      prev = item;
      item = item->link ();
    }
    return nullptr;
  }

  // -- Swap

  void
  swap ( List< T > & list_n )
  {
    Basic_List::swap ( list_n );
  }

  // -- Splice

  void
  splice_back_not_empty ( List< T > & src_n )
  {
    Basic_List::splice_back_not_empty ( src_n );
  }

  void
  splice_back ( List< T > & src_n )
  {
    Basic_List::splice_back ( src_n );
  }

  // -- Iteration

  iterator
  begin ()
  {
    return iterator ( _front.link () );
  }

  const_iterator
  begin () const
  {
    return const_iterator ( _front.link () );
  }

  const_iterator
  cbegin () const
  {
    return const_iterator ( _front.link () );
  }

  iterator
  end ()
  {
    return iterator ( &_back );
  }

  const_iterator
  end () const
  {
    return const_iterator ( &_back );
  }

  const_iterator
  cend () const
  {
    return const_iterator ( &_back );
  }

  // -- Subscript operators

  T *
  operator[] ( std::size_t index_n )
  {
    return static_cast< T * > ( Basic_List::operator[] ( index_n ) );
  }

  const T *
  operator[] ( std::size_t index_n ) const
  {
    return static_cast< const T * > ( Basic_List::operator[] ( index_n ) );
  }

  // -- Assignment

  void
  assign_move ( List< T > && list_n )
  {
    Basic_List::assign_move ( std::move ( list_n ) );
  }

  // -- Assignment operators

  Basic_List &
  operator= ( const List< T > & list_n ) = delete;

  Basic_List &
  operator= ( List< T > && list_n )
  {
    Basic_List::assign_move ( std::move ( list_n ) );
    return *this;
  }
};

} // namespace sev::mem::list::se
