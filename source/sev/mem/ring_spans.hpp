/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/math/numbers.hpp>
#include <sev/mem/ring_spans_read.hpp>
#include <algorithm>
#include <cstddef>
#include <iterator>
#include <new>

namespace sev::mem
{

/// @brief Tiles of a ring buffer
///
template < class T >
class Ring_Spans : public sev::mem::Ring_Spans_Read< T >
{
  public:
  // -- Types
  using typename sev::mem::Ring_Spans_Read< T >::Span;
  using typename sev::mem::Ring_Spans_Read< T >::Spans;

  // -- Constructors

  Ring_Spans () {}

  Ring_Spans ( Span span_a_n, Span span_b_n )
  : sev::mem::Ring_Spans_Read< T > ( span_a_n, span_b_n )
  {
  }

  Ring_Spans ( T * items_a_n,
               std::size_t size_a_n,
               T * items_b_n,
               std::size_t size_b_n )
  : sev::mem::Ring_Spans_Read< T > ( items_a_n, size_a_n, items_b_n, size_b_n )
  {
  }

  // -- Searching

  using sev::mem::Ring_Spans_Read< T >::index_of;

  // -- Span access

  using sev::mem::Ring_Spans_Read< T >::spans_size;
  using sev::mem::Ring_Spans_Read< T >::num_spans;
  using sev::mem::Ring_Spans_Read< T >::span;

  // -- Manipulation

  using sev::mem::Ring_Spans_Read< T >::clear;
  using sev::mem::Ring_Spans_Read< T >::drop_front;

  void
  fill ( const T & value_n );

  /// @return Number of items moved
  std::size_t
  move_items_to ( sev::mem::Ring_Spans< T > & spans_dst_n );

  /// @return Number of items moved
  std::size_t
  move_items_to ( Span & span_dst_n );

  /// @brief Trusts that there is enough space for all items
  /// @return Number of new items constructed
  template < class Input_Iter >
  std::size_t
  append_copy_not_full ( Input_Iter begin_n, Input_Iter end_n );

  /// @brief Trusts that there is enough space for all items
  /// @return Number of new items constructed
  template < class Input_Iter >
  std::size_t
  prepend_copy_not_full ( Input_Iter begin_n, Input_Iter end_n );

  /// @return +1 if front items were shifted forward,
  ///         -1 if back items were shifted backwards,
  ///         0 if the index was invalid
  int
  remove_at ( std::size_t index_n );

  /// @return +1 if front items were shifted forward,
  ///         -1 if back items were shifted backwards,
  ///         0 if the index was invalid
  int
  remove_first_match ( const T & item_match_n );

  /// @return +1 if front items were shifted forward,
  ///         -1 if back items were shifted backwards,
  ///         0 if the index was invalid
  int
  remove_last_match ( const T & item_match_n );

  /// @brief Replaces the first matching item with another value
  ///
  /// @arg match_item_n The item to find
  /// @arg replace_item_n The item to replace with
  /// @return True if an item was found and replaced
  bool
  replace_first_match ( const T & item_match_n, const T & item_replace_n );

  /// @brief Replaces all matching items with another value
  ///
  /// @arg match_item_n The item to find
  /// @arg replace_item_n The item to replace with
  /// @return The number of replacements
  std::size_t
  replace_all_matches ( const T & item_match_n, const T & item_replace_n );
};

/// @brief Assigns all items in the spans to the given value
///
/// @arg value_n The new items value
template < class T >
inline void
Ring_Spans< T >::fill ( const T & value_n )
{
  for ( auto & cspan : *this ) {
    for ( auto & item : cspan ) {
      item = value_n;
    }
  }
}

template < class T >
std::size_t
Ring_Spans< T >::move_items_to ( sev::mem::Ring_Spans< T > & spans_dst_n )
{
  std::size_t num_moved ( 0 );
  {
    Span * span_src_end ( &span ( 0 ) );
    Span * span_dst_end ( &spans_dst_n.span ( 0 ) );
    Span * span_src ( span_src_end );
    Span * span_dst ( span_dst_end );
    span_src_end += 2;
    span_dst_end += 2;
    std::size_t num_src ( span_src->size () );
    std::size_t num_dst ( span_dst->size () );
    T * itc_src ( span_src->begin () );
    T * itc_dst ( span_dst->begin () );

    while ( true ) {
      std::size_t num_move ( num_src );
      math::assign_smaller ( num_move, num_dst );
      if ( num_move != 0 ) {
        num_src -= num_move;
        num_dst -= num_move;
        num_moved += num_move;
        do {
          new ( itc_dst ) T ( std::move ( *itc_src ) );
          itc_src->~T ();
          ++itc_src;
          ++itc_dst;
          --num_move;
        } while ( num_move != 0 );

        if ( num_src == 0 ) {
          ++span_src;
          if ( span_src == span_src_end ) {
            break;
          }
          itc_src = span_src->begin ();
          num_src = span_src->size ();
        }
        if ( num_dst == 0 ) {
          ++span_dst;
          if ( span_dst == span_dst_end ) {
            break;
          }
          itc_dst = span_dst->begin ();
          num_dst = span_dst->size ();
        }
      } else {
        break;
      }
    }
  }
  return num_moved;
}

template < class T >
template < class Input_Iter >
std::size_t
Ring_Spans< T >::append_copy_not_full ( Input_Iter begin_n, Input_Iter end_n )
{
  const std::size_t size_total ( std::distance ( begin_n, end_n ) );
  std::size_t size_remain ( size_total );
  for ( Span & cspan : *this ) {
    const std::size_t ssize ( std::min ( cspan.size (), size_remain ) );
    if ( ssize != 0 ) {
      size_remain -= ssize;
      T * it_ins ( cspan.begin () );
      T * const it_end ( it_ins + ssize );
      for ( ; it_ins != it_end; ++it_ins, ++begin_n ) {
        new ( it_ins ) T ( *begin_n );
      }
    }
  }
  return size_total;
}

template < class T >
template < class Input_Iter >
std::size_t
Ring_Spans< T >::prepend_copy_not_full ( Input_Iter begin_n, Input_Iter end_n )
{
  const std::size_t size_total ( std::distance ( begin_n, end_n ) );
  std::size_t size_remain ( size_total );
  for ( Span * const cspan : { &span ( 1 ), &span ( 0 ) } ) {
    const std::size_t ssize ( std::min ( cspan->size (), size_remain ) );
    if ( ssize != 0 ) {
      Input_Iter it_in ( end_n - 1 );
      T * it_ins ( cspan->end () - 1 );
      T * const it_end ( it_ins - ssize );
      for ( ; it_ins != it_end; --it_ins, --it_in ) {
        new ( it_ins ) T ( *it_in );
      }
      end_n -= ssize;
      size_remain -= ssize;
    }
  }
  return size_total;
}

template < class T >
std::size_t
Ring_Spans< T >::move_items_to ( Span & span_dst_n )
{
  std::size_t num_moved ( 0 );
  {
    Span * span_src_end ( &span ( 0 ) );
    Span * span_src ( span_src_end );
    span_src_end += 2;
    std::size_t num_src ( span_src->size () );
    std::size_t num_dst ( span_dst_n.size () );
    T * itc_src ( span_src->begin () );
    T * itc_dst ( span_dst_n.begin () );

    while ( true ) {
      std::size_t num_move ( num_src );
      math::assign_smaller ( num_move, num_dst );
      if ( num_move != 0 ) {
        num_src -= num_move;
        num_dst -= num_move;
        num_moved += num_move;
        do {
          new ( itc_dst ) T ( std::move ( *itc_src ) );
          itc_src->~T ();
          ++itc_src;
          ++itc_dst;
          --num_move;
        } while ( num_move != 0 );
        if ( num_dst == 0 ) {
          break;
        }
        if ( num_src == 0 ) {
          ++span_src;
          if ( span_src == span_src_end ) {
            break;
          }
          itc_src = span_src->begin ();
          num_src = span_src->size ();
        }
      } else {
        break;
      }
    }
  }
  return num_moved;
}

template < class T >
int
Ring_Spans< T >::remove_at ( std::size_t index_n )
{
  {
    Span & cspan ( span ( 0 ) );
    if ( index_n < cspan.size () ) {
      T * it_begin ( cspan.begin () );
      T * it_src ( it_begin + index_n );
      // Move elements upward
      while ( it_src != it_begin ) {
        T * it_dst ( it_src );
        --it_src;
        *it_dst = std::move ( *it_src );
      }
      // Call first element destructor
      it_src->~T ();
      // Return move direction
      return 1;
    }
    // Subtract span size from search index
    index_n -= cspan.size ();
  }
  {
    Span & cspan ( span ( 1 ) );
    if ( index_n < cspan.size () ) {
      T * it_last ( cspan.end () );
      --it_last;
      T * it_src ( cspan.begin () + index_n );
      // Move elements downward
      while ( it_src != it_last ) {
        T * it_dst ( it_src );
        ++it_src;
        *it_dst = std::move ( *it_src );
      }
      // Call last element destructor
      it_src->~T ();
      // Return move direction
      return -1;
    }
  }
  // Index is too large
  return 0;
}

template < class T >
int
Ring_Spans< T >::remove_first_match ( const T & item_match_n )
{
  // First span
  {
    Span & cspan ( span ( 0 ) );
    T * it_begin ( cspan.begin () );
    T * it_end ( cspan.end () );
    T * it_src ( it_begin );
    for ( ; it_src != it_end; ++it_src ) {
      if ( *it_src == item_match_n ) {
        break;
      }
    }
    if ( it_src != it_end ) {
      // Move elements upward
      while ( it_src != it_begin ) {
        T * it_dst ( it_src );
        --it_src;
        *it_dst = std::move ( *it_src );
      }
      // Call first element destructor
      it_src->~T ();
      // Return move direction
      return 1;
    }
  }
  // Second span
  {
    Span & cspan ( span ( 1 ) );
    T * it_end ( cspan.end () );
    T * it_src ( cspan.begin () );
    for ( ; it_src != it_end; ++it_src ) {
      if ( *it_src == item_match_n ) {
        break;
      }
    }
    if ( it_src != it_end ) {
      // Move elements downward
      --it_end;
      while ( it_src != it_end ) {
        T * it_dst ( it_src );
        ++it_src;
        *it_dst = std::move ( *it_src );
      }
      // Call last element destructor
      it_src->~T ();
      // Return move direction
      return -1;
    }
  }
  // Element not found
  return 0;
}

template < class T >
int
Ring_Spans< T >::remove_last_match ( const T & item_match_n )
{
  // Second span
  {
    Span & cspan ( span ( 1 ) );
    T * it_end ( cspan.begin () );
    T * it_src ( cspan.end () );
    --it_end;
    --it_src;
    for ( ; it_src != it_end; --it_src ) {
      if ( *it_src == item_match_n ) {
        break;
      }
    }
    if ( it_src != it_end ) {
      T * it_last ( cspan.end () );
      --it_last;
      // Move elements downward
      while ( it_src != it_last ) {
        T * it_dst ( it_src );
        ++it_src;
        *it_dst = std::move ( *it_src );
      }
      // Call last element destructor
      it_src->~T ();
      // Return move direction
      return -1;
    }
  }
  // First span
  {
    Span & cspan ( span ( 0 ) );
    T * it_end ( cspan.begin () );
    T * it_src ( cspan.end () );
    --it_end;
    --it_src;
    for ( ; it_src != it_end; --it_src ) {
      if ( *it_src == item_match_n ) {
        break;
      }
    }
    if ( it_src != it_end ) {
      // Move elements upward
      ++it_end;
      while ( it_src != it_end ) {
        T * it_dst ( it_src );
        --it_src;
        *it_dst = std::move ( *it_src );
      }
      // Call first element destructor
      it_src->~T ();
      // Return move direction
      return 1;
    }
  }
  // Element not found
  return 0;
}

template < class T >
bool
Ring_Spans< T >::replace_first_match ( const T & item_match_n,
                                       const T & item_replace_n )
{
  bool found ( false );
  for ( auto & cspan : *this ) {
    for ( auto & item : cspan ) {
      if ( item == item_match_n ) {
        item = item_replace_n;
        found = true;
        break;
      }
    }
    if ( found ) {
      break;
    }
  }
  return found;
}

template < class T >
std::size_t
Ring_Spans< T >::replace_all_matches ( const T & item_match_n,
                                       const T & item_replace_n )
{
  std::size_t num_found ( 0 );
  for ( auto & cspan : *this ) {
    for ( auto & item : cspan ) {
      if ( item == item_match_n ) {
        item = item_replace_n;
        ++num_found;
      }
    }
  }
  return num_found;
}

} // namespace sev::mem
