/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::mem
{

/// @brief 64 bit wide container for pod types
///
class Number
{
  public:
  // -- Types
  enum class Type : std::uint8_t
  {
    I_8,
    I_16,
    I_32,
    I_64,
    UI_8,
    UI_16,
    UI_32,
    UI_64,
    F_S,
    F_D
  };

  public:
  // -- Constructors

  Number ()
  {
    _type = Type::UI_64;
    _value.ui_64 = 0;
  }

  Number ( std::uint8_t val_n )
  {
    _type = Type::UI_8;
    _value.ui_64 = val_n;
  }

  Number ( int8_t val_n )
  {
    _type = Type::I_8;
    _value.i_64 = val_n;
  }

  Number ( std::uint16_t val_n )
  {
    _type = Type::UI_16;
    _value.ui_64 = val_n;
  }

  Number ( int16_t val_n )
  {
    _type = Type::I_16;
    _value.i_64 = val_n;
  }

  Number ( std::uint32_t val_n )
  {
    _type = Type::UI_32;
    _value.ui_64 = val_n;
  }

  Number ( std::int32_t val_n )
  {
    _type = Type::I_32;
    _value.i_64 = val_n;
  }

  Number ( std::uint64_t val_n )
  {
    _type = Type::UI_64;
    _value.ui_64 = val_n;
  }

  Number ( std::int64_t val_n )
  {
    _type = Type::I_64;
    _value.i_64 = val_n;
  }

  Number ( float val_n )
  {
    _type = Type::F_S;
    _value.f_s = val_n;
  }

  Number ( double val_n )
  {
    _type = Type::F_D;
    _value.f_d = val_n;
  }

  // -- Type

  Type
  type () const
  {
    return _type;
  }

  bool
  is_signed_integer () const
  {
    return ( _type >= Type::I_8 ) && ( _type <= Type::I_64 );
  }

  bool
  is_unsigned_integer () const
  {
    return ( _type >= Type::UI_8 ) && ( _type <= Type::UI_64 );
  }

  bool
  is_floating () const
  {
    return ( _type == Type::F_S ) || ( _type == Type::F_D );
  }

  bool
  is_float () const
  {
    return ( _type == Type::F_S );
  }

  bool
  is_double () const
  {
    return ( _type == Type::F_D );
  }

  // -- Value access

  std::int64_t
  get_int64 () const
  {
    return _value.ui_64;
  }

  std::uint64_t
  get_uint64 () const
  {
    return _value.i_64;
  }

  float
  get_float () const
  {
    return _value.f_s;
  }

  double
  get_double () const
  {
    return _value.f_d;
  }

  // -- Operators

  Number &
  operator= ( const Number & number_n )
  {
    _type = number_n._type;
    _value.ui_64 = number_n._value.ui_64;
    return *this;
  }

  // Private attributes
  public:
  Type _type;
  union
  {
    std::int64_t i_64;
    std::uint64_t ui_64;
    float f_s;
    double f_d;
  } _value;
};

} // namespace sev::mem
