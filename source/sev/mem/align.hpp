/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstddef>

namespace sev::mem
{

/// @brief Default alignment block size
constexpr std::size_t align_block_size = 8;

// -- Align down

constexpr inline std::size_t
aligned_down ( std::size_t size_n, std::size_t block_size_n = align_block_size )
{
  return ( size_n / block_size_n ) * block_size_n;
}

template < typename T >
constexpr inline T *
aligned_down ( T * ptr_n, std::size_t block_size_n = align_block_size )
{
  static_assert ( sizeof ( T * ) <= sizeof ( std::size_t ) );
  return reinterpret_cast< T * > (
      aligned_down ( reinterpret_cast< std::size_t > ( ptr_n ) ) );
}

// -- Align up

constexpr inline std::size_t
aligned_up ( std::size_t size_n, std::size_t block_size_n = align_block_size )
{
  return ( ( size_n + ( block_size_n - 1 ) ) / block_size_n ) * block_size_n;
}

template < typename T >
constexpr inline T *
aligned_up ( T * ptr_n, std::size_t block_size_n = align_block_size )
{
  static_assert ( sizeof ( T * ) <= sizeof ( std::size_t ) );
  return reinterpret_cast< T * > (
      aligned_up ( reinterpret_cast< std::size_t > ( ptr_n ) ) );
}

// -- Is aligned

constexpr bool
is_aligned ( std::size_t size_n, std::size_t block_size_n = align_block_size )
{
  return ( size_n % block_size_n ) == 0;
}

template < typename T >
constexpr bool
is_aligned ( T * ptr_n, std::size_t block_size_n = align_block_size )
{
  static_assert ( sizeof ( T * ) <= sizeof ( std::size_t ) );
  return is_aligned ( reinterpret_cast< std::size_t > ( ptr_n ), block_size_n );
}

} // namespace sev::mem
