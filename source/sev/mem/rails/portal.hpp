/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::mem::rails
{

// -- Forward declaration
class Chunk;

/// @brief End portal of a chunk sequence in a rail
///
class Portal
{
  public:
  // -- Construction

  constexpr Portal () = default;

  constexpr Portal ( std::uintptr_t size_n, Chunk * chunk_next_n )
  : size_free ( size_n )
  , chunk_next ( chunk_next_n )
  {
    static_assert ( ( sizeof ( Portal ) == sizeof ( std::uintptr_t ) * 2 ),
                    "Portal bad size" );
  }

  public:
  // -- Attributes
  /// @brief Free size after/before the token
  std::uintptr_t size_free = 0;
  Chunk * chunk_next = nullptr;
};

} // namespace sev::mem::rails
