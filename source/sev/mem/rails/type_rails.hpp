/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/mem/rails/rails.hpp>
#include <sev/mem/rails/type_iterator.hpp>
#include <type_traits>
#include <utility>

namespace sev::mem::rails
{

/// @brief Rails wrapper for classes of a single type or derivatives of it
///
template < typename T >
class Type_Rails : private Rails
{
  public:
  // -- Types
  using value_type = T;
  using reference = value_type &;
  using const_reference = const value_type &;
  using iterator = Type_Iterator< value_type >;
  using const_iterator = Type_Const_Iterator< value_type >;
  using reverse_iterator = Type_Reverse_Iterator< value_type >;
  using const_reverse_iterator = Type_Const_Reverse_Iterator< value_type >;

  // -- Construction

  Type_Rails ();

  // -- Assignment

  void
  assign_copy ( const Type_Rails< T > & rails_n )
  {
    Rails::assign_copy ( rails_n );
  }

  void
  assign_move ( Type_Rails< T > && rails_n )
  {
    Rails::assign_copy ( std::move ( rails_n ) );
  }

  Type_Rails< T > &
  operator= ( const Type_Rails< T > & rails_n )
  {
    assign_copy ( rails_n );
    return *this;
  }

  Type_Rails< T > &
  operator= ( Type_Rails< T > && rails_n )
  {
    assign_move ( std::move ( rails_n ) );
    return *this;
  }

  // -- Setup

  using Rails::clear;

  // -- Rails capacity

  using Rails::rail_capacity;
  using Rails::set_rail_capacity;

  // -- Pool capacity

  using Rails::pool_capacity;
  using Rails::pool_size;
  using Rails::set_pool_capacity;

  // -- Statistics

  using Rails::empty;
  using Rails::is_empty;
  using Rails::rails;
  using Rails::rails_total;
  using Rails::size;

  // -- Front access

  reference
  front ()
  {
    return *reinterpret_cast< T * > ( Rails::front () );
  }

  const_reference
  front () const
  {
    return *reinterpret_cast< const T * > ( Rails::front () );
  }

  using Rails::pop_front;
  using Rails::push_front;

  template < typename P >
  P &
  push_front ( const P & item_n )
  {
    return emplace_front< P > ( item_n );
  }

  template < typename P, typename... Args >
  P &
  emplace_front ( Args &&... args_n )
  {
    static_assert ( std::is_base_of< T, P >::value );
    return Rails::emplace_front< P > ( std::forward< Args > ( args_n )... );
  }

  // -- Back access

  reference
  back ()
  {
    return *reinterpret_cast< T * > ( Rails::back () );
  }

  const_reference
  back () const
  {
    return *reinterpret_cast< const T * > ( Rails::back () );
  }

  using Rails::pop_back;
  using Rails::push_back;

  template < typename P >
  P &
  push_back ( const P & item_n )
  {
    return emplace_back< P > ( item_n );
  }

  template < typename P, typename... Args >
  P &
  emplace_back ( Args &&... args_n )
  {
    static_assert ( std::is_base_of< T, P >::value );
    return Rails::emplace_back< P > ( std::forward< Args > ( args_n )... );
  }

  // -- Iteration

  iterator
  begin ()
  {
    return iterator ( Rails::begin () );
  }

  const_iterator
  begin () const
  {
    return const_iterator ( Rails::begin () );
  }

  const_iterator
  cbegin () const
  {
    return const_iterator ( Rails::cbegin () );
  }

  iterator
  end ()
  {
    return iterator ( Rails::end () );
  }

  const_iterator
  end () const
  {
    return const_iterator ( Rails::end () );
  }

  const_iterator
  cend () const
  {
    return const_iterator ( Rails::cend () );
  }

  reverse_iterator
  rbegin ()
  {
    return reverse_iterator ( Rails::rbegin () );
  }

  const_reverse_iterator
  rbegin () const
  {
    return const_reverse_iterator ( Rails::rbegin () );
  }

  const_reverse_iterator
  crbegin () const
  {
    return const_reverse_iterator ( Rails::crbegin () );
  }

  reverse_iterator
  rend ()
  {
    return reverse_iterator ( Rails::rend () );
  }

  const_reverse_iterator
  rend () const
  {
    return const_reverse_iterator ( Rails::rend () );
  }

  const_reverse_iterator
  crend () const
  {
    return const_reverse_iterator ( Rails::crend () );
  }
};

template < typename T >
Type_Rails< T >::Type_Rails ()
{
  static_assert ( !std::is_polymorphic< T >::value );
}

} // namespace sev::mem::rails
