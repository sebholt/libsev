/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/mem/rails/chunk.hpp>
#include <sev/mem/view.hpp>
#include <cstddef>
#include <cstdint>
#include <iterator>
#include <type_traits>

namespace sev::mem::rails
{

// -- Forward declaration
class Rails;

/// @brief Forward iterator base class
///
template < bool CONST >
class Iterator_Forward_
{
  public:
  // -- Types
  using Iter = Iterator_Forward_< CONST >;
  using value_type =
      typename std::conditional< CONST, const std::byte, std::byte >::type;
  using chunk_type =
      typename std::conditional< CONST, const Chunk, Chunk >::type;
  using chunk_view = sev::mem::View< value_type >;
  using pointer = value_type *;
  using reference = value_type &;
  using iterator_category = std::bidirectional_iterator_tag;
  using difference_type = std::ptrdiff_t;

  // -- Construction

  /// @brief Constructs an invalid iterator
  Iterator_Forward_ () = default;

  private:
  friend class sev::mem::rails::Rails;

  Iterator_Forward_ ( chunk_type * chunk_n )
  : _chunk ( chunk_n )
  {
  }

  public:
  // -- Setup

  /// @brief Invalidates the iterator
  ///
  void
  reset ()
  {
    _chunk = nullptr;
  }

  // -- Chunk

  constexpr bool
  is_valid () const
  {
    return ( _chunk != nullptr );
  }

  constexpr chunk_type *
  chunk () const
  {
    return _chunk;
  }

  // -- Data access

  /// @brief Size of the data chunk
  std::uint32_t
  size () const
  {
    return _chunk->size_back;
  }

  /// @brief Pointer to the data chunk
  pointer
  data () const
  {
    return _chunk->data_back ();
  }

  chunk_view
  view () const
  {
    return chunk_view ( data (), size () );
  }

  // -- Cast operators

  /// @brief Pointer to the data chunk
  pointer
  operator-> () const
  {
    return data ();
  }

  chunk_view
  operator* () const
  {
    return chunk_view ( data (), size () );
  }

  // -- Increment operators

  Iter &
  operator++ ()
  {
    increment ();
    return *this;
  }

  Iter
  operator++ ( int )
  {
    Iter res ( *this );
    increment ();
    return res;
  }

  Iter &
  operator+= ( std::size_t delta_n )
  {
    increment ( delta_n );
    return *this;
  }

  // -- Decrement operators

  Iter &
  operator-- ()
  {
    decrement ();
    return *this;
  }

  Iter
  operator-- ( int )
  {
    Iter res ( *this );
    decrement ();
    return res;
  }

  Iter &
  operator-= ( std::size_t delta_n )
  {
    decrement ( delta_n );
    return *this;
  }

  // -- Comparison operators

  bool
  operator== ( const Iter & it_n ) const
  {
    return ( _chunk == it_n._chunk );
  }

  bool
  operator!= ( const Iter & it_n ) const
  {
    return ( _chunk != it_n._chunk );
  }

  // -- Utility

  void
  increment ();

  void
  increment ( std::size_t delta_n );

  void
  decrement ();

  void
  decrement ( std::size_t delta_n );

  protected:
  // -- Attributes
  chunk_type * _chunk = nullptr;
};

/// @brief Reverse iterator base class
///
template < bool CONST >
class Iterator_Reverse_
{
  public:
  // -- Types
  using Iter = Iterator_Reverse_< CONST >;
  using value_type =
      typename std::conditional< CONST, const std::byte, std::byte >::type;
  using chunk_type =
      typename std::conditional< CONST, const Chunk, Chunk >::type;
  using chunk_view = sev::mem::View< value_type >;
  using pointer = value_type *;
  using reference = value_type &;
  using iterator_category = std::bidirectional_iterator_tag;
  using difference_type = std::ptrdiff_t;

  // -- Construction

  /// @brief Constructs an invalid iterator
  Iterator_Reverse_ () = default;

  private:
  friend class sev::mem::rails::Rails;

  Iterator_Reverse_ ( chunk_type * chunk_n )
  : _chunk ( chunk_n )
  {
  }

  public:
  // -- Setup

  /// @brief Invalidates the iterator
  ///
  void
  reset ()
  {
    _chunk = nullptr;
  }

  // -- Chunk

  constexpr bool
  is_valid () const
  {
    return ( _chunk != nullptr );
  }

  constexpr chunk_type *
  chunk () const
  {
    return _chunk;
  }

  // -- Data access

  /// @brief Size of the data chunk
  std::uint32_t
  size () const
  {
    return _chunk->size_front;
  }

  /// @brief Pointer to the data chunk
  pointer
  data () const
  {
    return _chunk->data_front ();
  }

  chunk_view
  view () const
  {
    return chunk_view ( data (), size () );
  }

  // -- Cast operators

  /// @brief Pointer to the data chunk
  pointer
  operator-> () const
  {
    return data ();
  }

  chunk_view
  operator* () const
  {
    return chunk_view ( data (), size () );
  }

  // -- Increment operators

  Iter &
  operator++ ()
  {
    increment ();
    return *this;
  }

  Iter
  operator++ ( int )
  {
    Iter res ( *this );
    increment ();
    return res;
  }

  Iter &
  operator+= ( std::size_t delta_n )
  {
    increment ( delta_n );
    return *this;
  }

  // -- Decrement operators

  Iter &
  operator-- ()
  {
    decrement ();
    return *this;
  }

  Iter
  operator-- ( int )
  {
    Iter res ( *this );
    decrement ();
    return res;
  }

  Iter &
  operator-= ( std::size_t delta_n )
  {
    decrement ( delta_n );
    return *this;
  }

  // -- Comparison operators

  bool
  operator== ( const Iter & it_n ) const
  {
    return ( _chunk == it_n._chunk );
  }

  bool
  operator!= ( const Iter & it_n ) const
  {
    return ( _chunk != it_n._chunk );
  }

  // -- Utility

  void
  increment ();

  void
  increment ( std::size_t delta_n );

  void
  decrement ();

  void
  decrement ( std::size_t delta_n );

  protected:
  // -- Attributes
  chunk_type * _chunk = nullptr;
};

// -- Specializations

using Iterator = Iterator_Forward_< false >;
using Const_Iterator = Iterator_Forward_< true >;
using Reverse_Iterator = Iterator_Reverse_< false >;
using Const_Reverse_Iterator = Iterator_Reverse_< true >;

} // namespace sev::mem::rails
