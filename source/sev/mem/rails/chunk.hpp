/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/mem/rails/portal.hpp>
#include <cstddef>
#include <cstdint>

namespace sev::mem::rails
{

/// @brief Two sided type and length information header
///
/// Sizes are unaligned sizes from allocation requests.
///
/// Zero size signalizes that there is a Portal
/// right before/after the chunk.
class Chunk
{
  public:
  // -- Construction

  constexpr Chunk () = default;

  constexpr Chunk ( std::uint32_t prev_size_n, std::uint32_t next_size_n )
  : size_front ( prev_size_n )
  , size_back ( next_size_n )
  {
  }

  // -- Chunk pointers

  Chunk *
  chunk_prev ()
  {
    std::byte * res = reinterpret_cast< std::byte * > ( this );
    res -= size_front;
    res -= sizeof ( Chunk );
    return reinterpret_cast< Chunk * > ( res );
  }

  const Chunk *
  chunk_prev () const
  {
    const std::byte * res = reinterpret_cast< const std::byte * > ( this );
    res -= size_front;
    res -= sizeof ( Chunk );
    return reinterpret_cast< const Chunk * > ( res );
  }

  Chunk *
  chunk_next ()
  {
    std::byte * res = reinterpret_cast< std::byte * > ( this );
    res += size_back;
    res += sizeof ( Chunk );
    return reinterpret_cast< Chunk * > ( res );
  }

  const Chunk *
  chunk_next () const
  {
    const std::byte * res = reinterpret_cast< const std::byte * > ( this );
    res += size_back;
    res += sizeof ( Chunk );
    return reinterpret_cast< const Chunk * > ( res );
  }

  // -- Data pointers

  std::byte *
  data_front ()
  {
    std::byte * res = reinterpret_cast< std::byte * > ( this );
    res -= size_front;
    return res;
  }

  const std::byte *
  data_front () const
  {
    const std::byte * res = reinterpret_cast< const std::byte * > ( this );
    res -= size_front;
    return res;
  }

  std::byte *
  data_back ()
  {
    std::byte * res = reinterpret_cast< std::byte * > ( this );
    res += sizeof ( Chunk );
    return res;
  }

  const std::byte *
  data_back () const
  {
    const std::byte * res = reinterpret_cast< const std::byte * > ( this );
    res += sizeof ( Chunk );
    return res;
  }

  // -- End portal pointers

  Portal *
  portal_front ()
  {
    return reinterpret_cast< Portal * > (
        reinterpret_cast< std::byte * > ( this ) - sizeof ( Portal ) );
  }

  Portal *
  portal_back ()
  {
    return reinterpret_cast< Portal * > (
        reinterpret_cast< std::byte * > ( this ) + sizeof ( Chunk ) );
  }

  const Portal *
  portal_front () const
  {
    return reinterpret_cast< const Portal * > (
        reinterpret_cast< const std::byte * > ( this ) - sizeof ( Portal ) );
  }

  const Portal *
  portal_back () const
  {
    return reinterpret_cast< const Portal * > (
        reinterpret_cast< const std::byte * > ( this ) + sizeof ( Chunk ) );
  }

  public:
  // -- Attributes
  std::uint32_t size_front = 0;
  std::uint32_t size_back = 0;
};

} // namespace sev::mem::rails
