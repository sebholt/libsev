/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/assert.hpp>
#include <sev/mem/ring_spans.hpp>
#include <sev/mem/ring_spans_read.hpp>
#include <sev/mem/view.hpp>
#include <cstddef>
#include <memory>
#include <new>
#include <type_traits>
#include <utility>

namespace sev::mem
{

/// @brief Dynamically allocated ring buffer with a fixed size
///
template < class T >
class Ring_Fixed
{
  public:
  // -- Types

  using size_type = std::size_t;
  using Span = sev::mem::View< T >;
  using Spans = sev::mem::Ring_Spans< T >;
  using Span_Const = sev::mem::View< const T >;
  using Spans_Const = sev::mem::Ring_Spans_Read< const T >;

  // -- Constructors

  /// @brief Doesn't allocate anything
  Ring_Fixed ();

  /// @brief Allocates a chunk of memory
  Ring_Fixed ( size_type capacity_n );

  /// @brief Copy constructor
  ///
  Ring_Fixed ( const sev::mem::Ring_Fixed< T > & buffer_n );

  /// @brief Move constructor
  Ring_Fixed ( sev::mem::Ring_Fixed< T > && buffer_n );

  ~Ring_Fixed ();

  /// @brief Removes all items but does not change the capacity()
  ///
  void
  clear ();

  /// @brief Assigns all items to item_n
  ///
  void
  fill ( const T & item_n );

  // -- Capacity

  size_type
  capacity () const
  {
    return _capacity;
  }

  /// @brief Removes all items and sets the capacity to 0
  ///
  void
  clear_capacity ();

  /// @brief Changes the capacity
  ///
  void
  set_capacity ( size_type capacity_n );

  /// @brief Changes the capacity on demand
  ///
  void
  ensure_minimum_capacity ( size_type capacity_n );

  /// @brief Changes the capacity on demand
  ///
  void
  ensure_maximum_capacity ( size_type capacity_n );

  /// @brief Changes the capacity on demand
  ///
  void
  ensure_minimum_capacity_free ( size_type size_n );

  // -- Size

  size_type
  size () const
  {
    return _size;
  }

  size_type
  capacity_free () const
  {
    return ( _capacity - _size );
  }

  bool
  is_empty () const
  {
    return ( _size == 0 );
  }

  /// @brief Same as is_empty()
  bool
  empty () const
  {
    return ( _size == 0 );
  }

  bool
  is_full () const
  {
    return ( _size == _capacity );
  }

  // -- Item access

  T &
  item ( size_type index_n )
  {
    return _items[ ( _begin + index_n ) % _capacity ];
  }

  const T &
  item ( size_type index_n ) const
  {
    return _items[ ( _begin + index_n ) % _capacity ];
  }

  T &
  front ()
  {
    return _items[ _begin ];
  }

  const T &
  front () const
  {
    return _items[ _begin ];
  }

  T &
  back ()
  {
    return _items[ ( _begin + _size - 1 ) % _capacity ];
  }

  const T &
  back () const
  {
    return _items[ ( _begin + _size - 1 ) % _capacity ];
  }

  // -- Back pushing

  /// @brief Trusts in the buffer not being full
  void
  push_back_not_full ( const T & item_n );

  /// @brief Trusts in the buffer not being full
  void
  push_back_not_full ( T && item_n );

  /// @brief Trusts in the buffer not being full
  template < typename... Args >
  void
  emplace_back_not_full ( Args &&... args_n );

  /// @brief Trusts in the buffer to have enough capacity
  template < class Input_Iter >
  void
  append_copy_not_full ( Input_Iter begin_n, Input_Iter end_n );

  /// @brief Drops the first item if the buffer is full
  void
  push_back_dropping ( const T & item_n );

  /// @brief Drops the first item if the buffer is full
  void
  push_back_dropping ( T && item_n );

  /// @brief Drops the first item if the buffer is full
  template < typename... Args >
  void
  emplace_back_dropping ( Args &&... args_n );

  // -- Back popping

  /// @brief Trusts in the buffer not being empty
  void
  pop_back_not_empty ();

  /// @brief Trusts in the buffer not being empty
  void
  pop_back_not_empty ( T * output_n );

  // -- Front pushing

  /// @brief Trusts in the buffer not being full
  void
  push_front_not_full ( const T & item_n );

  /// @brief Trusts in the buffer not being full
  void
  push_front_not_full ( T && item_n );

  /// @brief Trusts in the buffer not being full
  template < typename... Args >
  void
  emplace_front_not_full ( Args &&... args_n );

  /// @brief Trusts in the buffer not being full
  template < class Input_Iter >
  void
  prepend_copy_not_full ( Input_Iter begin_n, Input_Iter end_n );

  /// @brief Drops the last item if the buffer is full
  void
  push_front_dropping ( const T & item_n );

  /// @brief Drops the last item if the buffer is full
  void
  push_front_dropping ( T && item_n );

  /// @brief Drops the last item if the buffer is full
  template < typename... Args >
  void
  emplace_front_dropping ( Args &&... args_n );

  // -- Front popping

  /// @brief Trusts in the buffer not being empty
  void
  pop_front_not_empty ();

  /// @brief Trusts in the buffer not being empty
  void
  pop_front_not_empty ( T * output_n );

  // -- Spans

  Span
  span_front ();

  Span
  span_back ();

  Span_Const
  span_front () const;

  Span_Const
  span_back () const;

  Span_Const
  span_const_front () const;

  Span_Const
  span_const_back () const;

  Spans
  spans ();

  Spans_Const
  spans () const;

  Spans_Const
  spans_const () const;

  Spans
  spans_free ();

  // -- Size count manipulation

  void
  increment_size_count_front ( size_type delta_n );

  void
  increment_size_count_back ( size_type delta_n );

  void
  decrement_size_count_front ( size_type delta_n );

  void
  decrement_size_count_back ( size_type delta_n );

  size_type
  index_of ( const T & match_value_n ) const;

  size_type
  index_of ( const T & match_value_n, size_type begin_index_n ) const;

  void
  remove_at ( size_type index_n );

  /// @return True if an item was removed
  bool
  remove_first_match ( const T & match_value_n );

  /// @return True if an item was removed
  bool
  remove_last_match ( const T & match_value_n );

  /// @return Number of items removed
  size_type
  remove_all_matches ( const T & match_value_n );

  // -- Subscript operators

  T &
  operator[] ( size_type index_n )
  {
    return item ( index_n );
  }

  const T &
  operator[] ( size_type index_n ) const
  {
    return item ( index_n );
  }

  // -- Assignment operators

  sev::mem::Ring_Fixed< T > &
  operator= ( const sev::mem::Ring_Fixed< T > & buffer_n )
  {
    copy_assign ( buffer_n );
    return *this;
  }

  sev::mem::Ring_Fixed< T > &
  operator= ( sev::mem::Ring_Fixed< T > && buffer_n )
  {
    move_assign ( std::move ( buffer_n ) );
    return *this;
  }

  protected:
  // -- Utility

  void
  copy_assign ( const sev::mem::Ring_Fixed< T > & buffer_n );

  void
  move_assign ( sev::mem::Ring_Fixed< T > && buffer_n );

  private:
  T *
  allocate_num_items ( size_type size_n );

  void
  deallocate_num_items ( T * items_n, size_type size_n );

  T *
  alloc_front_not_full ();

  void
  destruct_reverse ();

  template < bool TD = std::is_trivially_destructible< T >::value >
  void
  destruct ( T & item_n )
  {
    if ( !TD ) {
      item_n.~T ();
    }
  }

  private:
  // -- Attributes
  T * _items = nullptr;
  size_type _begin = 0;
  size_type _capacity = 0;
  size_type _size = 0;
  std::allocator< T > _allocator;
};

template < class T >
Ring_Fixed< T >::Ring_Fixed ()
{
}

template < class T >
Ring_Fixed< T >::Ring_Fixed ( size_type capacity_n )
{
  if ( capacity_n == 0 ) {
    return;
  }

  _items = allocate_num_items ( capacity_n );
  _capacity = capacity_n;
}

template < class T >
Ring_Fixed< T >::Ring_Fixed ( const sev::mem::Ring_Fixed< T > & buffer_n )
{
  // Allocate capacity
  const size_type capa_new = buffer_n.capacity ();
  if ( capa_new == 0 ) {
    return;
  }
  _items = allocate_num_items ( capa_new );
  _capacity = capa_new;
  _size = buffer_n.size ();

  // Copy new items
  if ( _size == 0 ) {
    return;
  }
  Spans_Const cspans = buffer_n.spans_const ();
  T * it_dst ( &_items[ 0 ] );
  for ( auto & cspan : cspans ) {
    const T * it_src_end ( cspan.end () );
    const T * it_src_cur ( cspan.begin () );
    while ( it_src_cur != it_src_end ) {
      new ( it_dst ) T ( *it_src_cur );
      ++it_dst;
      ++it_src_cur;
    }
  }
}

template < class T >
Ring_Fixed< T >::Ring_Fixed ( sev::mem::Ring_Fixed< T > && buffer_n )
: _items ( buffer_n._items )
, _begin ( buffer_n._begin )
, _capacity ( buffer_n._capacity )
, _size ( buffer_n._size )
, _allocator ( std::move ( buffer_n._allocator ) )
{
  buffer_n._items = nullptr;
  buffer_n._begin = 0;
  buffer_n._capacity = 0;
  buffer_n._size = 0;
}

template < class T >
Ring_Fixed< T >::~Ring_Fixed ()
{
  clear_capacity ();
}

template < class T >
void
Ring_Fixed< T >::copy_assign ( const sev::mem::Ring_Fixed< T > & buffer_n )
{
  clear ();
  const size_type capa_new ( buffer_n.capacity () );
  // Match capacities
  if ( capa_new != _capacity ) {
    // Clear old capacity
    clear_capacity ();
    // Allocate new capacity
    if ( capa_new != 0 ) {
      _items = allocate_num_items ( capa_new );
      _capacity = capa_new;
    }
  }
  // Copy items
  if ( ( capa_new != 0 ) && ( _capacity != 0 ) ) {
    _size = buffer_n.size ();
    // Copy new items
    if ( _size != 0 ) {
      Spans_Const cspans ( buffer_n.spans_const () );
      T * it_dst ( &_items[ 0 ] );
      for ( auto & cspan : cspans ) {
        const T * it_src_end ( cspan.end () );
        const T * it_src_cur ( cspan.begin () );
        while ( it_src_cur != it_src_end ) {
          new ( it_dst ) T ( *it_src_cur );
          ++it_dst;
          ++it_src_cur;
        }
      }
    }
  }
}

template < class T >
void
Ring_Fixed< T >::move_assign ( sev::mem::Ring_Fixed< T > && buffer_n )
{
  clear_capacity ();
  // Move pointers and size
  {
    _items = buffer_n._items;
    _begin = buffer_n._begin;
    _capacity = buffer_n._capacity;
    _size = buffer_n._size;
    _allocator = std::move ( buffer_n._allocator );
  }
  // Clear source instance
  {
    buffer_n._items = nullptr;
    buffer_n._begin = 0;
    buffer_n._capacity = 0;
    buffer_n._size = 0;
  }
}

template < class T >
inline void
Ring_Fixed< T >::clear ()
{
  if ( !std::is_trivially_destructible< T >::value ) {
    destruct_reverse ();
  }
  _begin = 0;
  _size = 0;
}

template < class T >
void
Ring_Fixed< T >::destruct_reverse ()
{
  if ( _size == 0 ) {
    return;
  }

  const Spans cspans = spans ();
  for ( const Span * cspan : { &cspans[ 1 ], &cspans[ 0 ] } ) {
    const typename Span::const_iterator it_begin ( cspan->begin () );
    typename Span::const_iterator it ( cspan->end () );
    while ( it != it_begin ) {
      --it;
      it->~T ();
    }
  }
}

template < class T >
void
Ring_Fixed< T >::fill ( const T & value_n )
{
  spans ().fill ( value_n );
}

template < class T >
inline T *
Ring_Fixed< T >::allocate_num_items ( size_type size_n )
{
  DEBUG_ASSERT ( size_n != 0 );
  T * items_new = _allocator.allocate ( size_n );
  return items_new;
}

template < class T >
inline void
Ring_Fixed< T >::deallocate_num_items ( T * items_n, size_type size_n )
{
  DEBUG_ASSERT ( items_n != nullptr );
  DEBUG_ASSERT ( size_n != 0 );
  _allocator.deallocate ( items_n, size_n );
}

template < class T >
void
Ring_Fixed< T >::clear_capacity ()
{
  if ( _items == nullptr ) {
    return;
  }

  clear ();
  DEBUG_ASSERT ( _capacity != 0 );
  deallocate_num_items ( _items, _capacity );
  _items = nullptr;
  _begin = 0;
  _capacity = 0;
  _size = 0;
}

template < class T >
void
Ring_Fixed< T >::set_capacity ( size_type capacity_n )
{
  if ( capacity () == capacity_n ) {
    return;
  }
  if ( capacity_n == 0 ) {
    clear_capacity ();
    return;
  }

  // Allocate new chunk
  T * items_new = allocate_num_items ( capacity_n );

  const size_type num_copy = std::min ( size (), capacity_n );

  // Move items
  for ( size_type ii = 0; ii != num_copy; ++ii ) {
    T & old_item = item ( ii );
    new ( items_new + ii ) T ( std::move ( old_item ) );
    destruct ( old_item );
  }
  // Destroy overcounting items
  if ( !std::is_trivially_destructible< T >::value ) {
    for ( size_type ii = num_copy; ii != size (); ++ii ) {
      item ( ii ).~T ();
    }
  }

  // Free old chunk
  if ( _items != nullptr ) {
    deallocate_num_items ( _items, _capacity );
  }
  _items = items_new;
  _begin = 0;
  _capacity = capacity_n;
  _size = num_copy;
}

template < class T >
inline void
Ring_Fixed< T >::ensure_minimum_capacity ( size_type capacity_n )
{
  if ( capacity () < capacity_n ) {
    set_capacity ( capacity_n );
  }
}

template < class T >
inline void
Ring_Fixed< T >::ensure_maximum_capacity ( size_type capacity_n )
{
  if ( capacity () > capacity_n ) {
    set_capacity ( capacity_n );
  }
}

template < class T >
inline void
Ring_Fixed< T >::ensure_minimum_capacity_free ( size_type size_n )
{
  if ( capacity_free () < size_n ) {
    set_capacity ( size () + size_n );
  }
}

template < class T >
inline void
Ring_Fixed< T >::push_back_not_full ( const T & item_n )
{
  new ( ( _items + ( ( _begin + _size ) % _capacity ) ) ) T ( item_n );
  ++_size;
}

template < class T >
inline void
Ring_Fixed< T >::push_back_not_full ( T && item_n )
{
  new ( ( _items + ( ( _begin + _size ) % _capacity ) ) )
      T ( std::move ( item_n ) );
  ++_size;
}

template < class T >
template < typename... Args >
inline void
Ring_Fixed< T >::emplace_back_not_full ( Args &&... args_n )
{
  new ( ( _items + ( ( _begin + _size ) % _capacity ) ) )
      T ( std::forward< Args > ( args_n )... );
  ++_size;
}

template < class T >
template < class Input_Iter >
void
Ring_Fixed< T >::append_copy_not_full ( Input_Iter begin_n, Input_Iter end_n )
{
  increment_size_count_back (
      spans_free ().append_copy_not_full ( begin_n, end_n ) );
}

template < class T >
inline void
Ring_Fixed< T >::push_back_dropping ( const T & item_n )
{
  if ( is_full () ) {
    pop_front_not_empty ();
  }
  push_back_not_full ( item_n );
}

template < class T >
inline void
Ring_Fixed< T >::push_back_dropping ( T && item_n )
{
  if ( is_full () ) {
    pop_front_not_empty ();
  }
  push_back_not_full ( std::move ( item_n ) );
}

template < class T >
template < typename... Args >
inline void
Ring_Fixed< T >::emplace_back_dropping ( Args &&... args_n )
{
  if ( is_full () ) {
    pop_front_not_empty ();
  }
  emplace_back_not_full ( std::forward< Args > ( args_n )... );
}

template < class T >
inline void
Ring_Fixed< T >::pop_back_not_empty ()
{
  destruct ( back () );
  --_size;
}

template < class T >
inline void
Ring_Fixed< T >::pop_back_not_empty ( T * output_n )
{
  T & bitem = back ();
  *output_n = std::move ( bitem );
  destruct ( bitem );
  --_size;
}

template < class T >
inline T *
Ring_Fixed< T >::alloc_front_not_full ()
{
  DEBUG_ASSERT ( !is_full () );
  ++_size;
  {
    // Decrement the begin index with bottom dropping
    _begin += _capacity;
    --_begin;
    _begin %= _capacity;
  }
  return &front ();
}

template < class T >
inline void
Ring_Fixed< T >::push_front_not_full ( const T & item_n )
{
  new ( alloc_front_not_full () ) T ( item_n );
}

template < class T >
inline void
Ring_Fixed< T >::push_front_not_full ( T && item_n )
{
  new ( alloc_front_not_full () ) T ( std::move ( item_n ) );
}

template < class T >
template < typename... Args >
inline void
Ring_Fixed< T >::emplace_front_not_full ( Args &&... args_n )
{
  new ( alloc_front_not_full () ) T ( std::forward< Args > ( args_n )... );
}

template < class T >
template < class Input_Iter >
void
Ring_Fixed< T >::prepend_copy_not_full ( Input_Iter begin_n, Input_Iter end_n )
{
  increment_size_count_front (
      spans_free ().prepend_copy_not_full ( begin_n, end_n ) );
}

template < class T >
inline void
Ring_Fixed< T >::push_front_dropping ( const T & item_n )
{
  if ( is_full () ) {
    pop_back_not_empty ();
  }
  push_front_not_full ( item_n );
}

template < class T >
inline void
Ring_Fixed< T >::push_front_dropping ( T && item_n )
{
  if ( is_full () ) {
    pop_back_not_empty ();
  }
  push_front_not_full ( std::move ( item_n ) );
}

template < class T >
template < typename... Args >
inline void
Ring_Fixed< T >::emplace_front_dropping ( Args &&... args_n )
{
  if ( is_full () ) {
    pop_back_not_empty ();
  }
  emplace_front_not_full ( std::forward< Args > ( args_n )... );
}

template < class T >
inline void
Ring_Fixed< T >::pop_front_not_empty ()
{
  destruct ( front () );
  {
    // Increment begin index with end dropping
    ++_begin;
    _begin %= _capacity;
  }
  --_size;
}

template < class T >
inline void
Ring_Fixed< T >::pop_front_not_empty ( T * output_n )
{
  T & fitem = front ();
  *output_n = std::move ( fitem );
  destruct ( fitem );

  {
    // Increment begin index with end dropping
    ++_begin;
    _begin %= _capacity;
  }
  --_size;
}

template < class T >
inline typename Ring_Fixed< T >::Span
Ring_Fixed< T >::span_front ()
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Span ( &front (), tsize );
}

template < class T >
inline typename Ring_Fixed< T >::Span
Ring_Fixed< T >::span_back ()
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Span ( &item ( tsize ), size () - tsize );
}

template < class T >
inline typename Ring_Fixed< T >::Span_Const
Ring_Fixed< T >::span_front () const
{
  return span_const_front ();
}

template < class T >
inline typename Ring_Fixed< T >::Span_Const
Ring_Fixed< T >::span_back () const
{
  return span_const_back ();
}

template < class T >
inline typename Ring_Fixed< T >::Span_Const
Ring_Fixed< T >::span_const_front () const
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Span_Const ( &front (), tsize );
}

template < class T >
inline typename Ring_Fixed< T >::Span_Const
Ring_Fixed< T >::span_const_back () const
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Span_Const ( &item ( tsize ), size () - tsize );
}

template < class T >
inline typename Ring_Fixed< T >::Spans
Ring_Fixed< T >::spans ()
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Spans ( &front (), tsize, &item ( tsize ), size () - tsize );
}

template < class T >
inline typename Ring_Fixed< T >::Spans_Const
Ring_Fixed< T >::spans () const
{
  return spans_const ();
}

template < class T >
inline typename Ring_Fixed< T >::Spans_Const
Ring_Fixed< T >::spans_const () const
{
  const size_type tsize ( std::min ( ( capacity () - _begin ), size () ) );
  return Spans_Const ( &front (), tsize, &item ( tsize ), size () - tsize );
}

template < class T >
inline typename Ring_Fixed< T >::Spans
Ring_Fixed< T >::spans_free ()
{
  const size_type sfree ( capacity_free () );
  const size_type index_begin ( ( _begin + size () ) % capacity () );
  const size_type tsize ( std::min ( ( capacity () - index_begin ), sfree ) );
  return Spans ( &_items[ index_begin ],
                 tsize,
                 &_items[ ( index_begin + tsize ) % capacity () ],
                 ( sfree - tsize ) );
}

template < class T >
inline void
Ring_Fixed< T >::increment_size_count_front ( size_type delta_n )
{
  _size += delta_n;
  DEBUG_ASSERT ( _size <= capacity () );
  // Decrement the begin index with bottom looping
  _begin += capacity ();
  _begin -= delta_n;
  _begin %= capacity ();
}

template < class T >
inline void
Ring_Fixed< T >::increment_size_count_back ( size_type delta_n )
{
  _size += delta_n;
  DEBUG_ASSERT ( _size <= capacity () );
}

template < class T >
inline void
Ring_Fixed< T >::decrement_size_count_front ( size_type delta_n )
{
  DEBUG_ASSERT ( delta_n <= _size );
  DEBUG_ASSERT ( capacity () != 0 );
  _size -= delta_n;
  // Decrement the begin index with bottom looping
  _begin += delta_n;
  _begin %= capacity ();
}

template < class T >
inline void
Ring_Fixed< T >::decrement_size_count_back ( size_type delta_n )
{
  DEBUG_ASSERT ( delta_n <= _size );
  _size -= delta_n;
}

template < class T >
typename Ring_Fixed< T >::size_type
Ring_Fixed< T >::index_of ( const T & match_value_n ) const
{
  return spans_const ().index_of ( match_value_n );
}

template < class T >
typename Ring_Fixed< T >::size_type
Ring_Fixed< T >::index_of ( const T & match_value_n,
                            size_type begin_index_n ) const
{
  return spans_const ().index_of ( match_value_n, begin_index_n );
}

template < class T >
void
Ring_Fixed< T >::remove_at ( size_type index_n )
{
  Spans cspans = spans ();
  const int mdir = cspans.remove_at ( index_n );
  if ( mdir == 0 ) {
    return;
  }

  if ( mdir < 0 ) {
    decrement_size_count_back ( 1 );
  } else {
    decrement_size_count_front ( 1 );
  }
}

template < class T >
bool
Ring_Fixed< T >::remove_first_match ( const T & match_value_n )
{
  Spans cspans = spans ();
  const int mdir = cspans.remove_first_match ( match_value_n );
  if ( mdir == 0 ) {
    return false;
  }

  if ( mdir < 0 ) {
    decrement_size_count_back ( 1 );
  } else {
    decrement_size_count_front ( 1 );
  }
  return true;
}

template < class T >
bool
Ring_Fixed< T >::remove_last_match ( const T & match_value_n )
{
  Spans cspans = spans ();
  const int mdir = cspans.remove_last_match ( match_value_n );
  if ( mdir == 0 ) {
    return false;
  }

  if ( mdir < 0 ) {
    decrement_size_count_back ( 1 );
  } else {
    decrement_size_count_front ( 1 );
  }
  return true;
}

template < class T >
typename Ring_Fixed< T >::size_type
Ring_Fixed< T >::remove_all_matches ( const T & match_value_n )
{
  if ( is_empty () ) {
    return 0;
  }
  // Init with search start index
  size_type num_found = 0;
  size_type index = 0;
  do {
    index = index_of ( match_value_n, index );
    if ( index == _size ) {
      break;
    }
    remove_at ( index );
    ++num_found;
  } while ( !is_empty () );

  return num_found;
}

} // namespace sev::mem
