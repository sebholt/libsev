/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <sev/mem/view.hpp>
#include <cstddef>
#include <iterator>
#include <memory>
#include <new>
#include <type_traits>
#include <utility>

namespace sev::mem
{

/// @brief Dynamically allocated stack with a fixed size
///
/// Stack does not grow or shrink on push/pop
///
template < class T >
class Stack_Fixed
{
  public:
  // -- Types

  typedef T value_type;
  typedef T * iterator;
  typedef const T * const_iterator;
  typedef std::reverse_iterator< iterator > reverse_iterator;
  typedef std::reverse_iterator< const_iterator > const_reverse_iterator;
  typedef std::size_t size_type;

  typedef sev::mem::View< T > Span;
  typedef sev::mem::View< const T > Span_Const;

  public:
  // -- Constructors

  /// @brief Doesn't allocate anything
  Stack_Fixed ();

  /// @brief Allocates a chunk of memory
  Stack_Fixed ( size_type capacity_n );

  /// @brief Disable copy constructor
  ///
  Stack_Fixed ( const sev::mem::Stack_Fixed< T > & stack_n );

  /// @brief Move constructor
  ///
  Stack_Fixed ( sev::mem::Stack_Fixed< T > && stack_n );

  ~Stack_Fixed ();

  // -- Capacity

  /// @return Total capacity
  size_type
  capacity () const
  {
    return _capacity;
  }

  /// @brief Removes all items and sets the capacity to 0
  ///
  void
  clear_capacity ();

  /// @brief Changes the capacity
  ///
  void
  set_capacity ( size_type capacity_n );

  /// @brief Changes the capacity on demand
  ///
  void
  ensure_minimum_capacity ( size_type capacity_n );

  /// @brief Changes the capacity on demand
  ///
  void
  ensure_maximum_capacity ( size_type capacity_n );

  /// @brief Changes the capacity on demand
  ///
  void
  ensure_minimum_capacity_free ( size_type size_n );

  // -- Size

  /// @brief Number of items in the stack
  size_type
  size () const
  {
    return _size;
  }

  /// @return ( capacity() - size() )
  size_type
  capacity_free () const
  {
    return ( _capacity - _size );
  }

  /// @return True if ( size() == 0 )
  bool
  is_empty () const
  {
    return ( _size == 0 );
  }

  /// @brief Same as is_empty()
  bool
  empty () const
  {
    return ( _size == 0 );
  }

  bool
  is_full () const
  {
    return ( _size == capacity () );
  }

  /// @brief Resizes the stack to the given number of items
  ///
  template < typename... Args >
  void
  resize ( size_type size_n, Args &&... args_n );

  /// @brief Clears the stack and resizes to the given size
  ///
  /// Clearing before resizing avoids calling of item copy constructors.
  ///
  /// If the size matches the current size all items get destructed
  /// and constructed new nevertheless.
  ///
  template < typename... Args >
  void
  clear_resize ( size_type size_n, Args &&... args_n );

  /// @brief Ensure there are at least size_n objects available
  ///
  template < typename... Args >
  void
  ensure_minimum_size ( size_type size_n, Args &&... args_n );

  /// @brief Ensure there are at maximum size_n objects available
  ///
  template < typename... Args >
  void
  ensure_maximum_size ( size_type size_n );

  // -- Iterators

  iterator
  begin ()
  {
    return _items;
  }

  const_iterator
  begin () const
  {
    return _items;
  }

  const_iterator
  cbegin () const
  {
    return _items;
  }

  iterator
  end ()
  {
    return ( _items + _size );
  }

  const_iterator
  end () const
  {
    return ( _items + _size );
  }

  const_iterator
  cend () const
  {
    return ( _items + _size );
  }

  reverse_iterator
  rbegin ()
  {
    return reverse_iterator ( end () );
  }

  const_reverse_iterator
  rbegin () const
  {
    return const_reverse_iterator ( end () );
  }

  const_reverse_iterator
  crbegin () const
  {
    return const_reverse_iterator ( cend () );
  }

  reverse_iterator
  rend ()
  {
    return reverse_iterator ( begin () );
  }

  const_reverse_iterator
  rend () const
  {
    return const_reverse_iterator ( begin () );
  }

  const_reverse_iterator
  crend () const
  {
    return const_reverse_iterator ( cbegin () );
  }

  // -- Item access

  T *
  data ()
  {
    return _items;
  }

  const T *
  data () const
  {
    return _items;
  }

  const T *
  cdata () const
  {
    return _items;
  }

  T &
  item ( size_type index_n )
  {
    return _items[ index_n ];
  }

  const T &
  item ( size_type index_n ) const
  {
    return _items[ index_n ];
  }

  const T &
  citem ( size_type index_n ) const
  {
    return _items[ index_n ];
  }

  T &
  bottom ()
  {
    return _items[ 0 ];
  }

  const T &
  bottom () const
  {
    return _items[ 0 ];
  }

  T &
  front ()
  {
    return _items[ 0 ];
  }

  const T &
  front () const
  {
    return _items[ 0 ];
  }

  T &
  top ()
  {
    return _items[ _size - 1 ];
  }

  const T &
  top () const
  {
    return _items[ _size - 1 ];
  }

  T &
  back ()
  {
    return _items[ _size - 1 ];
  }

  const T &
  back () const
  {
    return _items[ _size - 1 ];
  }

  // -- Pushing

  void
  push_not_full ( const T & item_n );

  void
  push_not_full ( T && item_n );

  template < typename... Args >
  void
  emplace_not_full ( Args &&... args_n );

  template < typename... Args >
  void
  emplace_num_not_full ( size_type num_n, Args &&... args_n );

  /// @brief Trusts in the buffer to have enough capacity
  template < class Input_Iter >
  void
  stack_copy_not_full ( Input_Iter begin_n, Input_Iter end_n );

  // -- Popping

  void
  pop_not_empty ();

  void
  pop_not_empty ( T * output_n );

  void
  pop_num_not_empty ( size_type num_n );

  // -- Manipulation

  Span
  acquire_span ()
  {
    return Span ( _items, _size );
  }

  Span_Const
  acquire_span () const
  {
    return Span_Const ( _items, _size );
  }

  Span_Const
  acquire_span_const () const
  {
    return Span_Const ( _items, _size );
  }

  Span
  acquire_span_free ()
  {
    return Span ( ( _items + _size ), capacity_free () );
  }

  /// @brief Use with care
  void
  set_size_count ( size_type size_n )
  {
    DEBUG_ASSERT ( size_n <= _capacity );
    _size = size_n;
  }

  /// @brief Use with care
  void
  increment_size_count ( size_type delta_n )
  {
    DEBUG_ASSERT ( ( _size + delta_n ) <= _capacity );
    _size += delta_n;
  }

  /// @brief Use with care
  void
  decrement_size_count ( size_type delta_n )
  {
    DEBUG_ASSERT ( _size >= delta_n );
    _size -= delta_n;
  }

  size_type
  index_of ( const T & match_item_n ) const;

  size_type
  index_of ( const T & match_item_n, size_type begin_at_n ) const;

  /// @brief Removes the item a index_n and moves all elements above down by
  /// one
  ///
  /// @return False if the index was invalid
  bool
  remove_at ( size_type index_n );

  /// @brief Removes the first item that matches (searching bottom up)
  ///
  /// @return True if an item was removed
  bool
  remove_first_match ( const T & match_value_n );

  /// @brief Removes all item that match
  ///
  /// @return Number of items removed
  size_type
  remove_all_matches ( const T & match_value_n );

  /// @return The iterator after the removed item or end()
  ///
  iterator
  erase ( iterator it_n );

  // -- Mass change

  /// @brief Removes all items but does not change the capacity()
  ///
  void
  clear ();

  /// @brief Assigns all items to item_n
  ///
  void
  fill ( const T & value_n );

  // -- Operators

  T &
  operator[] ( size_type index_n )
  {
    return _items[ index_n ];
  }

  const T &
  operator[] ( size_type index_n ) const
  {
    return _items[ index_n ];
  }

  sev::mem::Stack_Fixed< T > &
  operator= ( const sev::mem::Stack_Fixed< T > & stack_n )
  {
    copy_assign ( stack_n );
    return *this;
  }

  sev::mem::Stack_Fixed< T > &
  operator= ( sev::mem::Stack_Fixed< T > && stack_n )
  {
    move_assign ( std::move ( stack_n ) );
    return *this;
  }

  // Protected methods
  protected:
  void
  copy_assign ( const sev::mem::Stack_Fixed< T > & stack_n );

  void
  move_assign ( sev::mem::Stack_Fixed< T > && stack_n );

  // Private methods
  private:
  void
  set_capacity_empty ( size_type capacity_n );

  T *
  allocate_num_items ( size_type size_n );

  void
  deallocate_num_items ( T * items_n, size_type size_n );

  void
  destruct_reverse ();

  void
  destruct_reverse_num ( size_type num_n );

  private:
  T * _items;
  size_type _capacity;
  size_type _size;
  std::allocator< T > _allocator;
};

template < class T >
inline Stack_Fixed< T >::Stack_Fixed ()
: _items ( nullptr )
, _capacity ( 0 )
, _size ( 0 )
{
}

template < class T >
Stack_Fixed< T >::Stack_Fixed ( size_type capacity_n )
: _items ( nullptr )
, _capacity ( 0 )
, _size ( 0 )
{
  if ( capacity_n != 0 ) {
    _items = allocate_num_items ( capacity_n );
    _capacity = capacity_n;
  }
}

template < class T >
Stack_Fixed< T >::Stack_Fixed ( const sev::mem::Stack_Fixed< T > & stack_n )
: _items ( nullptr )
, _capacity ( 0 )
, _size ( 0 )
{
  if ( stack_n.capacity () != 0 ) {
    // Allocate new capacity
    T * items_new ( allocate_num_items ( stack_n.capacity () ) );
    // Copy new items
    if ( stack_n.size () != 0 ) {
      T * it_new ( items_new );
      const_iterator it_cur ( stack_n.begin () );
      const_iterator it_end ( stack_n.end () );
      do {
        new ( it_new ) T ( *it_cur );
        ++it_new;
        ++it_cur;
      } while ( it_cur != it_end );
    }
    _items = items_new;
    _capacity = stack_n.capacity ();
    _size = stack_n.size ();
  }
}

template < class T >
Stack_Fixed< T >::Stack_Fixed ( sev::mem::Stack_Fixed< T > && stack_n )
: _items ( stack_n._items )
, _capacity ( stack_n._capacity )
, _size ( stack_n._size )
, _allocator ( std::move ( stack_n._allocator ) )
{
  stack_n._items = nullptr;
  stack_n._capacity = 0;
  stack_n._size = 0;
}

template < class T >
Stack_Fixed< T >::~Stack_Fixed ()
{
  clear_capacity ();
}

template < class T >
void
Stack_Fixed< T >::copy_assign ( const sev::mem::Stack_Fixed< T > & stack_n )
{
  clear ();
  set_capacity_empty ( stack_n.capacity () );
  // Copy new items on demand
  if ( stack_n._size != 0 ) {
    _size = stack_n._size;
    {
      T * it_new ( _items );
      const_iterator it_cur ( stack_n.begin () );
      const_iterator it_end ( stack_n.end () );
      do {
        new ( it_new ) T ( *it_cur );
        ++it_new;
        ++it_cur;
      } while ( it_cur != it_end );
    }
  }
}

template < class T >
void
Stack_Fixed< T >::move_assign ( sev::mem::Stack_Fixed< T > && stack_n )
{
  clear_capacity ();
  // Move pointers and size
  {
    _items = stack_n._items;
    _capacity = stack_n._capacity;
    _size = stack_n._size;
    _allocator = std::move ( stack_n._allocator );
  }
  // Clear source instance
  {
    stack_n._items = nullptr;
    stack_n._capacity = 0;
    stack_n._size = 0;
  }
}

template < class T >
void
Stack_Fixed< T >::destruct_reverse ()
{
  if ( _size != 0 ) {
    iterator it_cur ( end () );
    iterator it_last ( begin () );
    do {
      --it_cur;
      it_cur->~T ();
    } while ( it_cur != it_last );
  }
}

template < class T >
void
Stack_Fixed< T >::destruct_reverse_num ( size_type num_n )
{
  iterator it_cur ( end () );
  iterator it_last ( it_cur - num_n );
  while ( it_cur != it_last ) {
    --it_cur;
    it_cur->~T ();
  }
}

template < class T >
inline T *
Stack_Fixed< T >::allocate_num_items ( size_type size_n )
{
  DEBUG_ASSERT ( size_n != 0 );
  T * items_new ( _allocator.allocate ( size_n ) );
  return items_new;
}

template < class T >
inline void
Stack_Fixed< T >::deallocate_num_items ( T * items_n, size_type size_n )
{
  DEBUG_ASSERT ( items_n != 0 );
  DEBUG_ASSERT ( size_n != 0 );
  _allocator.deallocate ( items_n, size_n );
}

template < class T >
void
Stack_Fixed< T >::clear_capacity ()
{
  if ( _items != 0 ) {
    clear ();
    deallocate_num_items ( _items, _capacity );
    _items = nullptr;
    _capacity = 0;
  }
}

template < class T >
void
Stack_Fixed< T >::set_capacity ( size_type capacity_n )
{
  if ( _capacity != capacity_n ) {
    if ( capacity_n == 0 ) {
      // Zero capacity requested
      clear_capacity ();
    } else {
      T * items_new ( allocate_num_items ( capacity_n ) );
      size_type num_move ( _size );
      math::assign_smaller ( num_move, capacity_n );
      // Move items
      if ( num_move != 0 ) {
        T * it_new ( items_new );
        iterator it_cur ( begin () );
        iterator it_end ( it_cur + num_move );
        do {
          new ( it_new ) T ( std::move ( *it_cur ) );
          ++it_new;
          ++it_cur;
        } while ( it_cur != it_end );
      }
      // Destroy old items
      clear_capacity ();
      // Assign new storage state
      _items = items_new;
      _capacity = capacity_n;
      _size = num_move;
    }
  }
}

template < class T >
inline void
Stack_Fixed< T >::set_capacity_empty ( size_type capacity_n )
{
  // Match capacities
  if ( _capacity != capacity_n ) {
    // Clear old capacity
    clear_capacity ();
    // Allocate new capacity on demand
    if ( capacity_n != 0 ) {
      _items = allocate_num_items ( capacity_n );
      _capacity = capacity_n;
    }
  }
}

template < class T >
inline void
Stack_Fixed< T >::ensure_minimum_capacity ( size_type capacity_n )
{
  if ( capacity () < capacity_n ) {
    set_capacity ( capacity_n );
  }
}

template < class T >
inline void
Stack_Fixed< T >::ensure_maximum_capacity ( size_type capacity_n )
{
  if ( capacity () > capacity_n ) {
    set_capacity ( capacity_n );
  }
}

template < class T >
inline void
Stack_Fixed< T >::ensure_minimum_capacity_free ( size_type size_n )
{
  if ( capacity_free () < size_n ) {
    set_capacity ( _size + size_n );
  }
}

template < class T >
template < typename... Args >
void
Stack_Fixed< T >::resize ( size_type size_n, Args &&... args_n )
{
  if ( size_n != _size ) {
    if ( size_n == 0 ) {
      clear ();
    } else {
      if ( size_n < _size ) {
        // Remove items
        pop_num_not_empty ( ( _size - size_n ) );
      } else {
        ensure_minimum_capacity ( size_n );
        {
          // Construct new items
          const size_type num_new ( size_n - _size );
          {
            T * it_cur ( end () );
            T * it_end ( it_cur + num_new );
            do {
              new ( it_cur ) T ( std::forward< Args > ( args_n )... );
              ++it_cur;
            } while ( it_cur != it_end );
          }
          _size = size_n;
        }
      }
    }
  }
}

template < class T >
template < typename... Args >
void
Stack_Fixed< T >::clear_resize ( size_type size_n, Args &&... args_n )
{
  clear ();
  if ( size_n != 0 ) {
    if ( _capacity < size_n ) {
      set_capacity_empty ( size_n );
    }
    if ( _capacity >= size_n ) {
      // Construct new items
      {
        T * it_cur ( &_items[ 0 ] );
        T * it_end ( it_cur + size_n );
        do {
          new ( it_cur ) T ( std::forward< Args > ( args_n )... );
          ++it_cur;
        } while ( it_cur != it_end );
      }
      _size = size_n;
    }
  }
}

template < class T >
template < typename... Args >
void
Stack_Fixed< T >::ensure_minimum_size ( size_type size_n, Args &&... args_n )
{
  if ( _size < size_n ) {
    // Ensure there is enough capacity available
    ensure_minimum_capacity ( size_n );
    emplace_num_not_full ( ( size_n - _size ),
                           std::forward< Args > ( args_n )... );
  }
}

template < class T >
template < typename... Args >
inline void
Stack_Fixed< T >::ensure_maximum_size ( size_type size_n )
{
  if ( _size > size_n ) {
    pop_num_not_empty ( _size - size_n );
  }
}

template < class T >
inline void
Stack_Fixed< T >::push_not_full ( const T & item_n )
{
  DEBUG_ASSERT ( !is_full () );
  new ( ( _items + _size ) ) T ( item_n );
  ++_size;
}

template < class T >
inline void
Stack_Fixed< T >::push_not_full ( T && item_n )
{
  DEBUG_ASSERT ( !is_full () );
  new ( ( _items + _size ) ) T ( std::move ( item_n ) );
  ++_size;
}

template < class T >
template < typename... Args >
inline void
Stack_Fixed< T >::emplace_not_full ( Args &&... args_n )
{
  DEBUG_ASSERT ( !is_full () );
  new ( ( _items + _size ) ) T ( std::forward< Args > ( args_n )... );
  ++_size;
}

template < class T >
template < typename... Args >
void
Stack_Fixed< T >::emplace_num_not_full ( size_type num_n, Args &&... args_n )
{
  for ( iterator itc = end (), it_end = ( end () + num_n ); itc != it_end;
        ++itc ) {
    new ( itc ) T ( std::forward< Args > ( args_n )... );
  }
  _size += num_n;
}

template < class T >
template < class Input_Iter >
void
Stack_Fixed< T >::stack_copy_not_full ( Input_Iter begin_n, Input_Iter end_n )
{
  iterator itc = end ();
  for ( ; begin_n != end_n; ++begin_n, ++itc ) {
    new ( itc ) T ( *begin_n );
  }
  _size = std::distance ( begin (), itc );
}

template < class T >
inline void
Stack_Fixed< T >::pop_not_empty ()
{
  top ().~T ();
  --_size;
}

template < class T >
inline void
Stack_Fixed< T >::pop_not_empty ( T * output_n )
{
  T & bitem ( top () );
  *output_n = std::move ( bitem );
  bitem.~T ();
  --_size;
}

template < class T >
inline void
Stack_Fixed< T >::pop_num_not_empty ( size_type num_n )
{
  if ( !std::is_trivially_destructible< T >::value ) {
    destruct_reverse_num ( num_n );
  }
  _size -= num_n;
}

template < class T >
typename Stack_Fixed< T >::size_type
Stack_Fixed< T >::index_of ( const T & match_item_n ) const
{
  const_iterator it_begin ( cbegin () );
  const_iterator itc ( it_begin );
  const_iterator it_end ( it_begin + _size );
  for ( ; itc != it_end; ++itc ) {
    if ( *itc == match_item_n ) {
      break;
    }
  }
  return std::distance ( it_begin, itc );
}

template < class T >
typename Stack_Fixed< T >::size_type
Stack_Fixed< T >::index_of ( const T & match_item_n,
                             size_type begin_at_n ) const
{
  if ( begin_at_n < _size ) {
    const_iterator it_begin ( cbegin () );
    const_iterator itc ( it_begin + begin_at_n );
    const_iterator it_end ( it_begin + _size );
    for ( ; itc != it_end; ++itc ) {
      if ( *itc == match_item_n ) {
        break;
      }
    }
    return std::distance ( it_begin, itc );
  }
  return _size;
}

template < class T >
bool
Stack_Fixed< T >::remove_at ( size_type index_n )
{
  if ( index_n < _size ) {
    {
      iterator it_end ( end () );
      --it_end;
      // Move items above downwards by one
      {
        iterator itc ( begin () );
        itc += index_n;
        while ( itc != it_end ) {
          iterator it_next ( itc );
          ++it_next;
          *itc = std::move ( *it_next );
          itc = it_next;
        }
      }
      // Destruct last item
      it_end->~T ();
    }
    --_size;
    return true;
  }
  return false;
}

template < class T >
bool
Stack_Fixed< T >::remove_first_match ( const T & match_value_n )
{
  bool found ( false );
  if ( !is_empty () ) {
    const size_type index ( index_of ( match_value_n ) );
    if ( index != _size ) {
      remove_at ( index );
      found = true;
    }
  }
  return found;
}

template < class T >
typename Stack_Fixed< T >::size_type
Stack_Fixed< T >::remove_all_matches ( const T & match_value_n )
{
  size_type num_found ( 0 );
  if ( !is_empty () ) {
    // Init with search start index
    size_type index ( 0 );
    do {
      index = index_of ( match_value_n, index );
      if ( index != _size ) {
        remove_at ( index );
        ++num_found;
      } else {
        break;
      }
    } while ( !is_empty () );
  }
  return num_found;
}

template < class T >
typename Stack_Fixed< T >::iterator
Stack_Fixed< T >::erase ( iterator it_n )
{
  const size_type item_index ( std::distance ( begin (), it_n ) );
  if ( item_index < _size ) {
    {
      iterator it_end ( end () );
      --it_end;
      {
        iterator itc ( it_n );
        while ( itc != it_end ) {
          iterator it_next ( itc );
          ++it_next;
          *itc = std::move ( *it_next );
          itc = it_next;
        }
      }
      // Destruct last item
      it_end->~T ();
    }
    --_size;
    return it_n;
  }
  return end ();
}

template < class T >
inline void
Stack_Fixed< T >::clear ()
{
  if ( !std::is_trivially_destructible< T >::value ) {
    destruct_reverse ();
  }
  _size = 0;
}

template < class T >
void
Stack_Fixed< T >::fill ( const T & value_n )
{
  for ( auto & item : *this ) {
    item = value_n;
  }
}

} // namespace sev::mem
