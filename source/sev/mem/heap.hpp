/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/mem/key_value.hpp>
#include <sev/mem/stack_fixed.hpp>

namespace sev::mem
{

/// @brief Binary minimum heap
///
template < class KEY, class VALUE >
class Heap
: protected sev::mem::Stack_Fixed< sev::mem::Key_Value< KEY, VALUE > >
{
  public:
  // -- Types
  typedef sev::mem::Key_Value< KEY, VALUE > Item;
  typedef sev::mem::Stack_Fixed< Item > Stack;
  using typename Stack::size_type;

  public:
  // -- Constructors

  /// @brief Doesn't allocate anything
  Heap ();

  /// @brief Allocates a chunk of memory
  Heap ( size_type capacity_n );

  /// @brief Creates a deep copy of the data
  Heap ( const sev::mem::Heap< KEY, VALUE > & heap_n );

  /// @brief Move constructor
  ///
  Heap ( sev::mem::Heap< KEY, VALUE > && heap_n );

  ~Heap () = default;

  // -- Capacity

  using Stack::capacity;
  using Stack::clear_capacity;
  using Stack::ensure_maximum_capacity;
  using Stack::ensure_minimum_capacity;
  using Stack::ensure_minimum_capacity_free;
  using Stack::set_capacity;

  // -- Size

  using Stack::clear;
  using Stack::empty;
  using Stack::is_empty;
  using Stack::is_full;
  using Stack::size;

  // -- Size & Capacity

  using Stack::capacity_free;

  // -- Item access

  using Stack::citem;

  /// @brief Front item
  const Item &
  front () const;

  // -- Insert

  /// @brief Assumes !is_full() and copy inserts the item
  void
  insert_not_full ( const Item & item_n );

  /// @brief Assumes !is_full() and move inserts the item
  void
  insert_not_full ( Item && item_n );

  /// @brief Assumes !is_full() and move insert a new item
  void
  insert_not_full ( const KEY & key_n, const VALUE & value_n );

  /// @brief Assumes !is_full() and move insert a new item
  void
  insert_not_full ( KEY && key_n, VALUE && value_n );

  // -- Remove

  void
  pop_front_not_empty ();

  void
  pop_front_not_empty ( Item * item_n );

  void
  remove_at ( size_type index_n );

  /// @brief Removes the first detected item with a key match
  /// @return True if an item was found and removed
  bool
  remove_key_match ( const KEY & value_n );

  /// @brief Removes the first detected item with a value match
  /// @return True if an item was found and removed
  bool
  remove_value_match ( const VALUE & value_n );

  // -- Operators

  sev::mem::Heap< KEY, VALUE > &
  operator= ( const sev::mem::Heap< KEY, VALUE > & heap_n )
  {
    Stack::copy_assign ( static_cast< const Stack & > ( heap_n ) );
    return *this;
  }

  sev::mem::Heap< KEY, VALUE > &
  operator= ( sev::mem::Heap< KEY, VALUE > && heap_n )
  {
    Stack::move_assign ( static_cast< Stack && > ( heap_n ) );
    return *this;
  }

  protected:
  const KEY &
  item_key ( size_type index_n ) const
  {
    return citem ( index_n ).key ();
  }

  const VALUE &
  item_value ( size_type index_n ) const
  {
    return citem ( index_n ).value ();
  }

  void
  swap_items ( size_type index_a_n, size_type index_b_n )
  {
    std::swap ( Stack::item ( index_a_n ), Stack::item ( index_b_n ) );
  }

  void
  move_item ( size_type index_dst_n, size_type index_src_n );

  /// @brief Returns the parent item index
  size_type
  parent_index ( size_type index_n ) const
  {
    size_type res ( index_n );
    --res;
    res /= size_type ( 2 );
    return res;
  }

  /// @brief Returns the left child item index
  size_type
  left_index ( size_type index_n ) const
  {
    size_type res ( index_n );
    res *= size_type ( 2 );
    res += size_type ( 1 );
    return res;
  }

  /// @brief Returns the left child item index
  size_type
  right_index ( size_type index_n ) const
  {
    size_type res ( index_n );
    res *= size_type ( 2 );
    res += size_type ( 2 );
    return res;
  }

  void
  sink_down ( size_type index_n );

  void
  sink_down_last ();

  /// @brief Sifts and item up until tree order is restored
  void
  sift_up ( size_type index_n );
};

template < class KEY, class VALUE >
Heap< KEY, VALUE >::Heap ()
{
}

template < class KEY, class VALUE >
Heap< KEY, VALUE >::Heap ( size_type capacity_n )
: Stack ( capacity_n )
{
}

template < class KEY, class VALUE >
Heap< KEY, VALUE >::Heap ( const sev::mem::Heap< KEY, VALUE > & heap_n )
: Stack ( static_cast< const Stack & > ( heap_n ) )
{
}

template < class KEY, class VALUE >
Heap< KEY, VALUE >::Heap ( sev::mem::Heap< KEY, VALUE > && heap_n )
: Stack ( static_cast< Stack && > ( heap_n ) )
{
}

template < class KEY, class VALUE >
inline const typename Heap< KEY, VALUE >::Item &
Heap< KEY, VALUE >::front () const
{
  return Stack::front ();
}

template < class KEY, class VALUE >
inline void
Heap< KEY, VALUE >::insert_not_full ( const Item & item_n )
{
  Stack::push_not_full ( item_n );
  sink_down_last ();
}

template < class KEY, class VALUE >
inline void
Heap< KEY, VALUE >::insert_not_full ( Item && item_n )
{
  Stack::push_not_full ( std::move ( item_n ) );
  sink_down_last ();
}

template < class KEY, class VALUE >
inline void
Heap< KEY, VALUE >::insert_not_full ( const KEY & key_n, const VALUE & value_n )
{
  Stack::emplace_not_full ( key_n, value_n );
  sink_down_last ();
}

template < class KEY, class VALUE >
inline void
Heap< KEY, VALUE >::insert_not_full ( KEY && key_n, VALUE && value_n )
{
  Stack::emplace_not_full ( std::move ( key_n ), std::move ( value_n ) );
  sink_down_last ();
}

template < class KEY, class VALUE >
void
Heap< KEY, VALUE >::pop_front_not_empty ()
{
  // Move last item to front
  if ( size () != 1 ) {
    Stack::front () = std::move ( Stack::back () );
  }
  // Remove last item
  Stack::pop_not_empty ();
  // Sift item up until tree order is restored
  sift_up ( 0 );
}

template < class KEY, class VALUE >
inline void
Heap< KEY, VALUE >::pop_front_not_empty ( Item * item_n )
{
  *item_n = std::move ( Stack::front () );
  pop_front_not_empty ();
}

template < class KEY, class VALUE >
void
Heap< KEY, VALUE >::remove_at ( size_type index_n )
{
  if ( index_n < size () ) {
    size_type index_last ( size () - 1 );
    if ( index_n != index_last ) {
      // Overwrite item with last item
      Stack::item ( index_n ) = std::move ( Stack::item ( index_last ) );
    }
    // Drop last item
    Stack::pop_not_empty ();
    if ( index_n != index_last ) {
      // Restore tree order
      if ( ( index_n == 0 ) ||
           ( item_key ( index_n ) > item_key ( parent_index ( index_n ) ) ) ) {
        sift_up ( index_n );
      } else {
        sink_down ( index_n );
      }
    }
  }
}

template < class KEY, class VALUE >
bool
Heap< KEY, VALUE >::remove_key_match ( const KEY & key_n )
{
  typedef const Item * Iter;
  Iter it_begin ( Stack::cbegin () );
  Iter it_end ( Stack::cend () );
  Iter it_cur ( it_begin );
  for ( ; it_cur != it_end; ++it_cur ) {
    if ( it_cur->key () == key_n ) {
      remove_at ( it_cur - it_begin );
      return true;
    }
  }
  return false;
}

template < class KEY, class VALUE >
bool
Heap< KEY, VALUE >::remove_value_match ( const VALUE & value_n )
{
  typedef const Item * Iter;
  Iter it_begin ( Stack::cbegin () );
  Iter it_end ( Stack::cend () );
  Iter it_cur ( it_begin );
  for ( ; it_cur != it_end; ++it_cur ) {
    if ( it_cur->value () == value_n ) {
      remove_at ( it_cur - it_begin );
      return true;
    }
  }
  return false;
}

template < class KEY, class VALUE >
void
Heap< KEY, VALUE >::sink_down ( size_type index_n )
{
  size_type iii ( index_n );
  while ( iii != 0 ) {
    const size_type pindex ( parent_index ( iii ) );
    {
      Item & item_cur ( Stack::item ( iii ) );
      Item & item_parent ( Stack::item ( pindex ) );
      // Swap items and continue OR finish
      if ( item_cur.key () < item_parent.key () ) {
        std::swap ( item_cur, item_parent );
      } else {
        break;
      }
    }
    iii = pindex;
  }
}

template < class KEY, class VALUE >
void
Heap< KEY, VALUE >::sink_down_last ()
{
  size_type iii ( size () - 1 );
  while ( iii != 0 ) {
    const size_type pindex ( parent_index ( iii ) );
    {
      Item & item_cur ( Stack::item ( iii ) );
      Item & item_parent ( Stack::item ( pindex ) );
      // Swap items and continue OR finish
      if ( item_cur.key () < item_parent.key () ) {
        std::swap ( item_cur, item_parent );
      } else {
        break;
      }
    }
    iii = pindex;
  }
}

template < class KEY, class VALUE >
void
Heap< KEY, VALUE >::sift_up ( size_type index_n )
{
  const size_type lsize ( size () );
  if ( index_n < lsize ) {
    // Running index
    size_type iii ( index_n );
    do {
      // Index of the smallest item
      size_type min_idx ( iii );
      {
        size_type lidx ( left_index ( iii ) );
        size_type ridx ( right_index ( iii ) );
        // Check left
        if ( lidx < lsize ) {
          if ( item_key ( lidx ) < item_key ( min_idx ) ) {
            min_idx = lidx;
          }
        }
        // Check right
        if ( ridx < lsize ) {
          if ( item_key ( ridx ) < item_key ( min_idx ) ) {
            min_idx = ridx;
          }
        }
      }
      if ( min_idx == iii ) {
        // Item relations are in order
        break;
      }
      // Swap item upward
      swap_items ( iii, min_idx );
      // Continue at new position
      iii = min_idx;
    } while ( true );
  }
}

} // namespace sev::mem