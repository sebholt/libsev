/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstddef>
#include <cstdint>

namespace sev::mem
{

/// @brief 32 bit wide container for pod types
///
class Value32
{
  // Public methods
  public:
  /// @brief Leaves the value uninitialized
  ///
  Value32 () = default;

  explicit Value32 ( std::uint8_t val0_n,
                     std::uint8_t val1_n,
                     std::uint8_t val2_n,
                     std::uint8_t val3_n )
  {
    // Static checks
    static_assert ( sizeof ( float ) <= 4, "sizeof ( float ) must be <= 4" );
    static_assert ( sizeof ( Value32 ) == 4, "Value32 must 4 bytes in size" );

    _value.vuint8[ 0 ] = val0_n;
    _value.vuint8[ 1 ] = val1_n;
    _value.vuint8[ 2 ] = val2_n;
    _value.vuint8[ 3 ] = val3_n;
  }

  explicit Value32 ( std::uint16_t val0_n, std::uint16_t val1_n )
  {
    _value.vuint16[ 0 ] = val0_n;
    _value.vuint16[ 1 ] = val1_n;
  }

  explicit Value32 ( std::uint32_t val0_n ) { _value.vuint32 = val0_n; }

  explicit Value32 ( float val0_n ) { _value.vfloat = val0_n; }

  std::uint8_t &
  ref_uint8 ( std::size_t index_n = 0 )
  {
    return _value.vuint8[ index_n ];
  }

  std::uint8_t
  get_uint8 ( std::size_t index_n = 0 ) const
  {
    return _value.vuint8[ index_n ];
  }

  void
  set_uint8 ( std::size_t index_n, std::uint8_t value_n )
  {
    _value.vuint8[ index_n ] = value_n;
  }

  std::uint16_t &
  ref_uint16 ( std::size_t index_n = 0 )
  {
    return _value.vuint16[ index_n ];
  }

  std::uint16_t
  get_uint16 ( std::size_t index_n = 0 ) const
  {
    return _value.vuint16[ index_n ];
  }

  void
  set_uint16 ( std::size_t index_n, std::uint16_t value_n )
  {
    _value.vuint16[ index_n ] = value_n;
  }

  std::uint32_t &
  ref_uint32 ()
  {
    return _value.vuint32;
  }

  std::uint32_t
  get_uint32 () const
  {
    return _value.vuint32;
  }

  void
  set_uint32 ( std::uint32_t value_n )
  {
    _value.vuint32 = value_n;
  }

  float &
  ref_float ()
  {
    return _value.vfloat;
  }

  float
  get_float () const
  {
    return _value.vfloat;
  }

  void
  set_float ( float value_n )
  {
    _value.vfloat = value_n;
  }

  // -- Operators

  bool
  operator== ( const Value32 & value_n ) const
  {
    return ( _value.vuint32 == value_n._value.vuint32 );
  }

  bool
  operator!= ( const Value32 & value_n ) const
  {
    return ( _value.vuint32 != value_n._value.vuint32 );
  }

  // Private attributes
  public:
  union
  {
    std::uint8_t vuint8[ 4 ];
    std::uint16_t vuint16[ 2 ];
    std::uint32_t vuint32;
    float vfloat;
  } _value;
};

} // namespace sev::mem
