/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstddef>
#include <cstdint>

namespace sev::mem
{

/// @brief 64 bit wide container for pod types
///
class Value64
{
  // Public methods
  public:
  /// @brief Leaves the value uninitialized
  ///
  Value64 () = default;

  explicit Value64 ( std::uint8_t val0_n,
                     std::uint8_t val1_n,
                     std::uint8_t val2_n,
                     std::uint8_t val3_n,
                     std::uint8_t val4_n,
                     std::uint8_t val5_n,
                     std::uint8_t val6_n,
                     std::uint8_t val7_n )
  {
    // Static checks
    static_assert ( sizeof ( float ) <= 4, "sizeof ( float ) must be <= 4" );
    static_assert ( sizeof ( double ) <= 8, "sizeof ( double ) must be <= 8" );
    static_assert ( sizeof ( Value64 ) == 8, "Value64 must 8 bytes in size" );

    _value.vuint8[ 0 ] = val0_n;
    _value.vuint8[ 1 ] = val1_n;
    _value.vuint8[ 2 ] = val2_n;
    _value.vuint8[ 3 ] = val3_n;
    _value.vuint8[ 4 ] = val4_n;
    _value.vuint8[ 5 ] = val5_n;
    _value.vuint8[ 6 ] = val6_n;
    _value.vuint8[ 7 ] = val7_n;
  }

  explicit Value64 ( std::uint16_t val0_n,
                     std::uint16_t val1_n,
                     std::uint16_t val2_n,
                     std::uint16_t val3_n )
  {
    _value.vuint16[ 0 ] = val0_n;
    _value.vuint16[ 1 ] = val1_n;
    _value.vuint16[ 2 ] = val2_n;
    _value.vuint16[ 3 ] = val3_n;
  }

  explicit Value64 ( std::uint32_t val0_n, std::uint32_t val1_n )
  {
    _value.vuint32[ 0 ] = val0_n;
    _value.vuint32[ 1 ] = val1_n;
  }

  explicit Value64 ( float val0_n, float val1_n )
  {
    _value.vfloat[ 0 ] = val0_n;
    _value.vfloat[ 1 ] = val1_n;
  }

  explicit Value64 ( std::uint64_t val0_n ) { _value.vuint64 = val0_n; }

  explicit Value64 ( double val_n ) { _value.vdouble = val_n; }

  std::uint8_t &
  ref_uint8 ( std::size_t index_n = 0 )
  {
    return _value.vuint8[ index_n ];
  }

  std::uint8_t
  get_uint8 ( std::size_t index_n = 0 ) const
  {
    return _value.vuint8[ index_n ];
  }

  void
  set_uint8 ( std::size_t index_n, std::uint8_t value_n )
  {
    _value.vuint8[ index_n ] = value_n;
  }

  std::uint16_t &
  ref_uint16 ( std::size_t index_n = 0 )
  {
    return _value.vuint16[ index_n ];
  }

  std::uint16_t
  get_uint16 ( std::size_t index_n = 0 ) const
  {
    return _value.vuint16[ index_n ];
  }

  void
  set_uint16 ( std::size_t index_n, std::uint16_t value_n )
  {
    _value.vuint16[ index_n ] = value_n;
  }

  std::uint32_t &
  ref_uint32 ( std::size_t index_n = 0 )
  {
    return _value.vuint32[ index_n ];
  }

  std::uint32_t
  get_uint32 ( std::size_t index_n = 0 ) const
  {
    return _value.vuint32[ index_n ];
  }

  void
  set_uint32 ( std::size_t index_n, std::uint32_t value_n )
  {
    _value.vuint32[ index_n ] = value_n;
  }

  std::uint64_t &
  ref_uint64 ()
  {
    return _value.vuint64;
  }

  std::uint64_t
  get_uint64 () const
  {
    return _value.vuint64;
  }

  void
  set_uint64 ( std::uint64_t value_n )
  {
    _value.vuint64 = value_n;
  }

  float &
  ref_float ( std::size_t index_n = 0 )
  {
    return _value.vfloat[ index_n ];
  }

  float
  get_float ( std::size_t index_n = 0 ) const
  {
    return _value.vfloat[ index_n ];
  }

  void
  set_float ( std::size_t index_n, float value_n )
  {
    _value.vfloat[ index_n ] = value_n;
  }

  double &
  ref_double ()
  {
    return _value.vdouble;
  }

  double
  get_double () const
  {
    return _value.vdouble;
  }

  void
  set_double ( double value_n )
  {
    _value.vdouble = value_n;
  }

  // -- Operators

  bool
  operator== ( const Value64 & value_n ) const
  {
    return ( _value.vuint64 == value_n._value.vuint64 );
  }

  bool
  operator!= ( const Value64 & value_n ) const
  {
    return ( _value.vuint64 != value_n._value.vuint64 );
  }

  // Private attributes
  public:
  union
  {
    std::uint8_t vuint8[ 8 ];
    std::uint16_t vuint16[ 4 ];
    std::uint32_t vuint32[ 2 ];
    std::uint64_t vuint64;
    float vfloat[ 2 ];
    double vdouble;
  } _value;
};

} // namespace sev::mem
