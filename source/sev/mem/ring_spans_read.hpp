/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/math/numbers.hpp>
#include <sev/mem/view.hpp>
#include <array>

namespace sev::mem
{

/// @brief Read only spans of a ring buffer
///
template < class T >
class Ring_Spans_Read : public std::array< sev::mem::View< T >, 2 >
{
  public:
  // -- Types

  using Span = sev::mem::View< T >;
  using Spans = std::array< Span, 2 >;

  // -- Construction

  Ring_Spans_Read () {}

  Ring_Spans_Read ( Span span_a_n, Span span_b_n )
  : Spans{ { span_a_n, span_b_n } }
  {
  }

  Ring_Spans_Read ( T * items_a_n,
                    std::size_t size_a_n,
                    T * items_b_n,
                    std::size_t size_b_n )
  : Spans{ { Span ( items_a_n, size_a_n ), Span ( items_b_n, size_b_n ) } }
  {
  }

  // -- Searching

  std::size_t
  index_of ( const T & item_match_n ) const;

  std::size_t
  index_of ( const T & item_match_n, std::size_t index_begin_n ) const;

  // -- Span access

  std::size_t
  spans_size () const
  {
    return ( span ( 0 ).size () + span ( 1 ).size () );
  }

  constexpr static std::size_t
  num_spans ()
  {
    return 2;
  }

  Span &
  span ( std::size_t index_n )
  {
    return Spans::operator[] ( index_n );
  }

  const Span &
  span ( std::size_t index_n ) const
  {
    return Spans::operator[] ( index_n );
  }

  // -- Manipulation

  void
  clear ()
  {
    span ( 0 ).clear ();
    span ( 1 ).clear ();
  }

  void
  drop_front ( std::size_t num_n );
};

template < class T >
inline std::size_t
Ring_Spans_Read< T >::index_of ( const T & item_match_n ) const
{
  std::size_t iindex ( 0 );
  bool found ( false );
  for ( const auto & cspan : *this ) {
    {
      typedef typename Span::const_iterator TCIter;
      TCIter it_begin ( cspan.cbegin () );
      TCIter it_cur ( it_begin );
      TCIter it_end ( it_begin + cspan.size () );
      while ( it_cur != it_end ) {
        if ( *it_cur == item_match_n ) {
          iindex += std::distance ( it_begin, it_cur );
          found = true;
          break;
        }
        ++it_cur;
      }
    }
    if ( found ) {
      break;
    }
    iindex += cspan.size ();
  }
  return iindex;
}

template < class T >
inline std::size_t
Ring_Spans_Read< T >::index_of ( const T & item_match_n,
                                 std::size_t index_begin_n ) const
{
  std::size_t iindex ( 0 );
  {
    bool found ( false );
    for ( const auto & cspan : *this ) {
      if ( index_begin_n < cspan.size () ) {
        typedef typename Span::const_iterator TCIter;
        TCIter it_begin ( cspan.cbegin () );
        TCIter it_cur ( it_begin + index_begin_n );
        TCIter it_end ( it_begin + cspan.size () );
        while ( it_cur != it_end ) {
          if ( *it_cur == item_match_n ) {
            iindex += std::distance ( it_begin, it_cur );
            found = true;
            break;
          }
          ++it_cur;
        }
      }
      if ( found ) {
        break;
      }
      // Check next span with these modifications
      index_begin_n -= cspan.size ();
      iindex += cspan.size ();
    }
  }
  return iindex;
}

template < class T >
void
Ring_Spans_Read< T >::drop_front ( std::size_t num_n )
{
  if ( num_n < span ( 0 ).size () ) {
    span ( 0 ).set_items ( span ( 0 ).items () + num_n );
    span ( 0 ).set_size ( span ( 0 ).size () - num_n );
  } else {
    num_n -= span ( 0 ).size ();
    span ( 0 ).set_items ( span ( 1 ).items () + num_n );
    span ( 0 ).set_size ( span ( 1 ).size () - num_n );
    span ( 1 ).clear ();
  }
}

} // namespace sev::mem
