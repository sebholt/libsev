/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::mem
{

/// @brief Set of binary flags with set/unset/test interface
///
template < class IT >
class Flags
{
  public:
  // -- Types

  using Int_Type = IT;

  static constexpr Int_Type F_None = Int_Type ( 0 );
  static constexpr Int_Type F_All = ~Int_Type ( 0 );

  // -- Constructioin

  constexpr Flags ( Int_Type flags_n = F_None )
  : _flags ( flags_n )
  {
  }

  // -- Accessor

  constexpr Int_Type &
  flags ()
  {
    return _flags;
  }

  constexpr const Int_Type &
  flags () const
  {
    return _flags;
  }

  // -- Fill

  constexpr void
  assign ( Int_Type flags_n )
  {
    _flags = flags_n;
  }

  constexpr void
  fill ( bool state_n )
  {
    if ( state_n ) {
      fill_high ();
    } else {
      fill_low ();
    }
  }

  constexpr void
  fill_high ()
  {
    assign ( F_All );
  }

  constexpr void
  fill_low ()
  {
    assign ( F_None );
  }

  constexpr void
  clear ()
  {
    fill_low ();
  }

  // -- Test

  /// @return True if all flags are low
  constexpr bool
  is_empty () const
  {
    return ( _flags == F_None );
  }

  /// @return True if any of the given flags is set
  constexpr bool
  test_any ( Int_Type flags_n ) const
  {
    return ( ( _flags & flags_n ) != F_None );
  }

  /// @return True if all of the given flags are set
  constexpr bool
  test_all ( Int_Type flags_n ) const
  {
    return ( ( _flags & flags_n ) == flags_n );
  }

  /// @return True if the bit registers are equal
  constexpr bool
  matches ( Int_Type flags_n ) const
  {
    return ( _flags == flags_n );
  }

  // -- Get

  /// @return The flags.
  constexpr const Int_Type &
  get () const
  {
    return _flags;
  }

  /// @return Returns the flags and clears the register.
  constexpr Int_Type
  fetch_and_clear ()
  {
    Int_Type res = _flags;
    _flags = F_None;
    return res;
  }

  // -- Set

  /// @return Sets the given flags high
  constexpr void
  set ( Int_Type flags_n )
  {
    _flags |= flags_n;
  }

  /// @return Sets the given flags low
  constexpr void
  unset ( Int_Type flags_n )
  {
    _flags &= ~flags_n;
  }

  /// @return Sets the given flags high or low
  constexpr void
  set ( Int_Type flags_n, bool on_n )
  {
    if ( on_n ) {
      set ( flags_n );
    } else {
      unset ( flags_n );
    }
  }

  // -- Test and set

  /// @brief Tests if any of the given flags is high and sets all high
  /// otherwise
  /// @return True if any of the given flags were high
  constexpr bool
  test_any_set ( Int_Type flags_n )
  {
    if ( !test_any ( flags_n ) ) {
      set ( flags_n );
      return false;
    }
    return true;
  }

  /// @brief Tests if any of the given flags is high and sets all low then
  /// @return True if any of the given flags were high
  constexpr bool
  test_any_unset ( Int_Type flags_n )
  {
    if ( test_any ( flags_n ) ) {
      unset ( flags_n );
      return true;
    }
    return false;
  }

  // -- Type cast operators

  operator Int_Type & () { return _flags; }

  operator const Int_Type & () const { return _flags; }

  private:
  // -- Attributes
  Int_Type _flags;
};

// -- Types
using Flags_Fast8 = Flags< std::uint_fast8_t >;
using Flags_Fast16 = Flags< std::uint_fast16_t >;
using Flags_Fast32 = Flags< std::uint_fast32_t >;
using Flags_Fast64 = Flags< std::uint_fast64_t >;

} // namespace sev::mem