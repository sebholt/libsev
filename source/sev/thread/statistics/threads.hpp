/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/thread/statistics/thread.hpp>
#include <chrono>
#include <cstddef>
#include <vector>

namespace sev::thread::detail
{
class Tracker;
}

namespace sev::thread::statistics
{

/// @brief Multiple threads statistics
///
class Threads
{
  public:
  // -- Types

  using Clock = std::chrono::steady_clock;
  using Duration = Clock::duration;
  using Time_Point = Clock::time_point;
  using Threads_List = std::vector< Thread >;

  // -- Construction

  Threads ();

  Threads ( const Threads & statistics_n );

  // -- Assignment operators

  Threads &
  operator= ( const Threads & statistics_n );

  // -- Accessor: Frame time

  const Time_Point &
  frame_begin () const
  {
    return _frame_begin;
  }

  const Time_Point &
  frame_end () const
  {
    return _frame_end;
  }

  const Duration &
  frame_duration () const
  {
    return _frame_duration;
  }

  double
  frame_duration_seconds () const
  {
    return std::chrono::duration< double > ( _frame_duration ).count ();
  }

  // -- Accessor: Threads

  std::size_t
  num_threads () const
  {
    return _threads.size ();
  }

  std::size_t
  threads_running () const
  {
    return _threads_running;
  }

  std::uint_fast32_t
  list_changes () const
  {
    return _list_changes;
  }

  const Threads_List &
  threads () const
  {
    return _threads;
  }

  const Thread &
  thread ( std::size_t index_n ) const
  {
    return _threads[ index_n ];
  }

  const Thread *
  thread_by_id ( std::uint_fast32_t id_n ) const;

  private:
  // -- Utility
  friend class sev::thread::detail::Tracker;

  void
  thread_emplace ( const sev::thread::detail::Thread & thread_n );

  void
  thread_remove ( std::size_t index_n );

  private:
  // -- Attributes
  Time_Point _frame_begin;
  Time_Point _frame_end;
  Duration _frame_duration;
  std::size_t _threads_running = 0;
  std::uint_fast32_t _list_changes = 0;
  Threads_List _threads;
};

} // namespace sev::thread::statistics
