/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/thread/statistics/threads.hpp>
#include <algorithm>

namespace sev::thread::statistics
{

Threads::Threads ()
: _frame_begin ( Clock::now () )
, _frame_end ( _frame_begin )
, _frame_duration ( _frame_end - _frame_begin )
{
}

Threads::Threads ( const Threads & statistics_n ) = default;

Threads &
Threads::operator= ( const Threads & statistics_n ) = default;

void
Threads::thread_emplace ( const sev::thread::detail::Thread & thread_n )
{
  _threads.emplace_back ( thread_n );
  ++_list_changes;
}

void
Threads::thread_remove ( std::size_t index_n )
{
  _threads.erase ( _threads.cbegin () + index_n );
  ++_list_changes;
}

const Thread *
Threads::thread_by_id ( std::uint_fast32_t id_n ) const
{
  auto it_end = _threads.cend ();
  auto it = std::find_if (
      _threads.cbegin (), it_end, [ id_n ] ( const auto & thread_n ) {
        return thread_n.id () == id_n;
      } );
  if ( it != it_end ) {
    return &( *it );
  }
  return nullptr;
}

} // namespace sev::thread::statistics
