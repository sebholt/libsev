/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/unicode/view.hpp>
#include <time.h>
#include <functional>
#include <string>
#include <thread>

// -- Forward declaration
namespace sev::thread
{
class Tracker;
}

namespace sev::thread::detail
{

/// @brief Processing thread
///
class Thread
{
  public:
  // -- Construction

  Thread ( sev::unicode::View name_n,
           sev::thread::Tracker * tracker_n = nullptr );

  Thread ( const Thread & thread_n ) = delete;

  Thread ( Thread && thread_n ) = delete;

  ~Thread ();

  // -- Assignment operators

  Thread &
  operator= ( const Thread & thread_n ) = delete;

  Thread &
  operator= ( Thread && thread_n ) = delete;

  // -- Thread session interface

  /// @return True if the thread was started
  bool
  start ();

  /// @brief Join thread
  void
  join ();

  public:
  // -- Identification
  std::uint_fast32_t id = 0;
  const std::string name;
  // -- Thread execution
  bool is_running = false;
  struct
  {
    bool valid = false;
    clockid_t id = {};
  } cpu_clock;
  std::function< void () > function;
  std::thread std_thread;
  // -- Tracking
  sev::thread::Tracker * tracker = nullptr;
};

} // namespace sev::thread::detail
