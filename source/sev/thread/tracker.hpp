/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <memory>

// -- Forward declaration
namespace sev::thread::detail
{
class Thread;
class Tracker;
} // namespace sev::thread::detail
namespace sev::thread::statistics
{
class Threads;
} // namespace sev::thread::statistics

namespace sev::thread
{

/// @brief Tracks processing threads and provides thread statistics.
///
class Tracker
{
  public:
  // -- Construction

  Tracker ();

  Tracker ( const Tracker & tracker_n ) = delete;

  Tracker ( Tracker && tracker_n );

  ~Tracker ();

  // -- Assignment operators

  Tracker &
  operator= ( const Tracker & tracker_n ) = delete;

  Tracker &
  operator= ( Tracker && tracker_n );

  // -- Threads statistics

  std::shared_ptr< statistics::Threads >
  statistics_acquire ();

  void
  statistics_release ( std::shared_ptr< statistics::Threads > && stats_n );

  private:
  // -- Implementation
  friend class detail::Thread;
  std::shared_ptr< detail::Tracker > _detail;
};

} // namespace sev::thread
