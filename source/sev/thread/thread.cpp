/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "thread.hpp"
#include <sev/thread/detail/thread.hpp>
#include <sev/unicode/view.hpp>
#include <string>
#include <utility>

namespace sev::thread
{

Thread::Thread ( sev::unicode::View name_n, Tracker * tracker_n )
: _detail ( std::make_unique< detail::Thread > ( name_n, tracker_n ) )
{
}

Thread::Thread ( Thread && thread_n ) = default;

Thread::~Thread () = default;

Thread &
Thread::operator= ( sev::thread::Thread && thread_n ) = default;

std::uint_fast32_t
Thread::id () const
{
  return _detail->id;
}

std::string_view
Thread::name () const
{
  return _detail->name;
}

sev::thread::Tracker *
Thread::tracker () const
{
  return _detail->tracker;
}

const std::thread &
Thread::std_thread () const
{
  return _detail->std_thread;
}

std::thread::native_handle_type
Thread::native_handle () const
{
  return _detail->std_thread.native_handle ();
}

bool
Thread::start ( std::function< void () > function_n )
{
  if ( _detail->is_running ) {
    return false;
  }

  _detail->function = std::move ( function_n );
  return _detail->start ();
}

void
Thread::join ()
{
  _detail->join ();
}

bool
Thread::is_running () const
{
  return _detail->is_running;
}

} // namespace sev::thread
