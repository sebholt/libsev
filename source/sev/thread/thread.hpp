/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/unicode/view.hpp>
#include <functional>
#include <memory>
#include <string_view>
#include <thread>

// -- Forward declaration
namespace sev::thread
{
class Tracker;
namespace detail
{
class Thread;
} // namespace detail
} // namespace sev::thread

namespace sev::thread
{

/// @brief Processing thread
///
class Thread
{
  public:
  // -- Construction

  Thread ( sev::unicode::View name_n,
           sev::thread::Tracker * tracker_n = nullptr );

  Thread ( const Thread & thread_n ) = delete;

  Thread ( Thread && thread_n );

  ~Thread ();

  // -- Assignment operators

  Thread &
  operator= ( const sev::thread::Thread & thread_n ) = delete;

  Thread &
  operator= ( sev::thread::Thread && thread_n );

  // -- Accessors

  std::uint_fast32_t
  id () const;

  std::string_view
  name () const;

  sev::thread::Tracker *
  tracker () const;

  const std::thread &
  std_thread () const;

  std::thread::native_handle_type
  native_handle () const;

  // -- Thread session control

  /// @return True if the thread was started
  bool
  start ( std::function< void () > function_n );

  /// @brief Join thread
  void
  join ();

  /// @brief True if the thread is running
  bool
  is_running () const;

  private:
  // -- Implementation
  std::unique_ptr< detail::Thread > _detail;
};

} // namespace sev::thread
