/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/json/escape.hpp>
#include <sev/unicode/view.hpp>
#include <array>
#include <string>

namespace sev::json::composer
{

// -- Forward declaration
class Node;

/// @brief Json composer class
///
class Composer
{
  public:
  // -- Friends

  friend class sev::json::composer::Node;

  // -- Types

  struct Char_Escape
  {
    char cha;
    const char * rep;
  };

  // -- Construction

  Composer ();

  // -- String

  const std::string &
  string () const
  {
    return _string;
  }

  private:
  // -- Attributes
  std::string _string;
};

/// @brief Composer user base class
///
class Node
{
  public:
  // -- Construction

  Node ( Composer & composer_n );

  Node ( Node & parent_n );

  Composer &
  composer ()
  {
    return _composer;
  }

  const Composer &
  composer () const
  {
    return _composer;
  }

  Node *
  parent ()
  {
    return _parent;
  }

  bool
  first_value () const;

  void
  increment_num_values ();

  protected:
  void
  append ( char char_n );

  void
  append ( std::string_view text_n );

  std::string
  quoted ( std::string_view text_n ) const;

  private:
  // -- Attributes
  Composer & _composer;
  Node * _parent = nullptr;
  std::uint_fast32_t _depth = 0;
  std::uint_fast32_t _num_values = 0;
};

/// @brief Json composer user: Object
///
class Node_Object : public Node
{
  public:
  // -- Construction

  Node_Object ( Composer & composer_n );

  Node_Object ( Node & parent_n );

  ~Node_Object () {}

  // -- Value writers

  // - String value
  void
  value ( std::string_view key_n, std::string_view value_n );

  void
  value ( std::string_view key_n, const char * value_n )
  {
    value ( key_n, std::string_view ( value_n ) );
  }

  // - Double value value
  void
  value ( std::string_view key_n, double value_n );

  // - Boolean value value
  void
  value ( std::string_view key_n, bool value_n );

  // - Creates a string from a binary number
  void
  value_number ( std::string_view key_n, std::int32_t value_n );

  // - Creates a string from a binary number
  void
  value_number ( std::string_view key_n, std::uint32_t value_n );

  // - Creates a string from a binary number
  void
  value_number ( std::string_view key_n, std::int64_t value_n );

  // - Creates a string from a binary number
  void
  value_number ( std::string_view key_n, std::uint64_t value_n );

  private:
  std::string_view
  bool_value_string ( bool value_n );

  void
  append_value ( std::string_view key_n, std::string_view value_n );
};

/// @brief Json composer user: Object
///
/// Must be destroyed before Composer::string() is used.
///
class Object : public Node_Object
{
  public:
  // -- Construction

  Object ( Composer & composer_n );

  Object ( Node & parent_n, std::string_view key_n );

  ~Object ();
};

/// @brief Json composer root object
///
class Root_Object : private Composer, public Node_Object
{
  public:
  // -- Construction
  Root_Object ();

  // -- Utility
  /// @brief Finish and return the Json string
  const std::string &
  finish ();
};

} // namespace sev::json::composer
