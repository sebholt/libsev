/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/json/importer/private.hpp>
#include <sev/math/numbers.hpp>
#include <sev/string/utility.hpp>
#include <sev/unicode/convert.hpp>
#include <limits>
#include <string>

namespace
{

// - Control characters
constexpr char32_t uc32_byte_order_mark = 0xfeff;
constexpr char32_t uc32_line_feed = '\n';
constexpr char32_t uc32_object_begin = '{';
constexpr char32_t uc32_object_end = '}';
constexpr char32_t uc32_array_begin = '[';
constexpr char32_t uc32_array_end = ']';
constexpr char32_t uc32_string_begin = '"';
constexpr char32_t uc32_string_end = '"';
constexpr char32_t uc32_escape_begin = '\\';
constexpr char32_t uc32_name_separator = ':';
constexpr char32_t uc32_value_separator = ',';

// - Value strings
constexpr std::u32string_view utf32_value_null = U"null";
constexpr std::u32string_view utf32_value_false = U"false";
constexpr std::u32string_view utf32_value_true = U"true";

} // namespace

namespace sev::json::importer
{

// -- Utility

// - Macros
#define CASES_JSON_SPACE                                                       \
  case 0x20:                                                                   \
  case 0x09:                                                                   \
  case 0x0a:                                                                   \
  case 0x0d:

// -- Class methods

static const std::size_t depth_stack_capacity_default = 64;
static const std::size_t depth_stack_capacity_min = 1;

static const std::size_t string_buffer_capacity_default = 32;
static const std::size_t string_buffer_capacity_min = 1;

Private::Private ( sev::json::Importer & importer_n )
: _importer ( importer_n )
, _parse_method ( &Private::pme_start )
, _depth_stack ( depth_stack_capacity_default )
, _string_state ( String_State::PLAIN )
, _string_buffer ( string_buffer_capacity_default )
{
}

Private::~Private () {}

void
Private::set_depth_stack_capacity ( std::size_t size_n )
{
  math::assign_larger ( size_n, depth_stack_capacity_min );
  math::assign_larger ( size_n, _depth_stack.size () );
  _depth_stack.set_capacity ( size_n );
}

void
Private::set_string_buffer_capacity ( std::size_t size_n )
{
  math::assign_larger ( size_n, string_buffer_capacity_min );
  math::assign_larger ( size_n, _string_buffer.size () );
  _string_buffer.set_capacity ( size_n );
}

std::size_t
Private::feed ( std::u32string_view utf32_n )
{
  _interrupt = false;
  const char32_t * it_cur ( utf32_n.begin () );
  const char32_t * it_end ( utf32_n.end () );
  while ( !_interrupt && ( it_cur != it_end ) ) {
    feed ( *it_cur );
    ++it_cur;
  }
  return ( it_cur - utf32_n.begin () );
}

void
Private::interrupt ()
{
  _interrupt = true;
}

void
Private::clear_error ()
{
  _error.clear ();
  _interrupt = false;
}

void
Private::reset ()
{
  _interrupt = false;
  _error.clear ();
  _parse_method = &Private::pme_start;
  _depth_stack.clear ();
  _string_state = String_State::PLAIN;
  _string_buffer.clear ();
}

void
Private::register_error ( Error_Type type_n )
{
  if ( !_error.is_valid () ) {
    // Update state
    _error.set_type ( type_n );
    _interrupt = true;

    // Notify callback
    _importer.ji_error ( _error );
  }
}

void
Private::register_error_bad_char ( char32_t uc32_n [[maybe_unused]] )
{
  register_error ( Error_Type::UNEXPECTED_CHARACTER );
}

void
Private::object_begin ()
{
  if ( !_depth_stack.is_full () ) {
    _depth_stack.emplace_not_full ( Depth_Step::OBJECT );
    _parse_method = &Private::pme_object_enter;
    // Notify callback
    _importer.ji_object_begin ();
  } else {
    register_error ( Error_Type::NESTING_TOO_DEEP );
  }
}

void
Private::object_end ()
{
  _depth_stack.pop_not_empty ();
  if ( !_depth_stack.is_empty () ) {
    // Update parse method according to parent container type
    switch ( _depth_stack.top () ) {
    case Depth_Step::OBJECT:
      _parse_method = &Private::pme_object_find_separator;
      break;
    case Depth_Step::ARRAY:
      _parse_method = &Private::pme_array_find_separator;
      break;
    default:
      break;
    }
  } else {
    // End of root object reached
    _parse_method = &Private::pme_start;
  }
  // Notify callback
  _importer.ji_object_end ();
}

void
Private::object_key_begin ()
{
  string_state_reset ();
  _parse_method = &Private::pme_object_key;
  // Notify callback
  _importer.ji_object_key_begin ();
}

void
Private::object_key_end ()
{
  _parse_method = &Private::pme_object_find_kv_separator;
  // Notify callback
  if ( !_string_buffer.is_empty () ) {
    _importer.ji_object_key_data (
        { &_string_buffer[ 0 ], _string_buffer.size () } );
    // Clear string buffer
    _string_buffer.clear ();
  }
  // Notify callback
  _importer.ji_object_key_end ();
}

void
Private::array_begin ()
{
  if ( !_depth_stack.is_full () ) {
    _depth_stack.emplace_not_full ( Depth_Step::ARRAY );
    _parse_method = &Private::pme_array_enter;
    // Notify callback
    _importer.ji_array_begin ();
  } else {
    register_error ( Error_Type::NESTING_TOO_DEEP );
  }
}

void
Private::array_end ()
{
  _depth_stack.pop_not_empty ();
  if ( !_depth_stack.is_empty () ) {
    // Update parse method according to parent container type
    switch ( _depth_stack.top () ) {
    case Depth_Step::OBJECT:
      _parse_method = &Private::pme_object_find_separator;
      break;
    case Depth_Step::ARRAY:
      _parse_method = &Private::pme_array_find_separator;
      break;
    default:
      break;
    }
  } else {
    // End of root array reached
    _parse_method = &Private::pme_start;
  }
  // Notify callback
  _importer.ji_array_end ();
}

void
Private::value_plain_begin ( char32_t uc32_n )
{
  string_state_reset ();
  _string_buffer.push_not_full ( uc32_n );
  _parse_method = &Private::pme_value_plain;
}

void
Private::value_plain_end ( bool separator_found_n )
{
  // Update parse method according to parent container type
  switch ( _depth_stack.top () ) {
  case Depth_Step::OBJECT:
    if ( separator_found_n ) {
      _parse_method = &Private::pme_object_find_key;
    } else {
      _parse_method = &Private::pme_object_find_separator;
    }
    break;
  case Depth_Step::ARRAY:
    if ( separator_found_n ) {
      _parse_method = &Private::pme_find_value;
    } else {
      _parse_method = &Private::pme_array_find_separator;
    }
    break;
  default:
    break;
  }

  // Parse binary value from string buffer
  {
    const std::u32string_view utf32_view{ _string_buffer.data (),
                                          _string_buffer.size () };
    if ( utf32_view == utf32_value_null ) {
      _importer.ji_object_value_plain ( utf32_view, Plain_Value () );
    } else if ( utf32_view == utf32_value_false ) {
      _importer.ji_object_value_plain ( utf32_view, Plain_Value ( false ) );
    } else if ( utf32_view == utf32_value_true ) {
      _importer.ji_object_value_plain ( utf32_view, Plain_Value ( true ) );
    } else {
      sev::unicode::convert ( _number_string, utf32_view );
      double dval = 0.0;
      if ( sev::string::get_number ( dval, _number_string ) ) {
        _importer.ji_object_value_plain ( utf32_view, Plain_Value ( dval ) );
      } else {
        register_error ( Error_Type::VALUE_INVALID );
      }
    }
  }
}

void
Private::value_string_begin ()
{
  string_state_reset ();
  _parse_method = &Private::pme_value_string;
  // Notify callback
  _importer.ji_value_string_begin ();
}

void
Private::value_string_end ()
{
  // Forward remaining characters
  if ( !_string_buffer.is_empty () ) {
    // Notify callback
    _importer.ji_value_string_data (
        { _string_buffer.data (), _string_buffer.size () } );
  }

  // Update parse method according to parent container type
  switch ( _depth_stack.top () ) {
  case Depth_Step::OBJECT:
    _parse_method = &Private::pme_object_find_separator;
    break;
  case Depth_Step::ARRAY:
    _parse_method = &Private::pme_array_find_separator;
    break;
  default:
    break;
  }

  // Notify callback
  _importer.ji_value_string_end ();
}

void
Private::string_state_reset ()
{
  _string_state = String_State::PLAIN;
  _string_code_point.clear ();
  _string_buffer.clear ();
}

char32_t
Private::string_escaped_single_char ( char32_t uc32_n )
{
  char32_t res ( 0 );
  switch ( uc32_n ) {
  case '"':
    res = 0x22;
    break;
  case '\\':
    res = 0x5c;
    break;
  case '/':
    res = 0x2f;
    break;
  case 'b':
    res = 0x08;
    break;
  case 'f':
    res = 0x0c;
    break;
  case 'n':
    res = 0x0a;
    break;
  case 'r':
    res = 0x0d;
    break;
  case 't':
    res = 0x09;
    break;
  default:
    break;
  }
  return res;
}

char32_t
Private::string_escaped_code_point () const
{
  char32_t res ( 0 );
  std::uint32_t shift ( ( _string_code_point.size () - 1 ) * 4 );
  for ( const char32_t cpt : _string_code_point ) {
    std::uint32_t val ( 0 );
    if ( ( cpt >= '0' ) && ( cpt <= '9' ) ) {
      val = ( cpt - '0' );
    } else if ( ( cpt >= 'a' ) && ( cpt <= 'f' ) ) {
      val = ( cpt - 'a' ) + 0x0a;
    } else if ( ( cpt >= 'A' ) && ( cpt <= 'F' ) ) {
      val = ( cpt - 'A' ) + 0x0a;
    }
    res |= ( val << shift );
    shift -= 4;
  }
  return res;
}

void
Private::string_escaped_feed ( char32_t uc32_n )
{
  switch ( _string_state ) {
  case String_State::PLAIN:
    // Plain character
    switch ( uc32_n ) {
    case uc32_string_end:
      _string_state = String_State::CLOSED;
      break;
    case uc32_escape_begin:
      _string_state = String_State::ESCAPE_NEXT;
      break;
    default:
      // Accept plain character
      if ( !_string_buffer.is_full () ) {
        _string_buffer.emplace_not_full ( uc32_n );
      } else {
        _string_state = String_State::ERROR_OVERFLOW;
      }
      break;
    }
    break;
  case String_State::ESCAPE_NEXT:
    // Escape next character(s)
    switch ( uc32_n ) {
    case 'u':
      // Multi character escape sequence
      _string_state = String_State::ESCAPE_CODE_POINT;
      break;
    default:
      // Single character escape sequence
      _string_state = String_State::PLAIN;
      {
        const char32_t uc32_esc ( string_escaped_single_char ( uc32_n ) );
        if ( uc32_esc != 0 ) {
          // Accept character
          if ( !_string_buffer.is_full () ) {
            _string_buffer.emplace_not_full ( uc32_esc );
          } else {
            _string_state = String_State::ERROR_OVERFLOW;
          }
        } else {
          // Invalid escape sequence
          register_error ( Error_Type::STRING_MALFORMED );
        }
      }
      break;
    }
    break;
  case String_State::ESCAPE_CODE_POINT:
    // Inside multi character escape sequence
    switch ( uc32_n ) {
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case 'a':
    case 'b':
    case 'c':
    case 'd':
    case 'e':
    case 'f':
    case 'A':
    case 'B':
    case 'C':
    case 'D':
    case 'E':
    case 'F':
      // Good hex char
      _string_code_point.push_not_full ( uc32_n );
      if ( _string_code_point.is_full () ) {
        // Accept character
        if ( !_string_buffer.is_full () ) {
          _string_buffer.emplace_not_full ( string_escaped_code_point () );
          _string_state = String_State::PLAIN;
        } else {
          _string_state = String_State::ERROR_OVERFLOW;
        }
        // Clear code point buffer
        _string_code_point.clear ();
      }
      break;
    default:
      // Unexpected non hex character
      register_error ( Error_Type::STRING_MALFORMED );
      break;
    }
    break;
  default:
    break;
  }
}

bool
Private::find_value ( char32_t uc32_n )
{
  switch ( uc32_n ) {
    CASES_JSON_SPACE
    // Ignore whitespace
    break;
  case 'n':
  case 'f':
  case 't':
  case '-':
  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
    value_plain_begin ( uc32_n );
    break;
  case uc32_array_begin:
    // Array value
    array_begin ();
    break;
  case uc32_object_begin:
    // Object value
    object_begin ();
    break;
  case uc32_string_begin:
    // String value
    value_string_begin ();
    break;
  default:
    // Invalid character
    return false;
  }
  return true;
}

void
Private::pme_start ( char32_t uc32_n )
{
  switch ( uc32_n ) {
    CASES_JSON_SPACE
    // Ignore whitespace at the beginning
    break;
  case uc32_byte_order_mark:
    // Allow byte order marks
    break;
  case uc32_object_begin:
    // Root object begin
    object_begin ();
    break;
  case uc32_array_begin:
    // Root array begin
    array_begin ();
    break;
  default:
    // Unexpected character
    register_error_bad_char ( uc32_n );
  }
}

/// @brief Find first member or object end
void
Private::pme_object_enter ( char32_t uc32_n )
{
  switch ( uc32_n ) {
    CASES_JSON_SPACE
    // Ignore whitespace
    break;
  case uc32_string_begin:
    // Key string begin
    object_key_begin ();
    break;
  case uc32_object_end:
    // End of empty object
    object_end ();
    break;
  default:
    // Unexpected character
    register_error_bad_char ( uc32_n );
    break;
  }
}

/// @brief Find next member after a separator
void
Private::pme_object_find_key ( char32_t uc32_n )
{
  switch ( uc32_n ) {
    CASES_JSON_SPACE
    // Ignore whitespace
    break;
  case uc32_string_begin:
    // Key string begin
    object_key_begin ();
    break;
  default:
    // Unexpected character
    register_error_bad_char ( uc32_n );
    break;
  }
}

void
Private::pme_object_key ( char32_t uc32_n )
{
  string_escaped_feed ( uc32_n );
  if ( _string_buffer.is_full () ) {
    // Notify callback
    _importer.ji_object_key_data (
        { _string_buffer.data (), _string_buffer.size () } );
    // Clear string buffer
    _string_buffer.clear ();
  } else {
    switch ( _string_state ) {
    case String_State::CLOSED:
      object_key_end ();
      break;
    case String_State::ERROR_OVERFLOW:
      // Error: Key too long
      register_error ( Error_Type::KEY_TOO_LONG );
      break;
    default:
      break;
    }
  }
}

void
Private::pme_object_find_kv_separator ( char32_t uc32_n )
{
  switch ( uc32_n ) {
    CASES_JSON_SPACE
    // Ignore whitespace
    return;
  case uc32_name_separator:
    // Member separator
    _parse_method = &Private::pme_find_value;
    break;
  default:
    // Unexpected character
    register_error_bad_char ( uc32_n );
    break;
  }
}

void
Private::pme_object_find_separator ( char32_t uc32_n )
{
  switch ( uc32_n ) {
    CASES_JSON_SPACE
    // Ignore whitespace
    return;
  case uc32_value_separator:
    // Member separator
    _parse_method = &Private::pme_object_find_key;
    break;
  case uc32_object_end:
    // End of object
    object_end ();
    break;
  default:
    // Unexpected character
    register_error_bad_char ( uc32_n );
    break;
  }
}

void
Private::pme_array_enter ( char32_t uc32_n )
{
  if ( !find_value ( uc32_n ) ) {
    switch ( uc32_n ) {
    case uc32_array_end:
      // End of empty array
      array_end ();
      break;
    default:
      // Unexpected character
      register_error_bad_char ( uc32_n );
      break;
    }
  }
}

void
Private::pme_array_find_separator ( char32_t uc32_n )
{
  switch ( uc32_n ) {
    CASES_JSON_SPACE
    // Ignore whitespace
    break;
  case uc32_value_separator:
    // Member separator
    _parse_method = &Private::pme_find_value;
    break;
  case uc32_array_end:
    // End of array
    array_end ();
    break;
  default:
    // Unexpected character
    register_error_bad_char ( uc32_n );
    break;
  }
}

void
Private::pme_find_value ( char32_t uc32_n )
{
  if ( !find_value ( uc32_n ) ) {
    // Unexpected character
    register_error_bad_char ( uc32_n );
  }
}

void
Private::pme_value_plain ( char32_t uc32_n )
{
  switch ( uc32_n ) {
    CASES_JSON_SPACE
  case uc32_value_separator:
    value_plain_end ( uc32_n == uc32_value_separator );
    // End of value reached
    break;
  default:
    if ( !_string_buffer.is_full () ) {
      _string_buffer.emplace_not_full ( uc32_n );
    } else {
      // Error: Value too long
      register_error ( Error_Type::VALUE_TOO_LONG );
    }
    break;
  }
}

void
Private::pme_value_string ( char32_t uc32_n )
{
  string_escaped_feed ( uc32_n );
  if ( _string_buffer.is_full () ) {
    // Notify callback
    _importer.ji_value_string_data (
        { _string_buffer.data (), _string_buffer.size () } );
    // Clear string buffer
    _string_buffer.clear ();
  } else {
    switch ( _string_state ) {
    case String_State::CLOSED:
      // End value string state
      value_string_end ();
      break;
    default:
      break;
    }
  }
}

} // namespace sev::json::importer
