/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/json/importer.hpp>
#include <sev/json/importer/error.hpp>
#include <sev/mem/stack_fixed.hpp>
#include <sev/mem/stack_static.hpp>
#include <array>
#include <string_view>

namespace sev::json::importer
{

/// @brief Converts a string value into numeric values
///
class Private
{
  public:
  // -- Types

  enum class String_State
  {
    PLAIN,
    ESCAPE_NEXT,
    ESCAPE_CODE_POINT,
    CLOSED,
    ERROR_OVERFLOW
  };

  enum class Depth_Step : std::uint8_t
  {
    OBJECT,
    ARRAY
  };

  typedef void ( Private::*Parse_Method ) ( char32_t uc32_n );

  // -- Construction

  Private ( sev::json::Importer & importer_n );

  Private ( const Private & importer_n ) = delete;

  Private ( Private && importer_n ) = default;

  ~Private ();

  // -- Assignment operators

  Private &
  operator= ( const Private & importer_n ) = delete;

  Private &
  operator= ( Private && importer_n ) = delete;

  // -- Accessors

  const Error &
  error () const
  {
    return _error;
  }

  // -- Depth stack capacity

  std::size_t
  depth_stack_capacity () const
  {
    return _depth_stack.capacity ();
  }

  void
  set_depth_stack_capacity ( std::size_t size_n );

  // -- String buffer capacity

  std::size_t
  string_buffer_capacity () const
  {
    return _string_buffer.capacity ();
  }

  void
  set_string_buffer_capacity ( std::size_t size_n );

  // -- Data feed

  std::size_t
  feed ( std::u32string_view utf32_n );

  void
  feed ( char32_t uc32_n )
  {
    // Call parse methods
    ( this->*_parse_method ) ( uc32_n );
  }

  /// @brief Interrupt the feed processing
  void
  interrupt ();

  void
  clear_error ();

  void
  reset ();

  private:
  // -- Error registration

  void
  register_error ( Error_Type type_n );

  void
  register_error_bad_char ( char32_t uc32_n );

  // -- Container type begin/end

  void
  object_begin ();

  void
  object_end ();

  void
  object_key_begin ();

  void
  object_key_end ();

  void
  array_begin ();

  void
  array_end ();

  void
  value_plain_begin ( char32_t uc32_n );

  void
  value_plain_end ( bool separator_found_n );

  void
  value_string_begin ();

  void
  value_string_end ();

  // -- String buffer management

  void
  string_state_reset ();

  char32_t
  string_escaped_single_char ( char32_t uc32_n );

  char32_t
  string_escaped_code_point () const;

  void
  string_escaped_feed ( char32_t uc32_n );

  // -- Value management

  /// @brief Return false if the given character is invalid
  bool
  find_value ( char32_t uc32_n );

  // -- Paser methods

  void
  pme_start ( char32_t uc32_n );

  void
  pme_object_enter ( char32_t uc32_n );

  void
  pme_object_find_key ( char32_t uc32_n );

  void
  pme_object_key ( char32_t uc32_n );

  void
  pme_object_find_kv_separator ( char32_t uc32_n );

  void
  pme_object_find_separator ( char32_t uc32_n );

  void
  pme_array_enter ( char32_t uc32_n );

  void
  pme_array_find_separator ( char32_t uc32_n );

  void
  pme_find_value ( char32_t uc32_n );

  void
  pme_value_plain ( char32_t uc32_n );

  void
  pme_value_string ( char32_t uc32_n );

  private:
  // -- Attributes
  sev::json::Importer & _importer;
  // - Abstract state
  bool _interrupt = false;
  sev::json::importer::Error _error;
  Parse_Method _parse_method;
  // - Depth tracking
  sev::mem::Stack_Fixed< Depth_Step > _depth_stack;
  // - String reading
  String_State _string_state;
  sev::mem::Stack_Static< char32_t, 4 > _string_code_point;
  sev::mem::Stack_Fixed< char32_t > _string_buffer;
  // - String evaluation
  std::string _number_string;
};

} // namespace sev::json::importer
