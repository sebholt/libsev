/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/json/importer.hpp>
#include <sev/json/importer/private.hpp>

namespace sev::json
{

Importer::Importer ()
: _private{ std::make_unique< importer::Private > ( *this ) }
{
}

Importer::~Importer () {}

std::size_t
Importer::depth_stack_capacity () const
{
  return _private->depth_stack_capacity ();
}

void
Importer::set_depth_stack_capacity ( std::size_t size_n )
{
  _private->set_depth_stack_capacity ( size_n );
}

std::size_t
Importer::string_buffer_capacity () const
{
  return _private->string_buffer_capacity ();
}

void
Importer::set_string_buffer_capacity ( std::size_t size_n )
{
  _private->set_string_buffer_capacity ( size_n );
}

std::size_t
Importer::feed ( std::u32string_view utf32_n )
{
  return _private->feed ( utf32_n );
}

void
Importer::feed ( char32_t uc32_n )
{
  return _private->feed ( uc32_n );
}

void
Importer::interrupt ()
{
  _private->interrupt ();
}

void
Importer::reset ()
{
  _private->reset ();
}

const importer::Error &
Importer::error () const
{
  return _private->error ();
}

} // namespace sev::json
