/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/json/importer/error.hpp>
#include <sev/json/plain_value.hpp>
#include <memory>
#include <string_view>

// -- Forward declaration
namespace sev::json::importer
{
class Private;
}

namespace sev::json
{

/// @brief Json importer base class
///
class Importer
{
  public:
  // -- Friends
  friend class sev::json::importer::Private;

  // -- Construction

  Importer ();

  Importer ( const Importer & importer_n ) = delete;

  Importer ( Importer && importer_n ) = delete;

  virtual ~Importer ();

  // -- Assignment operators

  Importer &
  operator= ( const Importer & importer_n ) = delete;

  Importer &
  operator= ( Importer && importer_n ) = delete;

  // -- Depth stack capacity

  std::size_t
  depth_stack_capacity () const;

  void
  set_depth_stack_capacity ( std::size_t size_n );

  // -- String buffer capacity

  std::size_t
  string_buffer_capacity () const;

  void
  set_string_buffer_capacity ( std::size_t size_n );

  // -- Feeding

  /// @brief Feed characters to the parser
  ///
  /// @see error()
  /// @see clear_error()
  /// @return Number of consumed characters
  std::size_t
  feed ( std::u32string_view utf32_n );

  /// @brief Feed a single character
  ///
  /// @see error()
  /// @see clear_error()
  void
  feed ( char32_t uc32_n );

  void
  reset ();

  // -- Error accessors

  const importer::Error &
  error () const;

  /// @brief Clears the error and any interrupt request
  ///
  /// If called from within ji_error() consider calling interrupt().
  void
  clear_error ();

  protected:
  // -- Utility

  /// @brief Interrupt the feed() processing
  ///
  /// Usually called from within a ji_FOO method.
  /// The interrupt happens after the ji_FOO method finished.
  void
  interrupt ();

  // -- Abstract callback interface

  virtual void
  ji_error ( const sev::json::importer::Error & error_n ) = 0;

  virtual void
  ji_object_begin () = 0;

  virtual void
  ji_object_end () = 0;

  virtual void
  ji_array_begin () = 0;

  virtual void
  ji_array_end () = 0;

  virtual void
  ji_object_key_begin () = 0;

  virtual void
  ji_object_key_data ( std::u32string_view utf32_n ) = 0;

  virtual void
  ji_object_key_end () = 0;

  virtual void
  ji_object_value_plain ( std::u32string_view utf32_n,
                          sev::json::Plain_Value value_n ) = 0;

  virtual void
  ji_value_string_begin () = 0;

  virtual void
  ji_value_string_data ( std::u32string_view utf32_n ) = 0;

  virtual void
  ji_value_string_end () = 0;

  private:
  // -- Attributes
  std::unique_ptr< importer::Private > _private;
};

} // namespace sev::json
