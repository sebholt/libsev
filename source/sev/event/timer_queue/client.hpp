/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <functional>

namespace sev::event::timer_queue
{

// -- Forward declaration
template < class CLK >
class Queue;

/// @brief Timer client run state
enum class Client_Run_State
{
  IDLE,
  SINGLE,
  PERIOD
};

/// @brief Client for a timer queue
///
template < class CLK >
class Client
{
  public:
  // -- Types

  typedef CLK Clock;
  typedef typename Clock::time_point Time_Point;
  typedef typename Clock::duration Duration;
  typedef typename sev::event::timer_queue::Queue< CLK > Queue;

  friend Queue;

  public:
  // -- Constructors

  Client ();

  Client ( Queue * queue_n );

  Client ( Queue * queue_n, std::function< void () > callback_n );

  ~Client ();

  // -- Queue connection

  Queue *
  queue () const
  {
    return _queue;
  }

  bool
  is_connected () const
  {
    return ( _queue != nullptr );
  }

  /// @brief Connect to a queue
  /// @return True on connection success
  bool
  connect ( Queue * queue_n );

  /// @brief Disconnect from queue
  void
  disconnect ();

  // -- Callback client

  const std::function< void () > &
  callback () const
  {
    return _callback;
  }

  /// @brief Stops the timer and sets the callback
  void
  set_callback ( std::function< void () > callback_n );

  void
  clear_callback ();

  // -- Time

  /// @brief Clock::now()
  static Time_Point
  now ()
  {
    return Clock::now ();
  }

  // -- Time point

  /// @brief Time point of the next timeout
  const Time_Point &
  point () const
  {
    return _point;
  }

  /// @brief Stops and sets the time point for the next timeout
  void
  set_point ( const Time_Point & time_point_n );

  /// @brief Stops and increments point() by duration_n
  void
  adjust_point ( const Duration & duration_n );

  // -- Period duration

  /// @brief Period duration
  const Duration &
  duration () const
  {
    return _duration;
  }

  /// @brief Stops the timer and sets the period duration
  void
  set_duration ( const Duration & duration_n );

  // -- Run state / start / stop

  Client_Run_State
  run_state () const
  {
    return _run_state;
  }

  bool
  is_running () const
  {
    return ( _run_state != Client_Run_State::IDLE );
  }

  bool
  is_running_single () const
  {
    return ( _run_state == Client_Run_State::SINGLE );
  }

  bool
  is_running_period () const
  {
    return ( _run_state == Client_Run_State::PERIOD );
  }

  // -- Single notification

  /// @brief Stops and request a single notification at point()
  void
  start_single_point ();

  /// @brief Stops, sets point to time_point_n
  ///        and request a single notification at point()
  void
  start_single_point ( const Time_Point & time_point_n );

  /// @brief Stops, sets point to Clock::now()
  ///        and request a single notification at point()
  ///
  /// Requests immediate processing
  void
  start_single_point_now ();

  /// @brief Stops, sets point() to ( time_point_n + duration() )
  ///        and requests a single notification at point()
  void
  start_single_period ( const Time_Point & time_point_n );

  /// @brief Stops, sets point() to ( point() + duration() )
  ///        and requests a single notification at point()
  void
  start_single_period_next ();

  /// @brief Stops, sets duration() to duration_n
  ///        then sets point() to ( point() +  duration() )
  ///        and requests a single notification at point()
  void
  start_single_period_next ( const Duration & duration_n );

  /// @brief Stops, sets point() to ( now() + duration() )
  ///        and requests a single notification at point()
  void
  start_single_period_now ();

  /// @brief Stops, sets duration() to duration_n
  ///        then sets point() to ( now() +  duration() )
  ///        and requests a single notification at point()
  void
  start_single_period_now ( const Duration & duration_n );

  // -- Periodical notification

  /// @brief Stops and request periodical notifications with the first
  ///        notification at point().
  ///        point() gets updated for the next period() after
  ///        the callback() notification.
  void
  start_periodical_at_point ();

  /// @brief Stops, sets point() to time_point_n
  ///        and request periodical notifications with the first
  ///        notification at point().
  ///        point() gets updated for the next period() after
  ///        the callback() notification.
  void
  start_periodical_at ( const Time_Point & time_point_n );

  /// @brief Stops, sets point to ( point() + duration() )
  ///        and request periodical notifications with the first
  ///        notification at point().
  ///        point() gets updated for the next period() after
  ///        the callback() notification.
  void
  start_periodical_next ();

  /// @brief Stops, sets point to ( now() + duration() )
  ///        and request periodical notifications with the first
  ///        notification at point().
  ///        point() gets updated for the next period() after
  ///        the callback() notification.
  void
  start_periodical_now ();

  /// @brief Stops, sets point to ( time_now_n + duration() )
  ///        and request periodical notifications with the first
  ///        notification at point().
  ///        point() gets updated for the next period() after
  ///        the callback() notification.
  void
  start_periodical_now ( const Time_Point & time_now_n );

  /// @brief Stops any further notifications.
  void
  stop ();

  private:
  Queue * _queue = nullptr;
  std::function< void () > _callback;
  Client_Run_State _run_state;
  Time_Point _point;
  Duration _duration;
};

} // namespace sev::event::timer_queue
