/// libsev: C++ library for threads, math strings and trees.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/event/timer_queue/client.hpp>
#include <sev/event/timer_queue/queue.hpp>

namespace sev::event::timer_queue
{

template < class CLK >
Queue< CLK >::Queue ()
: _callback_front ( []() {} )
{
}

template < class CLK >
Queue< CLK >::~Queue ()
{
  assert ( _reference_count == 0 );
}

template < class CLK >
void
Queue< CLK >::ensure_minimum_capacity ( std::size_t num_clients_n )
{
  _queue.ensure_minimum_capacity ( num_clients_n );
}

template < class CLK >
std::size_t
Queue< CLK >::process_timeouts ( Time_Point time_now_n )
{
  std::size_t res ( 0 );
  while ( !_queue.is_empty () ) {
    const typename Timeout_Heap::Item & ifront ( _queue.front () );
    if ( ifront.key () <= time_now_n ) {
      // Pop client from queue
      Client< CLK > * client ( ifront.value () );
      _queue.pop_front_not_empty ();
      // -- Process client timeout
      process_timeout ( client );
      ++res;
    } else {
      // Lowest time point is in the future
      break;
    }
  }
  return res;
}

template < class CLK >
inline void
Queue< CLK >::process_timeout ( Client< CLK > * client_n )
{
  // -- Update client run state
  switch ( client_n->run_state () ) {
  case Client_Run_State::SINGLE:
    // Stop client
    client_n->_run_state = Client_Run_State::IDLE;
    break;
  case Client_Run_State::PERIOD:
    // Register client for another period
    client_n->_point += client_n->_duration;
    // Insert into heap
    _queue.insert_not_full ( client_n->_point, client_n );
    break;
  default:
    break;
  }

  // -- Notify client callback as last action
  client_n->callback () ();
}

template < class CLK >
void
Queue< CLK >::set_callback_front ( std::function< void() > callback_n )
{
  _callback_front = std::move ( callback_n );
  if ( !_callback_front ) {
    _callback_front = []() {};
  }
}

template < class CLK >
void
Queue< CLK >::client_register ()
{
  ensure_minimum_capacity ( _reference_count + 1 );
  ++_reference_count;
}

template < class CLK >
void
Queue< CLK >::client_unregister ()
{
  // Remove client from registers
  DEBUG_ASSERT ( _reference_count != 0 );
  --_reference_count;
}

template < class CLK >
void
Queue< CLK >::client_start ( Client< CLK > * client_n )
{
  _queue.insert_not_full ( client_n->point (), client_n );
  // If this became the front time notify the front change callback
  if ( _queue.front ().value () == client_n ) {
    _callback_front ();
  }
}

template < class CLK >
void
Queue< CLK >::client_stop ( Client< CLK > * client_n )
{
  // Remove client from registers
  const bool was_first ( _queue.front ().value () == client_n );
  _queue.remove_value_match ( client_n );
  // If was the front time notify the front change callback
  if ( was_first ) {
    _callback_front ();
  }
}

} // namespace sev::event::timer_queue
