/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/timer_queue/queue.hpp>
#include <chrono>

namespace sev::event::timer_queue::chrono
{

// -- Types
typedef Queue< std::chrono::steady_clock > Queue_Steady;
typedef Queue< std::chrono::system_clock > Queue_System;

} // namespace sev::event::timer_queue::chrono
