/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/event/timer_queue/client.hpp>
#include <sev/event/timer_queue/client.tcc>
#include <chrono>

namespace sev::event::timer_queue
{

// -- Instantiation
template class Client< std::chrono::steady_clock >;
template class Client< std::chrono::system_clock >;

} // namespace sev::event::timer_queue
