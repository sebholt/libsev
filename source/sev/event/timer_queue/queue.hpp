/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/assert.hpp>
#include <sev/mem/heap.hpp>
#include <cstddef>
#include <functional>

namespace sev::event::timer_queue
{

// -- Forward declaration
template < class CLK >
class Client;

/// @brief Timer priority queue
///
/// Fast but not thread safe
///
template < class CLK >
class Queue
{
  public:
  // -- Types

  typedef CLK Clock;
  typedef typename Clock::time_point Time_Point;
  typedef typename Clock::duration Duration;
  typedef sev::mem::Heap< Time_Point, Client< CLK > * > Timeout_Heap;

  friend class Client< CLK >;

  public:
  // -- Constructors

  Queue ();

  ~Queue ();

  // -- Clock

  /// @brief Clock accessor
  ///
  Clock &
  clock ()
  {
    return _clock;
  }

  /// @brief Clock accessor
  ///
  const Clock &
  clock () const
  {
    return _clock;
  }

  /// @brief Current time
  ///
  Time_Point
  now () const
  {
    return _clock.now ();
  }

  // -- Client statistics

  /// @brief Number of clients
  ///
  std::size_t
  reference_count () const
  {
    return _reference_count;
  }

  /// @brief Ensure enough memory is allocated to support at least
  /// num_clients_n clients
  void
  ensure_minimum_capacity ( std::size_t num_clients_n );

  // -- Timeout queue processing

  /// @brief Number of queued time points
  ///
  std::size_t
  size () const
  {
    return _queue.size ();
  }

  /// @brief Tests if there are queued time points
  ///
  bool
  is_empty () const
  {
    return _queue.is_empty ();
  }

  /// @brief Queued time point with the lowest time value
  ///
  const Time_Point &
  front () const
  {
    DEBUG_ASSERT ( !is_empty () );
    return _queue.front ().key ();
  }

  /// @brief Client at the front() of the queue
  ///
  Client< CLK > *
  front_client () const
  {
    DEBUG_ASSERT ( !is_empty () );
    return _queue.front ().value ();
  }

  /// @brief Duration until the queue_front_point() is met (
  /// queue_front_point()
  /// - now() ).
  ///
  Duration
  front_duration_from_now ()
  {
    DEBUG_ASSERT ( !is_empty () );
    return ( front () - now () );
  }

  // -- Timeout processing

  /// @brief Processes all timeouts that elapsed until time_now_n
  /// @return Number of elapsed timeouts
  std::size_t
  process_timeouts ( Time_Point time_now_n );

  /// @brief Calls process_timeouts() with now()
  /// @return Number of elapsed timeouts
  std::size_t
  process_timeouts_now ()
  {
    return process_timeouts ( now () );
  }

  // -- Callbacks

  /// @brief Gets notified when the front() time changes
  ///
  /// Only gets called when clients adds or removes a time point.
  /// Not called during process_timeouts()
  ///
  const std::function< void () > &
  callback_front () const
  {
    return _callback_front;
  }

  void
  set_callback_front ( std::function< void () > callback_n );

  private:
  /// @brief Process the timeout of the client
  void
  process_timeout ( Client< CLK > * client_n );

  // -- Client interface

  void
  client_register ();

  void
  client_unregister ();

  /// @brief Insert client time point into timeout list
  void
  client_start ( Client< CLK > * client_n );

  /// @brief Remove client time point from timeout list
  void
  client_stop ( Client< CLK > * client_n );

  private:
  /// @brief Clock instance
  Clock _clock;
  // -- Client registers
  Timeout_Heap _queue;
  // -- Callbacks
  // @brief Front time change callback
  std::function< void () > _callback_front;
  /// @brief Reference count
  std::size_t _reference_count = 0;
};

} // namespace sev::event::timer_queue
