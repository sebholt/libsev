/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/assert.hpp>
#include <sev/event/event.hpp>
#include <sev/event/pool_base.hpp>
#include <sev/event/pool_tracker.hpp>

namespace sev::event
{

Pool_Base::Pool_Base ( Pool_Tracker & tracker_n )
: _capacity ( capacity_default )
, _tracker ( tracker_n )
{
  _tracker.pool_register ( this );
}

Pool_Base::~Pool_Base ()
{
  DEBUG_ASSERT ( all_home () );
  DEBUG_ASSERT ( is_empty () );
  _tracker.pool_unregister ( this );
}

void
Pool_Base::ensure_minimum_capacity ( std::size_t capacity_n )
{
  if ( capacity () < capacity_n ) {
    this->set_capacity ( capacity_n );
  }
}

void
Pool_Base::ensure_maximum_capacity ( std::size_t capacity_n )
{
  if ( capacity () > capacity_n ) {
    this->set_capacity ( capacity_n );
  }
}

void
Pool_Base::clear_capacity ()
{
  this->set_capacity ( 0 );
}

} // namespace sev::event
