/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "thread.hpp"
#include <sev/event/timer_server/detail/entry.hpp>
#include <sev/event/timer_server/detail/private.hpp>
#include <chrono>

namespace sev::event::timer_server::detail
{

template < class CLK >
Thread< CLK >::Thread ( Private * private_n )
: _private ( private_n )
, _mutex ( private_n->mutex () )
{
}

template < class CLK >
Thread< CLK >::~Thread ()
{
}

template < class CLK >
void
Thread< CLK >::begin ()
{
  std::lock_guard< std::mutex > lock ( _mutex );
  _abort = false;
}

template < class CLK >
void
Thread< CLK >::abort ()
{
  std::lock_guard< std::mutex > lock ( _mutex );
  if ( !_abort ) {
    _abort = true;
    _cond.notify_one ();
  }
}

template < class CLK >
void
Thread< CLK >::operator() ()
{
  std::unique_lock< std::mutex > lock ( _mutex );
  while ( !_abort ) {
    const Time_Point * tnear ( _private->nearest_time () );
    if ( tnear != nullptr ) {
      // Pass a copy of the time point
      _cond.wait_until ( lock, Time_Point ( *tnear ) );
      _private->process_timeouts ();
    } else {
      // Wait for abort or new time notification
      _cond.wait ( lock );
    }
  }
}

// -- Instantiation

template class Thread< std::chrono::steady_clock >;
template class Thread< std::chrono::system_clock >;

} // namespace sev::event::timer_server::detail
