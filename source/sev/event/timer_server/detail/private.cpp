/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "private.hpp"
#include <sev/event/timer_server/detail/thread.hpp>
#include <sev/thread/thread.hpp>
#include <sev/unicode/view.hpp>
#include <chrono>
#include <cstddef>

namespace sev::event::timer_server::detail
{

template < class CLK >
Private< CLK >::Private ( sev::thread::Tracker * thread_tracker_n )
: _private_thread ( std::make_unique< Thread< CLK > > ( this ) )
, _thread ( "Timer", thread_tracker_n )
{
}

template < class CLK >
Private< CLK >::~Private ()
{
  stop ();
}

template < class CLK >
bool
Private< CLK >::start ()
{
  if ( is_running () ) {
    return true;
  }
  _private_thread->begin ();
  _thread.start ( [ pt = _private_thread.get () ] () { ( *pt ) (); } );
  return true;
}

template < class CLK >
void
Private< CLK >::stop ()
{
  if ( !is_running () ) {
    return;
  }
  _private_thread->abort ();
  _thread.join ();
}

template < class CLK >
inline void
Private< CLK >::util_client_stop ( Entry * entry_n, bool & wake_thread_n )
{
  // -- Remove registered timeout on demand
  if ( entry_n->clear_run_state () ) {
    // Check if this is the front entry
    if ( _timeouts.front ().value () == entry_n ) {
      wake_thread_n = true;
    }
    // Remove from register
    _timeouts.remove_value_match ( entry_n );
  }
}

template < class CLK >
inline void
Private< CLK >::util_wake_thread (
    bool wake_thread_n, const std::lock_guard< std::mutex > & lock_n )
{
  if ( wake_thread_n ) {
    _private_thread->wake_up ( lock_n );
  }
}

template < class CLK >
void
Private< CLK >::client_set_callback (
    Entry * entry_n, const std::function< void () > & callback_notifier_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  // -- Remove registered entry timeout
  bool wthread ( false );
  util_client_stop ( entry_n, wthread );
  // -- Update entry callback
  entry_n->callback_notifier = callback_notifier_n;
  // -- Wake thread on demand
  util_wake_thread ( wthread, lock );
}

template < class CLK >
typename Private< CLK >::Entry *
Private< CLK >::client_register ()
{
  Entry * res ( 0 );
  // Ensure thread is allocated
  if ( _private_thread ) {
    std::lock_guard< std::mutex > lock ( _mutex );
    // Ensure capacities
    const std::size_t nsize ( _entries.size () + 1 );
    _timeouts.ensure_minimum_capacity ( nsize );
    _entries_periodical.ensure_minimum_capacity ( nsize );
    {
      // Insert point
      _entries.emplace_back ( std::make_unique< Entry > () );
      res = _entries.back ().get ();
    }
  }
  return res;
}

template < class CLK >
void
Private< CLK >::client_unregister ( Entry * entry_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  // -- Remove registered entry timeout
  bool wthread ( false );
  util_client_stop ( entry_n, wthread );
  // -- Remove client from list
  {
    auto it =
        std::find_if ( _entries.begin (),
                       _entries.end (),
                       [ entry_n ] ( std::unique_ptr< Entry > const & ptr_n ) {
                         return ptr_n.get () == entry_n;
                       } );
    if ( it != _entries.end () ) {
      _entries.erase ( it );
    }
  }
  // -- Wake thread on demand
  util_wake_thread ( wthread, lock );
}

template < class CLK >
void
Private< CLK >::client_insert_point ( Entry * entry_n,
                                      const Time_Point & time_point_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  // -- Remove registered entry timeout
  bool wthread ( false );
  util_client_stop ( entry_n, wthread );
  // -- Insert and update
  if ( entry_n->callback_notifier ) {
    // Update entry state
    entry_n->set_run_state_single ( time_point_n );
    // Insert new timeout
    _timeouts.insert_not_full ( time_point_n, entry_n );
    // Wake thread if this is the nearest timeout
    if ( _timeouts.front ().value () == entry_n ) {
      wthread = true;
    }
  }
  // -- Wake thread on demand
  util_wake_thread ( wthread, lock );
}

template < class CLK >
void
Private< CLK >::client_insert_period ( Entry * entry_n,
                                       const Time_Point & time_point_n,
                                       const Duration & duration_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  // -- Remove registered entry timeout
  bool wthread ( false );
  util_client_stop ( entry_n, wthread );
  // -- Insert and update
  if ( entry_n->callback_notifier ) {
    // Update entry state
    entry_n->set_run_state_period ( time_point_n, duration_n );
    // Insert new timeout
    _timeouts.insert_not_full ( time_point_n, entry_n );
    // Wake thread if this is the nearest timeout
    if ( _timeouts.front ().value () == entry_n ) {
      wthread = true;
    }
  }
  // --Wake thread on demand
  util_wake_thread ( wthread, lock );
}

template < class CLK >
void
Private< CLK >::client_remove ( Entry * entry_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  // -- Remove registered entry timeout
  bool wthread ( false );
  util_client_stop ( entry_n, wthread );
  // -- Wake thread on demand
  util_wake_thread ( wthread, lock );
}

template < class CLK >
void
Private< CLK >::process_timeouts ()
{
  if ( !_timeouts.is_empty () ) {
    // -- Process time out entries
    const Time_Point tnow ( Clock::now () );
    do {
      const typename Timeout_Heap::Item & item ( _timeouts.front () );
      if ( item.key () <= tnow ) {
        // - Process entry
        Entry & entry ( *item.value () );
        entry.callback_notifier ();
        // Check if this entry is periodical
        if ( entry.is_periodical () ) {
          // Periodical
          _entries_periodical.push_back_not_full ( &entry );
        } else {
          // Not periodical
          entry.clear_run_state ();
        }
        // - Pop entry from queue
        _timeouts.pop_front_not_empty ();
      } else {
        break;
      }
    } while ( !_timeouts.is_empty () );

    // -- Reinsert periodical entries
    while ( !_entries_periodical.is_empty () ) {
      Entry * entry ( _entries_periodical.front () );
      _entries_periodical.pop_front_not_empty ();
      entry->time_point += entry->duration;
      _timeouts.insert_not_full ( entry->time_point, entry );
    }
  }
}

// -- Instantiation

template class Private< std::chrono::steady_clock >;
template class Private< std::chrono::system_clock >;

} // namespace sev::event::timer_server::detail
