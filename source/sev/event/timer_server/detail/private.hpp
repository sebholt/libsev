/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/timer_server/detail/entry.hpp>
#include <sev/event/timer_server/server.hpp>
#include <sev/mem/heap.hpp>
#include <sev/mem/ring_fixed.hpp>
#include <sev/thread/thread.hpp>
#include <functional>
#include <memory>
#include <mutex>
#include <vector>

// -- Forward declaration
namespace sev::thread
{
class Tracker;
}
namespace sev::event::timer_server::detail
{
template < class CLK >
class Thread;
}

namespace sev::event::timer_server::detail
{

/// @brief Timer server implementation
///
template < class CLK >
class Private
{
  public:
  // -- Types

  typedef CLK Clock;
  typedef typename Clock::time_point Time_Point;
  typedef typename Clock::duration Duration;

  typedef sev::event::timer_server::Server< CLK > Server;
  typedef sev::event::timer_server::detail::Entry< CLK > Entry;
  typedef sev::mem::Heap< Time_Point, Entry * > Timeout_Heap;
  typedef sev::mem::Ring_Fixed< Entry * > Entry_Ring;
  typedef std::vector< std::unique_ptr< Entry > > Entry_List;

  public:
  // -- Constructors

  Private ( sev::thread::Tracker * thread_tracker_n );

  ~Private ();

  // -- Start / Stop

  bool
  start ();

  void
  stop ();

  bool
  is_running () const
  {
    return _thread.is_running ();
  }

  // -- Client interface

  /// @arg wake_thread_n Set true if the thread should be woken up
  void
  util_client_stop ( Entry * entry_n, bool & wake_thread_n );

  /// @brief Wakes the thread if wake_thread_n is true
  void
  util_wake_thread ( bool wake_thread_n,
                     const std::lock_guard< std::mutex > & lock_n );

  /// @return Non zero pointer on success
  Entry *
  client_register ();

  void
  client_unregister ( Entry * entry_n );

  /// @brief Stops entry and sets the entry callback
  void
  client_set_callback ( Entry * entry_n,
                        const std::function< void () > & callback_notifier_n );

  /// @brief Stops entry and insert time point into timeout list
  void
  client_insert_point ( Entry * entry_n, const Time_Point & time_point_n );

  /// @brief Stops entry and insert time point into timeout list
  void
  client_insert_period ( Entry * entry_n,
                         const Time_Point & time_point_n,
                         const Duration & duration_n );

  /// @brief Remove client time point from timeout list
  void
  client_remove ( Entry * entry_n );

  // -- Thread interface

  /// @brief Main mutex
  std::mutex &
  mutex ()
  {
    return _mutex;
  }

  const Time_Point *
  nearest_time ()
  {
    if ( !_timeouts.is_empty () ) {
      return &_timeouts.front ().key ();
    }
    return nullptr;
  }

  void
  process_timeouts ();

  private:
  std::mutex _mutex;
  // -- Registers
  Timeout_Heap _timeouts;
  Entry_Ring _entries_periodical;
  Entry_List _entries;
  /// @brief Thread instance
  std::unique_ptr< Thread< CLK > > _private_thread;
  sev::thread::Thread _thread;
};

} // namespace sev::event::timer_server::detail
