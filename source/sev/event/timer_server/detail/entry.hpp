/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <atomic>
#include <cstdint>
#include <functional>

namespace sev::event::timer_server::detail
{

/// @brief System clock time point queue client
///
template < class CLK >
class Entry
{
  public:
  // -- Types

  typedef CLK Clock;
  typedef typename Clock::time_point Time_Point;
  typedef typename Clock::duration Duration;

  enum class Run_State : unsigned int
  {
    NONE,
    SINGLE,
    PERIOD
  };

  public:
  // -- Constructors

  Entry ()
  : run_state ( (std::uint8_t)Run_State::NONE )
  , run_state_ext ( (std::uint8_t)Run_State::NONE )
  , time_point ( Time_Point::min () )
  , duration ( 0 )
  {
  }

  /// @brief Clears the run state and returns true if was running
  bool
  clear_run_state ()
  {
    const unsigned int val ( (unsigned int)Run_State::NONE );
    if ( run_state != val ) {
      // Update local state
      run_state = val;
      // Update shared state
      run_state_ext.store ( val );
      return true;
    }
    return false;
  }

  void
  set_run_state_single ( const Time_Point & time_point_n )
  {
    const unsigned int val ( (unsigned int)Run_State::SINGLE );
    time_point = time_point_n;
    run_state = val;
    run_state_ext.store ( val );
  }

  void
  set_run_state_period ( const Time_Point & time_point_n,
                         const Duration & duration_n )
  {
    const unsigned int val ( (unsigned int)Run_State::PERIOD );
    time_point = time_point_n;
    duration = duration_n;
    run_state = val;
    run_state_ext.store ( val );
  }

  bool
  is_periodical ()
  {
    return ( run_state == (unsigned int)Run_State::PERIOD );
  }

  bool
  ext_is_running ()
  {
    return ( run_state_ext.load () != (unsigned int)Run_State::NONE );
  }

  bool
  ext_is_running_single ()
  {
    return ( run_state_ext.load () == (unsigned int)Run_State::SINGLE );
  }

  bool
  ext_is_running_period ()
  {
    return ( run_state_ext.load () == (unsigned int)Run_State::PERIOD );
  }

  public:
  unsigned int run_state;
  std::atomic_uint run_state_ext;
  Time_Point time_point;
  Duration duration;
  std::function< void () > callback_notifier;
};

} // namespace sev::event::timer_server::detail
