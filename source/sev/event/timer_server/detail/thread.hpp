/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <condition_variable>
#include <mutex>

namespace sev::event::timer_server::detail
{

// -- Forward declaration
template < class CLK >
class Private;
template < class CLK >
class Entry;

/// @brief Waits for timeouts and notifies callbacks
///
template < class CLK >
class Thread
{
  public:
  // -- Types

  typedef CLK Clock;
  typedef typename Clock::time_point Time_Point;
  typedef typename Clock::duration Duration;
  typedef sev::event::timer_server::detail::Private< CLK > Private;
  typedef sev::event::timer_server::detail::Entry< CLK > Entry;

  enum class LEvent
  {
    RELOAD = ( 1 << 0 ),
    ABORT = ( 1 << 1 )
  };

  public:
  // -- Constructors

  Thread ( Private * private_n );

  ~Thread ();

  // -- Start / Stop

  void
  begin ();

  void
  abort ();

  /// @arg lock_n Just makes sure that the mutex is locked
  void
  wake_up ( const std::lock_guard< std::mutex > & lock_n [[maybe_unused]] )
  {
    _cond.notify_one ();
  }

  /// @brief Thread main loop
  void
  operator() ();

  private:
  Private * _private = nullptr;
  bool _abort = false;
  std::mutex & _mutex;
  std::condition_variable _cond;
};

} // namespace sev::event::timer_server::detail
