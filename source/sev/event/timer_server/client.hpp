/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <functional>

namespace sev::event::timer_server
{

// -- Forward declaration
template < class CLK >
class Server;

// -- Forward declaration
namespace detail
{
template < class CLK >
class Private;
template < class CLK >
class Entry;
} // namespace detail

/// @brief Client for a system time server
///
template < class CLK >
class Client
{
  public:
  // -- Types

  typedef CLK Clock;
  typedef typename Clock::time_point Time_Point;
  typedef typename Clock::duration Duration;
  typedef sev::event::timer_server::Server< CLK > Server;
  typedef sev::event::timer_server::detail::Private< CLK > Server_Private;
  typedef sev::event::timer_server::detail::Entry< CLK > Server_Entry;

  public:
  // -- Constructors

  Client ();

  Client ( Server * server_n );

  ~Client ();

  // -- Server connection

  bool
  is_connected () const
  {
    return ( _server_private != nullptr );
  }

  /// @brief Connect to a server
  /// @return True on connection success
  bool
  connect ( Server * server_n );

  /// @brief Disconnect from server
  void
  disconnect ();

  // -- Time

  /// @brief Clock::now()
  static Time_Point
  now ()
  {
    return Clock::now ();
  }

  // -- Time point

  /// @brief Time point of the next timeout
  const Time_Point &
  point () const
  {
    return _point;
  }

  /// @brief Stops and sets the time point
  void
  set_point ( const Time_Point & time_point_n );

  /// @brief Stops and increments point() by duration_n
  void
  adjust_point ( const Duration & duration_n );

  // -- Period duration

  /// @brief Period duration
  const Duration &
  duration () const
  {
    return _duration;
  }

  /// @brief Stops the timer and sets the period duration
  void
  set_duration ( const Duration & duration_n );

  // -- Run state / start / stop

  bool
  is_running ();

  bool
  is_running_single ();

  bool
  is_running_period ();

  // -- Single notification

  /// @brief Stops and request a single notification at point()
  void
  start_single_point ();

  /// @brief Stops, sets point to time_point_n
  ///        and request a single notification at point()
  void
  start_single_point ( const Time_Point & time_point_n );

  /// @brief Stops, sets point() to ( time_point_n + duration() )
  ///        and requests a single notification at point()
  void
  start_single_period ( const Time_Point & time_point_n );

  /// @brief Stops, sets point() to ( point() + duration() )
  ///        and requests a single notification at point()
  void
  start_single_period_next ();

  /// @brief Stops, sets point() to ( now() + duration() )
  ///        and requests a single notification at point()
  void
  start_single_period_now ();

  // -- Periodical notification

  /// @brief Stops and request periodical notifications with the first
  ///        notification at point().
  void
  start_periodical_at_point ();

  /// @brief Stops, sets point() to time_point_n
  ///        and request periodical notifications with the first
  ///        notification at point().
  void
  start_periodical_at ( const Time_Point & time_point_n );

  /// @brief Stops, sets point to ( point() + duration() )
  ///        and request periodical notifications with the first
  ///        notification at point().
  void
  start_periodical_next ();

  /// @brief Stops, sets point to ( time_now_n + duration() )
  ///        and request periodical notifications with the first
  ///        notification at point().
  void
  start_periodical_now ( const Time_Point & time_now_n );

  /// @brief Stops, sets point to ( now() + duration() )
  ///        and request periodical notifications with the first
  ///        notification at point().
  void
  start_periodical_now ();

  /// @brief Stops any further notifications.
  void
  stop ();

  // -- Callback client

  const std::function< void () > &
  callback () const
  {
    return _callback_notifier;
  }

  /// @brief Stops the timer and sets the callback
  void
  set_callback ( std::function< void () > callback_n );

  private:
  // -- Attributes
  Server_Private * _server_private = nullptr;
  Server_Entry * _server_entry = nullptr;
  Time_Point _point;
  Duration _duration;
  std::function< void () > _callback_notifier;
};

} // namespace sev::event::timer_server
