/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/timer_server/client.hpp>
#include <chrono>

namespace sev::event::timer_server::chrono
{

// -- Types
typedef Client< std::chrono::steady_clock > Client_Steady;
typedef Client< std::chrono::system_clock > Client_System;

} // namespace sev::event::timer_server::chrono
