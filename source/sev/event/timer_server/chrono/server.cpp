/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "server.hpp"
#include <sev/event/timer_server/server.hpp>
#include <sev/event/timer_server/server.tcc>
#include <chrono>

namespace sev::event::timer_server
{

// -- Instantiation
template class Server< std::chrono::steady_clock >;
template class Server< std::chrono::system_clock >;

} // namespace sev::event::timer_server
