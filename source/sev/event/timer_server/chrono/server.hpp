/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/timer_server/server.hpp>
#include <chrono>
#include <memory>

namespace sev::event::timer_server::chrono
{

// -- Types
typedef Server< std::chrono::steady_clock > Server_Steady;
typedef Server< std::chrono::system_clock > Server_System;

} // namespace sev::event::timer_server::chrono
