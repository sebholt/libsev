/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/thread/tracker.hpp>
#include <memory>

namespace sev::event::timer_server
{

// -- Forward declaration
template < class CLK >
class Client;

// -- Forward declaration
namespace detail
{
template < class CLK >
class Private;
}

/// @brief System clock timer server
///
template < class CLK >
class Server
{
  public:
  // -- Types

  using Private = sev::event::timer_server::detail::Private< CLK >;
  friend class sev::event::timer_server::Client< CLK >;

  public:
  // -- Constructors

  /// @brief Allocates resources but does not start the timer thread
  Server ( sev::thread::Tracker * thread_tracker_n );

  /// @brief Stops the timer thread on demand
  ~Server ();

  // -- Start / Stop

  bool
  start ();

  void
  stop ();

  bool
  is_running () const;

  private:
  std::unique_ptr< detail::Private< CLK > > _private;
};

} // namespace sev::event::timer_server
