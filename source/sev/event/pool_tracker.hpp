/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/event.hpp>
#include <cstddef>
#include <vector>

namespace sev::event
{

// -- Forward declaration
class Pool_Base;

/// @brief Simple event chain tracker
///
class Pool_Tracker
{
  public:
  friend class Pool_Base;

  // -- Types

  using Pool_Register = std::vector< Pool_Base * >;

  // -- Construction

  Pool_Tracker ();

  Pool_Tracker ( const Pool_Tracker & chain_n ) = delete;

  Pool_Tracker ( Pool_Tracker && chain_n ) = delete;

  ~Pool_Tracker ();

  // -- Assignment operators

  Pool_Tracker &
  operator= ( const Pool_Tracker & chain_n ) = delete;

  Pool_Tracker &
  operator= ( Pool_Tracker && chain_n ) = delete;

  // -- Pools statistics

  /// @brief Returns true if all pools' events are back home
  bool
  all_home () const;

  /// @brief Returns true if all pools are empty
  bool
  all_empty () const;

  /// @brief Returns true if any pool is empty
  bool
  any_empty () const;

  /// @brief Returns true if all pools are not empty
  bool
  all_not_empty () const;

  /// @brief Returns true if any pool is not empty
  bool
  any_not_empty () const;

  /// @brief Sum of all events not home in the pools
  std::size_t
  size_outside () const;

  // -- Pools manipulation

  void
  clear_all_instances () const;

  void
  set_all_capacities ( std::size_t capacity_n ) const;

  void
  clear_all_capacities () const;

  // -- Event tests

  /// @brief Returns true if the event is owned by one of the tracked pools.
  bool
  owned ( Event * event_n ) const;

  private:
  // -- Pool registration

  void
  pool_register ( Pool_Base * pool_n );

  void
  pool_unregister ( Pool_Base * pool_n );

  private:
  // -- Attributes
  Pool_Register _pool_register;
};

} // namespace sev::event
