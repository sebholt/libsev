/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <array>
#include <atomic>
#include <cstdint>

namespace sev::event::bit_accus_async
{

/// @brief Writes to a socket for notification and
///        reads from from it when the events are picked up
///
class Socket
{
  public:
  // -- Types

  using Int_Type = std::uint_fast32_t;

  // -- Construction

  Socket ();

  ~Socket ();

  // -- Accessors

  /// @brief Socket fd to listen on for events.
  ///
  int
  socket_fd () const
  {
    return _socket_fd[ 1 ];
  }

  /// @brief Socket event bits to listen for.
  ///
  short
  socket_events () const;

  // -- Bit set/get interface

  void
  set ( Int_Type bits_n );

  /// @brief Read but don't clear the socket or the bits.
  ///
  Int_Type
  get ()
  {
    return _bits.load ();
  }

  /// @brief Fetch and clear the bits and the socket.
  ///
  Int_Type
  fetch_and_clear ();

  private:
  // -- Attributes
  std::atomic< Int_Type > _bits;
  std::array< int, 2 > _socket_fd = { { -1, -1 } };
};

} // namespace sev::event::bit_accus_async
