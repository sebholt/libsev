/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/event/bit_accus_async/callback.hpp>

namespace sev::event::bit_accus_async
{

void
Callback::set_callback ( Function_Type callback_n, bool call_on_demand_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  _callback = std::move ( callback_n );
  if ( _callback && ( _bits != 0 ) && call_on_demand_n ) {
    _callback ();
  }
}

void
Callback::clear_callback ()
{
  std::lock_guard< std::mutex > lock ( _mutex );
  _callback = Function_Type ();
}

void
Callback::set ( Int_Type bits_n )
{
  if ( bits_n != 0 ) {
    std::lock_guard< std::mutex > lock ( _mutex );
    const Int_Type bits_before = _bits;
    _bits |= bits_n;
    if ( ( bits_before == 0 ) && _callback ) {
      _callback ();
    }
  }
}

Callback::Int_Type
Callback::get ()
{
  std::lock_guard< std::mutex > lock ( _mutex );
  return _bits;
}

Callback::Int_Type
Callback::fetch_and_clear ()
{
  Int_Type res = 0;
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    res = _bits;
    _bits = 0;
  }
  return res;
}

} // namespace sev::event::bit_accus_async
