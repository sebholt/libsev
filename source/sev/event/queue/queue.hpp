/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/list.hpp>
#include <sev/thread/reference_count.hpp>
#include <sev/thread/spinlock.hpp>
#include <array>
#include <cstddef>
#include <functional>
#include <mutex>
#include <type_traits>

// -- Forward declaration
namespace sev::event
{
class Event;
}

namespace sev::event::queue
{

// -- Forward declaration
template < std::size_t N >
class Reference_N;
template < std::size_t N >
class Connection_In_N;
template < std::size_t N >
class Connection_Out_N;

/// @brief Event queue with N channels
///
/// The queue supports a single reader and a single writer.
///
template < std::size_t N >
class Queue_N
{
  public:
  // -- Friends
  friend class Reference_N< N >;
  friend class Connection_In_N< N >;
  friend class Connection_Out_N< N >;

  // -- Types
  using Lists = std::array< sev::event::List, N >;

  public:
  // -- Constructors

  Queue_N ();

  /// @brief Disable copy contructor
  ///
  Queue_N ( const Queue_N< N > & queue_n ) = delete;

  /// @brief Disable move contructor
  ///
  Queue_N ( Queue_N< N > && queue_n ) = delete;

  ~Queue_N ();

  /// @ return True if all lists are empty
  bool
  is_empty ();

  // -- Push

  /// @brief Single event push
  void
  push ( std::size_t index_n, Event * event_n );

  template < typename = std::enable_if< ( N == 1 ) > >
  void
  push ( Event * event_n );

  /// @brief Whole lists push
  void
  push_lists ( Lists & lists_n );

  // -- Pop

  /// @brief Non blocking whole chain pop
  ///
  /// The queues are empty afterwards.
  /// @return True if events were fetched
  bool
  try_pop_lists ( Lists & lists_n );

  // -- Assignment operators

  /// @brief Disable copy assignment
  ///
  Queue_N &
  operator= ( const Queue_N< N > & queue_n ) = delete;

  /// @brief Disable move assignment
  ///
  Queue_N &
  operator= ( Queue_N< N > && queue_n ) = delete;

  private:
  bool
  is_empty_inline () const;

  void
  set_push_notifier ( const std::function< void () > & notifier_n,
                      bool call_on_demand_n = true );

  void
  clear_push_notifier ();

  sev::thread::Reference_Count &
  reference_count ()
  {
    return _reference_count;
  }

  private:
  std::mutex _lists_mutex;
  Lists _lists;
  // -- Push notifier
  sev::thread::Spinlock _push_notifier_mutex;
  std::function< void () > _push_notifier;
  // -- Reference count
  sev::thread::Reference_Count _reference_count;
};

} // namespace sev::event::queue
