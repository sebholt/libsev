/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/list.hpp>
#include <sev/event/queue/connection.hpp>
#include <cstddef>
#include <functional>
#include <type_traits>

// -- Forward declaration
namespace sev::event
{
class Event;
}
namespace sev::event::queue
{
template < std::size_t N >
class Reference_N;
}

namespace sev::event::queue
{

/// @brief Push connection to a  sev::event::Queue_N < N > with local event list
///
template < std::size_t N >
class Connection_In_N : protected Connection_N< N >
{
  public:
  /// @brief Interface to one of the connection event lists
  ///
  class Channel
  {
    public:
    // -- Single event processing

    bool
    is_empty () const
    {
      return _list.is_empty ();
    }

    sev::event::Event *
    front () const
    {
      return static_cast< Event * > ( _list.front () );
    }

    sev::event::Event *
    pop ()
    {
      if ( !_list.is_empty () ) {
        return static_cast< Event * > ( _list.pop_front_not_empty () );
      }
      return nullptr;
    }

    sev::event::Event *
    pop_not_empty ()
    {
      return static_cast< Event * > ( _list.pop_front_not_empty () );
    }

    /// @brief Push back to the front of the qeueue
    void
    repush ( sev::event::Event * event_n )
    {
      _list.push_front ( event_n );
    }

    private:
    // -- Friends
    friend class Connection_In_N< N >;

    // -- Constructors

    Channel ( sev::event::List & list_n )
    : _list ( list_n )
    {
    }

    private:
    sev::event::List & _list;
  };

  // -- Constructors

  Connection_In_N< N > ();

  Connection_In_N< N > ( const Connection_In_N< N > & connection_n ) = delete;

  Connection_In_N< N > ( Connection_In_N< N > && connection_n ) = delete;

  ~Connection_In_N< N > ();

  // -- Assignment operators

  Connection_In_N< N > &
  operator= ( const Connection_In_N< N > & connection_n ) = delete;

  Connection_In_N< N > &
  operator= ( Connection_In_N< N > && connection_n ) = delete;

  // -- Status

  using Connection_N< N >::is_empty;

  // -- Setup

  /// @brief Disconnects and clears the incoming notifier and event list
  ///
  void
  clear ();

  // -- Incoming notifier

  /// @brief The incoming notifier gets passed to the queue for new event
  ///        callback
  ///
  std::function< void () > const &
  incoming_notifier () const
  {
    return _incoming_notifier;
  }

  void
  set_incoming_notifier ( std::function< void () > const & notifier_n );

  void
  clear_incoming_notifier ();

  // -- Queue connection

  using Connection_N< N >::is_connected;
  using Connection_N< N >::queue;

  /// @brief Connects to a given queue
  ///
  /// @arg Call the incoming notifier if there are events in the queue
  /// @return True on success
  bool
  connect ( Reference_N< N > const & queue_n, bool call_on_demand_n = true );

  void
  disconnect ();

  // -- Queue_N < N > reading

  /// @return True if events were fetched
  bool
  read_from_queue ();

  // -- Channel interface

  /// @brief Returns the list interface
  template < typename = std::enable_if< ( N == 1 ) > >
  Channel
  channel ()
  {
    return Channel ( Connection_In_N< N >::list_ref ( 0 ) );
  }

  /// @brief Returns a list interface
  Channel
  channel ( std::size_t index_n )
  {
    return Channel ( Connection_In_N< N >::list_ref ( index_n ) );
  }

  // -- Single list single event processing

  template < typename = std::enable_if< ( N == 1 ) > >
  sev::event::Event *
  front () const
  {
    return static_cast< Event * > ( Connection_In_N< N >::list ( 0 ).front () );
  }

  template < typename = std::enable_if< ( N == 1 ) > >
  sev::event::Event *
  pop ()
  {
    return static_cast< Event * > (
        Connection_In_N< N >::list_ref ( 0 ).pop_front () );
  }

  template < typename = std::enable_if< ( N == 1 ) > >
  sev::event::Event *
  pop_not_empty ()
  {
    return static_cast< Event * > (
        Connection_In_N< N >::list_ref ( 0 ).pop_front_not_empty () );
  }

  /// @brief Push back to the front of the qeueue
  template < typename = std::enable_if< ( N == 1 ) > >
  void
  repush ( sev::event::Event * event_n )
  {
    Connection_In_N< N >::list_ref ( 0 ).push_front ( event_n );
  }

  protected:
  using Connection_N< N >::queue_ref;
  using Connection_N< N >::list;

  private:
  std::function< void () > _incoming_notifier;
};

// -- Types
using Connection_In = Connection_In_N< 1 >;
using Connection_In_1 = Connection_In_N< 1 >;
using Connection_In_2 = Connection_In_N< 2 >;
using Connection_In_3 = Connection_In_N< 3 >;
using Connection_In_4 = Connection_In_N< 4 >;

} // namespace sev::event::queue
