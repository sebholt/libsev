/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "queue.hpp"
#include <sev/mem/list/se/list.hpp>

namespace sev::event::queue
{

// -- Types
using Mutex_Guard = std::lock_guard< std::mutex >;
using Spinlock_Guard = std::lock_guard< sev::thread::Spinlock >;

template < std::size_t N >
Queue_N< N >::Queue_N ()
: _push_notifier ( [] () {} )
{
}

template < std::size_t N >
Queue_N< N >::~Queue_N ()
{
}

template < std::size_t N >
inline bool
Queue_N< N >::is_empty_inline () const
{
  for ( const auto & chain : _lists ) {
    if ( !chain.is_empty () ) {
      return false;
    }
  }
  return true;
}

template <>
inline bool
Queue_N< 1 >::is_empty_inline () const
{
  return _lists[ 0 ].is_empty ();
}

template < std::size_t N >
bool
Queue_N< N >::is_empty ()
{
  Mutex_Guard lists_lock ( _lists_mutex );
  return is_empty_inline ();
}

template < std::size_t N >
void
Queue_N< N >::set_push_notifier ( const std::function< void () > & notifier_n,
                                  bool call_on_demand_n )
{
  Spinlock_Guard notifier_lock ( _push_notifier_mutex );
  _push_notifier = notifier_n;
  if ( call_on_demand_n && !is_empty () ) {
    _push_notifier ();
  }
}

template < std::size_t N >
void
Queue_N< N >::clear_push_notifier ()
{
  Spinlock_Guard notifier_lock ( _push_notifier_mutex );
  _push_notifier = [] () {};
}

template < std::size_t N >
void
Queue_N< N >::push ( std::size_t index_n, Event * event_n )
{
  bool dst_empty;
  {
    Mutex_Guard lists_lock ( _lists_mutex );
    dst_empty = is_empty_inline ();
    _lists[ index_n ].push_back ( event_n );
  }
  if ( dst_empty ) {
    Spinlock_Guard notifier_lock ( _push_notifier_mutex );
    _push_notifier ();
  }
}

template <>
template <>
void
Queue_N< 1 >::push ( Event * event_n )
{
  bool dst_empty;
  {
    Mutex_Guard lists_lock ( _lists_mutex );
    dst_empty = is_empty_inline ();
    _lists[ 0 ].push_back ( event_n );
  }
  if ( dst_empty ) {
    Spinlock_Guard notifier_lock ( _push_notifier_mutex );
    _push_notifier ();
  }
}

template < std::size_t N >
void
Queue_N< N >::push_lists ( Lists & lists_n )
{
  bool src_not_empty = false;
  bool dst_not_empty = false;
  {
    Mutex_Guard lists_lock ( _lists_mutex );
    for ( std::size_t ii = 0; ii != N; ++ii ) {
      auto & src = lists_n[ ii ];
      auto & dst = _lists[ ii ];
      dst_not_empty = ( dst_not_empty || !dst.is_empty () );
      if ( src.is_empty () ) {
        continue;
      }
      dst.splice_back_not_empty ( src );
      src_not_empty = true;
    }
  }
  if ( src_not_empty && !dst_not_empty ) {
    Spinlock_Guard notifier_lock ( _push_notifier_mutex );
    _push_notifier ();
  }
}

template < std::size_t N >
bool
Queue_N< N >::try_pop_lists ( Lists & lists_n )
{
  bool src_not_empty = false;
  {
    Mutex_Guard lists_lock ( _lists_mutex );
    for ( std::size_t ii = 0; ii != N; ++ii ) {
      auto & src = _lists[ ii ];
      auto & dst = lists_n[ ii ];
      if ( src.is_empty () ) {
        continue;
      }
      dst.splice_back_not_empty ( src );
      src_not_empty = true;
    }
  }
  return src_not_empty;
}

// -- Instantiation
template class Queue_N< 1 >;
template class Queue_N< 2 >;
template class Queue_N< 3 >;
template class Queue_N< 4 >;

} // namespace sev::event::queue
