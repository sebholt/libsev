/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/pool.hpp>
#include <sev/event/pool_tracker.hpp>
#include <tuple>

namespace sev::event
{

/// @brief Event pools tuple with interface
///
template < typename... Args >
class Pools
{
  public:
  // -- Construction

  Pools ( sev::event::Pool_Tracker & pool_tracker_n )
  : _pools ( ( static_cast< void > ( sizeof ( Args ) ), pool_tracker_n )... )
  {
  }

  // -- Event type pool accessors

  template < typename T >
  auto &
  pool ()
  {
    return std::get< sev::event::Pool< T > > ( _pools );
  }

  template < typename T >
  const auto &
  pool () const
  {
    return std::get< const sev::event::Pool< T > > ( _pools );
  }

  // -- Index pool accessors

  template < std::size_t N >
  auto &
  pool_n ()
  {
    return std::get< N > ( _pools );
  }

  template < std::size_t N >
  const auto &
  pool_n () const
  {
    return std::get< N > ( _pools );
  }

  private:
  // -- Pools
  std::tuple< sev::event::Pool< Args >... > _pools;
};

} // namespace sev::event
