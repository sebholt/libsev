/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/event/list.hpp>
#include <sev/thread/reference_count.hpp>
#include <sev/thread/spinlock.hpp>
#include <array>
#include <cstddef>
#include <functional>
#include <mutex>

// -- Forward declaration
namespace sev::event::queue_io
{
template < std::size_t N >
class Reference_N;
template < std::size_t N >
class Link_N;
} // namespace sev::event::queue_io

namespace sev::event::queue_io
{

/// @brief Bidirectional event queue with N channels per direction
///
/// The queue supports a single reader and a single writer.
///
template < std::size_t N >
class Queue_N
{
  public:
  // -- Friends
  friend class sev::event::queue_io::Reference_N< N >;

  // -- Types
  using Lists = std::array< sev::event::List, N >;

  // -- Construction

  Queue_N ();

  /// @brief Disable copy contructor
  ///
  Queue_N ( const Queue_N< N > & queue_n ) = delete;

  /// @brief Disable move contructor
  ///
  Queue_N ( Queue_N< N > && queue_n ) = delete;

  ~Queue_N ();

  // -- Link accessor

  Link_N< N >
  link_a ();

  Link_N< N >
  link_b ();

  // -- Assignment operators

  /// @brief Disable copy assignment
  ///
  Queue_N &
  operator= ( const Queue_N< N > & queue_n ) = delete;

  /// @brief Disable move assignment
  ///
  Queue_N &
  operator= ( Queue_N< N > && queue_n ) = delete;

  private:
  // -- Utility

  constexpr static std::size_t
  other_index ( std::size_t index_n )
  {
    return ( index_n + 1 ) % 2;
  }

  // -- Lists empty

  bool
  lists_empty_inline ( Lists & lists_n );

  template < std::size_t INDEX >
  bool
  lists_empty ();

  // -- Static abstract interface

  template < std::size_t INDEX >
  static void
  set_push_notifier ( Queue_N< N > & queue_n,
                      std::function< void () > const & notifier_n,
                      bool call_on_demand_n = true );

  template < std::size_t INDEX >
  static void
  feed_ro ( Queue_N< N > & queue_n, Lists & lists_n );

  template < std::size_t INDEX >
  static void
  feed_wo ( Queue_N< N > & queue_n, Lists & lists_n );

  template < std::size_t INDEX >
  static void
  feed_rw ( Queue_N< N > & queue_n, Lists & in_n, Lists & out_n );

  // -- Reference count

  sev::thread::Reference_Count &
  reference_count ()
  {
    return _reference_count;
  }

  private:
  std::mutex _lists_mutex;
  std::array< Lists, 2 > _lists;
  /// Notifiers change rarely, but need a (fast) change protection anyway
  std::array< sev::thread::Spinlock, 2 > _push_notifier_mutex;
  std::array< std::function< void () >, 2 > _push_notifier;
  sev::thread::Reference_Count _reference_count;
};

// -- Types
using Queue = Queue_N< 1 >;
using Queue_1 = Queue_N< 1 >;
using Queue_2 = Queue_N< 2 >;

} // namespace sev::event::queue_io
