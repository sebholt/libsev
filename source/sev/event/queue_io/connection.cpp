/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "connection.hpp"
#include <sev/assert.hpp>
#include <sev/event/queue_io/reference.hpp>

namespace sev::event::queue_io
{

namespace
{
void
_noop ()
{
}
} // namespace

Connection_Base::Connection_Base ()
: _push_notifier ( _noop )
, _incoming_notifier ( _noop )
{
}

Connection_Base::~Connection_Base () {}

template < std::size_t N >
Connection_N< N >::Connection_N ()
{
}

template < std::size_t N >
Connection_N< N >::~Connection_N ()
{
  disconnect ();
}

template < std::size_t N >
void
Connection_N< N >::connect ( Link_N< N > const & link_n, bool call_on_demand_n )
{
  _link = link_n;
  if ( is_connected () ) {
    _link.set_incoming_notifier ( _incoming_notifier, call_on_demand_n );
  }
}

template < std::size_t N >
void
Connection_N< N >::disconnect ()
{
  if ( is_connected () ) {
    _link.set_incoming_notifier ( _noop, false );
    _link.clear ();
  }
}

template < std::size_t N >
bool
Connection_N< N >::all_empty () const
{
  for ( auto & lst : _lists_in ) {
    if ( !lst.is_empty () ) {
      return false;
    }
  }
  for ( auto & lst : _lists_out ) {
    if ( !lst.is_empty () ) {
      return false;
    }
  }
  return true;
}

template < std::size_t N >
void
Connection_N< N >::set_push_notifier (
    std::function< void () > const & notifier_n, bool call_on_demand_n )
{
  _push_notifier = notifier_n;
  if ( call_on_demand_n && _pushed ) {
    _push_notifier ();
  }
}

template < std::size_t N >
void
Connection_N< N >::clear_push_notifier ()
{
  _push_notifier = _noop;
}

template < std::size_t N >
void
Connection_N< N >::set_incoming_notifier (
    std::function< void () > const & notifier_n, bool call_on_demand_n )
{
  _incoming_notifier = notifier_n;
  if ( is_connected () ) {
    _link.set_incoming_notifier ( _incoming_notifier, call_on_demand_n );
  }
}

template < std::size_t N >
void
Connection_N< N >::clear_incoming_notifier ()
{
  _incoming_notifier = _noop;
  if ( is_connected () ) {
    _link.set_incoming_notifier ( _incoming_notifier, false );
  }
}

template < std::size_t N >
void
Connection_N< N >::feed_queue ()
{
  if ( !is_connected () ) {
    DEBUG_ASSERT ( !_pushed );
    return;
  }

  if ( _pushed ) {
    _pushed = false;
    _link.transfer_rw ( _lists_in, _lists_out );
  } else {
    _link.transfer_ro ( _lists_in );
  }

  DEBUG_ASSERT ( !_pushed );
}

template < std::size_t N >
void
Connection_N< N >::feed_queue ( std::uint_fast32_t & bits_n,
                                std::uint_fast32_t incoming_bits_n,
                                std::uint_fast32_t outgoing_bits_n )
{
  if ( !is_connected () ) {
    return;
  }

  bool incoming ( ( bits_n & incoming_bits_n ) != 0 );
  // Clear bits
  bits_n &= ~( incoming_bits_n | outgoing_bits_n );
  if ( incoming || _pushed ) {
    if ( _pushed ) {
      _pushed = false;
      if ( incoming ) {
        _link.transfer_rw ( _lists_in, _lists_out );
      } else {
        _link.transfer_wo ( _lists_out );
      }
    } else {
      _link.transfer_ro ( _lists_in );
    }
  }
}

// -- Instantiation
template class Connection_N< 1 >;
template class Connection_N< 2 >;

} // namespace sev::event::queue_io
