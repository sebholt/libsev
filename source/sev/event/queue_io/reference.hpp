/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstddef>

// -- Forward declaration
namespace sev::event::queue_io
{
template < std::size_t N >
class Queue_N;
template < std::size_t N >
class Link_N;
} // namespace sev::event::queue_io

namespace sev::event::queue_io
{

/// @brief Reference to a queue
///
/// The last destroyed reference deletes the queue.
///
template < std::size_t N >
class Reference_N
{
  public:
  // -- Constructors

  /// @brief Constructs an unconnected reference
  Reference_N () = default;

  /// @brief Makes this another reference to the queue
  Reference_N ( Queue_N< N > * queue_n );

  /// @brief Makes this another reference to the queue
  Reference_N ( const Reference_N< N > & value_n );

  /// @brief Takes over the reference and leaves it in an unconnected state
  Reference_N ( Reference_N< N > && value_n );

  ~Reference_N ();

  // -- Queue reference status

  /// @return True if connected to a queue
  bool
  is_valid () const
  {
    return ( _queue != nullptr );
  }

  Queue_N< N > *
  get () const
  {
    return _queue;
  }

  // -- Endpoint links

  Link_N< N >
  link_a () const;

  Link_N< N >
  link_b () const;

  template < std::size_t INDEX >
  Link_N< N >
  link_n ()
  {
    if ( INDEX == 0 ) {
      return link_a ();
    }
    return link_b ();
  }

  Link_N< N >
  link ( std::size_t index_n )
  {
    if ( index_n == 0 ) {
      return link_a ();
    }
    return link_b ();
  }

  // -- new / clear

  void
  clear ();

  /// @brief Generates a new queue
  static Reference_N< N >
  created ();

  /// @brief Drops and resets the queue reference
  void
  reset ( Queue_N< N > * queue_n );

  /// @brief Moves the reference to this
  void
  move_assign ( Reference_N< N > && ref_n );

  // -- Type cast operators

  operator Queue_N< N > * () const { return _queue; }

  Queue_N< N > *
  operator-> () const
  {
    return _queue;
  }

  // -- Comparison operators

  bool
  operator== ( const Reference_N< N > & ref_n ) const
  {
    return ( _queue == ref_n._queue );
  }

  bool
  operator!= ( const Reference_N< N > & ref_n ) const
  {
    return ( _queue != ref_n._queue );
  }

  bool
  operator== ( Queue_N< N > * queue_n ) const
  {
    return ( _queue == queue_n );
  }

  bool
  operator!= ( Queue_N< N > * queue_n ) const
  {
    return ( _queue != queue_n );
  }

  // -- Assignment operators

  /// @brief Makes this another reference to the queue
  Reference_N< N > &
  operator= ( const Reference_N< N > & ref_n )
  {
    reset ( ref_n._queue );
    return *this;
  }

  /// @brief Takes over the reference and leaves it in an unconnected state
  Reference_N< N > &
  operator= ( Reference_N< N > && ref_n )
  {
    move_assign ( static_cast< Reference_N< N > && > ( ref_n ) );
    return *this;
  }

  /// @brief Makes this another reference to the queue
  Reference_N< N > &
  operator= ( Queue_N< N > * queue_n )
  {
    reset ( queue_n );
    return *this;
  }

  private:
  void
  ref_count_add ( Queue_N< N > * queue_n );

  void
  ref_count_sub ( Queue_N< N > * queue_n );

  private:
  Queue_N< N > * _queue = nullptr;
};

// -- Types
using Reference = Reference_N< 1 >;
using Reference_1 = Reference_N< 1 >;
using Reference_2 = Reference_N< 2 >;

} // namespace sev::event::queue_io
