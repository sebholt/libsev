/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/assert.hpp>
#include <sev/event/list.hpp>
#include <sev/event/queue_io/reference.hpp>
#include <array>
#include <cstddef>
#include <functional>

// -- Forward declaration
namespace sev::event::queue_io
{
template < std::size_t N >
class Queue_N;
template < std::size_t N >
class Reference_N;
} // namespace sev::event::queue_io

namespace sev::event::queue_io
{

/// @brief Queue endpoint link
///
template < std::size_t N >
class Link_N
{
  public:
  // -- Friends
  friend class sev::event::queue_io::Queue_N< N >;

  // -- Types
  using Lists = std::array< sev::event::List, N >;
  using Func_Notifier =
      void ( * ) ( Queue_N< N > & queue_n,
                   std::function< void () > const & notifier_n,
                   bool call_on_demand_n );
  using Func_Single = void ( * ) ( Queue_N< N > & queue_n, Lists & lists_n );
  using Func_Dual = void ( * ) ( Queue_N< N > & queue_n,
                                 Lists & in_n,
                                 Lists & out_n );

  private:
  // -- Construction

  Link_N ( Reference_N< N > const & queue_n,
           Func_Notifier func_notifier_n,
           Func_Single func_ro_n,
           Func_Single func_wo_n,
           Func_Dual func_rw_n )
  : _queue ( queue_n )
  , _func_notifier ( func_notifier_n )
  , _func_ro ( func_ro_n )
  , _func_wo ( func_wo_n )
  , _func_rw ( func_rw_n )
  {
  }

  public:
  Link_N () = default;

  Link_N ( Link_N< N > const & link_n ) = default;

  Link_N ( Link_N< N > && link_n ) = default;

  // -- Assignment operators

  Link_N &
  operator= ( Link_N< N > const & link_n ) = default;

  Link_N &
  operator= ( Link_N< N > && link_n ) = default;

  // -- Reset

  void
  clear ()
  {
    _queue.clear ();
    _func_notifier = nullptr;
    _func_ro = nullptr;
    _func_wo = nullptr;
    _func_rw = nullptr;
  }

  // -- Queue

  Reference_N< N > const &
  queue () const
  {
    return _queue;
  }

  bool
  is_valid () const
  {
    return _queue.is_valid ();
  }

  // -- Bit notifier

  void
  set_incoming_notifier ( std::function< void () > const & notifier_n,
                          bool call_on_demand_n = true )
  {
    DEBUG_ASSERT ( is_valid () );
    _func_notifier ( *_queue, notifier_n, call_on_demand_n );
  }

  // -- Queue event exchange

  void
  transfer_ro ( Lists & lists_n )
  {
    DEBUG_ASSERT ( is_valid () );
    _func_ro ( *_queue, lists_n );
  }

  void
  transfer_wo ( Lists & lists_n )
  {
    DEBUG_ASSERT ( is_valid () );
    _func_wo ( *_queue, lists_n );
  }

  void
  transfer_rw ( Lists & in_n, Lists & out_n )
  {
    DEBUG_ASSERT ( is_valid () );
    _func_rw ( *_queue, in_n, out_n );
  }

  private:
  Reference_N< N > _queue;
  Func_Notifier _func_notifier = nullptr;
  Func_Single _func_ro = nullptr;
  Func_Single _func_wo = nullptr;
  Func_Dual _func_rw = nullptr;
};

// -- Types
using Link = Link_N< 1 >;
using Link_1 = Link_N< 1 >;
using Link_2 = Link_N< 2 >;

} // namespace sev::event::queue_io
