/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "reference.hpp"
#include <sev/event/queue_io/link.hpp>
#include <sev/event/queue_io/queue.hpp>

namespace sev::event::queue_io
{

template < std::size_t N >
Reference_N< N >::Reference_N ( Queue_N< N > * queue_n )
: _queue ( queue_n )
{
  if ( _queue != nullptr ) {
    ref_count_add ( _queue );
  }
}

template < std::size_t N >
Reference_N< N >::Reference_N ( const Reference_N< N > & value_n )
: _queue ( value_n._queue )
{
  // This is another reference
  if ( _queue != nullptr ) {
    ref_count_add ( _queue );
  }
}

template < std::size_t N >
Reference_N< N >::Reference_N ( Reference_N< N > && value_n )
: _queue ( value_n._queue )
{
  // We took over the source's reference
  if ( _queue != nullptr ) {
    value_n._queue = nullptr;
  }
}

template < std::size_t N >
Reference_N< N >::~Reference_N ()
{
  if ( _queue != nullptr ) {
    ref_count_sub ( _queue );
  }
}

template < std::size_t N >
void
Reference_N< N >::clear ()
{
  if ( _queue != nullptr ) {
    ref_count_sub ( _queue );
    _queue = nullptr;
  }
}

template < std::size_t N >
Reference_N< N >
Reference_N< N >::created ()
{
  return Reference_N ( new Queue_N< N > () );
}

template < std::size_t N >
void
Reference_N< N >::reset ( Queue_N< N > * queue_n )
{
  Queue_N< N > * queue_prev ( _queue );
  _queue = queue_n;
  if ( _queue != nullptr ) {
    ref_count_add ( _queue );
  }
  if ( queue_prev != nullptr ) {
    ref_count_sub ( queue_prev );
  }
}

template < std::size_t N >
void
Reference_N< N >::move_assign ( Reference_N< N > && value_n )
{
  // drop old reference
  clear ();
  // take over the source's reference
  if ( value_n._queue != nullptr ) {
    _queue = value_n._queue;
    value_n._queue = nullptr;
  }
}

template < std::size_t N >
Link_N< N >
Reference_N< N >::link_a () const
{
  if ( is_valid () ) {
    return _queue->link_a ();
  }
  return Link_N< N > ();
}

template < std::size_t N >
Link_N< N >
Reference_N< N >::link_b () const
{
  if ( is_valid () ) {
    return _queue->link_b ();
  }
  return Link_N< N > ();
}

template < std::size_t N >
inline void
Reference_N< N >::ref_count_add ( Queue_N< N > * queue_n )
{
  queue_n->reference_count ().add ();
}

template < std::size_t N >
inline void
Reference_N< N >::ref_count_sub ( Queue_N< N > * queue_n )
{
  if ( queue_n->reference_count ().sub () == 1 ) {
    delete queue_n;
  }
}

// -- Instantiation
template class Reference_N< 1 >;
template class Reference_N< 2 >;

} // namespace sev::event::queue_io
