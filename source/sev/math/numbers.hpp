/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::math
{

template < class T >
inline T
abs ( const T & val_n )
{
  if ( val_n < T ( 0 ) ) {
    return -val_n;
  }
  return val_n;
}

template < class T >
inline void
assign_abs ( T & val_n )
{
  if ( val_n < T ( 0 ) ) {
    val_n = -val_n;
  }
}

template < class T >
inline T
min ( const T & val_a_n, const T & val_b_n )
{
  if ( val_a_n < val_b_n ) {
    return val_a_n;
  }
  return val_b_n;
}

template < class T >
inline T
max ( const T & val_a_n, const T & val_b_n )
{
  if ( val_b_n > val_a_n ) {
    return val_b_n;
  }
  return val_a_n;
}

template < class T >
inline void
assign_smaller ( T & val_n, const T & other_val_n )
{
  if ( val_n > other_val_n ) {
    val_n = other_val_n;
  }
}

template < class T >
inline void
assign_larger ( T & val_n, const T & other_val_n )
{
  if ( val_n < other_val_n ) {
    val_n = other_val_n;
  }
}

template < class T >
inline void
subtract_minimum_zero ( T & val_n, const T & sub_val_n )
{
  if ( val_n > sub_val_n ) {
    val_n -= sub_val_n;
  } else {
    val_n = T ( 0 );
  }
}

template < class T >
inline void
range_crop ( T & val_n, const T & val_min_n, const T & val_max_n )
{
  if ( val_n < val_min_n ) {
    val_n = val_min_n;
  }
  if ( val_n > val_max_n ) {
    val_n = val_max_n;
  }
}

} // namespace sev::math
