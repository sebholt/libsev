/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cstdint>

namespace sev::math
{

// Unsigned int
constexpr unsigned int kilo_ui = 1000;
constexpr unsigned int mega_ui = 1000000;
constexpr unsigned int giga_ui = 1000000000;

constexpr unsigned int kibi_ui = 1024;
constexpr unsigned int mebi_ui = 1048576;    // 1024^2
constexpr unsigned int gibi_ui = 1073741824; // 1024^3

// Unsigned long long
constexpr unsigned long long kilo_ull = 1000ull;
constexpr unsigned long long mega_ull = 1000000ull;
constexpr unsigned long long giga_ull = 1000000000ull;
constexpr unsigned long long tera_ull = 1000000000000ull;
constexpr unsigned long long peta_ull = 1000000000000000ull;

constexpr unsigned long long kibi_ull = 1024ull;
constexpr unsigned long long mebi_ull = 1048576ull;
constexpr unsigned long long gibi_ull = 1073741824ull;
constexpr unsigned long long tebi_ull = 1099511627776ull;
constexpr unsigned long long pebi_ull = 1125899906842624ull;

// Double
constexpr double kilo_d = 1000.0;
constexpr double mega_d = 1000000.0;
constexpr double giga_d = 1000000000.0;
constexpr double tera_d = 1000000000000.0;
constexpr double peta_d = 1000000000000000.0;

constexpr double milli_d = 1.0e-3;
constexpr double micro_d = 1.0e-6;
constexpr double nano_d = 1.0e-9;
constexpr double pico_d = 1.0e-12;
constexpr double femto_d = 1.0e-15;

// Float
constexpr float kilo_f = 1000.0f;
constexpr float mega_f = 1000000.0f;
constexpr float giga_f = 1000000000.0f;
constexpr float tera_f = 1000000000000.0f;
constexpr float peta_f = 1000000000000000.0f;

constexpr float milli_f = 1.0e-3f;
constexpr float micro_f = 1.0e-6f;
constexpr float nano_f = 1.0e-9f;
constexpr float pico_f = 1.0e-12f;
constexpr float femto_f = 1.0e-15f;

} // namespace sev::math
