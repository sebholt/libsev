/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/logt/context.hpp>
#include <sev/logt/ostream_logger.hpp>

namespace sev::logt
{

void
OStream_Logger::Body::log () const
{
  context.log_unchecked ( flags, ost.str () );
}

} // namespace sev::logt
