/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/logt/flags.hpp>
#include <atomic>

namespace sev::logt
{
// -- Forward declaration
namespace server
{
class Message;
class Context;
class Core;
} // namespace server

/// @brief Sink base class
///
/// Sinks can be registered at a Server to log messages
///
class Sink
{
  public:
  // -- Types

  static const Flags mask_default = FLG_DEFAULT;
  friend class server::Core;

  public:
  // -- Constructors

  Sink ( Flags mask_n = mask_default );

  virtual ~Sink ();

  // -- Server registration

  server::Core *
  server_core () const
  {
    return _server_core;
  }

  bool
  server_is_registered ()
  {
    return ( _server_core != nullptr );
  }

  /// @brief Unregister this sink at server_core()
  void
  server_unregister ();

  // -- Log limit level

  Flags
  mask () const
  {
    return _mask.load ();
  }

  virtual void
  set_mask ( Flags mask_n );

  bool
  mask_test ( Flags flags_n )
  {
    return ( ( flags_n & _mask.load () ) != 0 );
  }

  // -- Context service registration

  virtual void
  context_service_begin ( server::Context * context_n );

  virtual void
  context_service_end ( server::Context * context_n );

  // -- Message logging

  virtual void
  log_message ( server::Message * message_n ) = 0;

  // -- Flushing

  /// @brief Request all message to be flushed
  virtual void
  flush_request () = 0;

  /// @brief Wait until all messages are flushed
  virtual void
  flush_wait () = 0;

  protected:
  /// @brief Called once when the sink gets registered
  ///
  /// Sets the server core pointer.
  /// Derived classes must call the parent implementation.
  virtual void
  server_session_begin ( server::Core * server_core_n );

  /// @brief Called once when the sink gets unregistered
  ///
  /// Clears the server core pointer.
  /// Derived classes must call the parent implementation.
  virtual void
  server_session_end ();

  private:
  std::atomic< Flags > _mask;
  server::Core * _server_core;
};

} // namespace sev::logt
