/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/logt/server/core.hpp>
#include <sev/logt/server/message.hpp>
#include <sev/logt/sink.hpp>
#include <cassert>

namespace sev::logt
{

Sink::Sink ( Flags mask_n )
: _mask ( mask_n )
, _server_core ( nullptr )
{
}

Sink::~Sink ()
{
  server_unregister ();
}

void
Sink::server_unregister ()
{
  if ( _server_core != nullptr ) {
    _server_core->sink_unregister ( this );
  }
}

void
Sink::set_mask ( Flags mask_n )
{
  _mask.store ( mask_n );
}

void
Sink::server_session_begin ( server::Core * server_n )
{
  _server_core = server_n;
}

void
Sink::server_session_end ()
{
  _server_core = nullptr;
}

void
Sink::context_service_begin ( server::Context * context_n [[maybe_unused]] )
{
}

void
Sink::context_service_end ( server::Context * context_n [[maybe_unused]] )
{
}

} // namespace sev::logt
