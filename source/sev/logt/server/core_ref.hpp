/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <utility>

namespace sev::logt::server
{

// -- Forward declaration
class Core;

/// @brief Reference to a server core
///
class Core_Ref
{
  public:
  // -- Constructors

  /// @brief Allocate a new core
  Core_Ref ();

  Core_Ref ( const logt::server::Core_Ref & ref_n );

  Core_Ref ( logt::server::Core_Ref && ref_n )
  : _core ( ref_n._core )
  {
    if ( _core != nullptr ) {
      ref_n._core = nullptr;
    }
  }

  ~Core_Ref ();

  // -- Core

  logt::server::Core *
  core () const
  {
    return _core;
  }

  void
  set_core ( logt::server::Core * core_n );

  // -- Cast operators

  /// @brief Cast operator
  ///
  operator bool () const { return ( _core != nullptr ); }

  /// @brief Cast operator
  ///
  operator logt::server::Core * () const { return _core; }

  /// @brief Pointer operator
  ///
  logt::server::Core *
  operator-> () const
  {
    return _core;
  }

  // -- Assignment operators

  Core_Ref &
  operator= ( const Core_Ref & ref_n )
  {
    copy_assign ( std::move ( ref_n ) );
    return *this;
  }

  Core_Ref &
  operator= ( Core_Ref && ref_n )
  {
    move_assign ( std::move ( ref_n ) );
    return *this;
  }

  private:
  // -- Copy / Move

  void
  copy_assign ( const Core_Ref & ref_n );

  void
  move_assign ( Core_Ref && ref_n );

  void
  decrement ( Core * core_n );

  private:
  logt::server::Core * _core;
};

} // namespace sev::logt::server
