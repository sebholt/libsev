/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/logt/reference.hpp>
#include <sev/logt/server/chain.hpp>
#include <sev/logt/server/message_meta.hpp>
#include <sev/thread/reference_count.hpp>
#include <string>

namespace sev::logt::server
{

/// @brief Message that keeps a reference to the sender context
///
class Message
{
  public:
  // -- Types

  friend class Chain< Message >;

  public:
  // -- Constructors

  Message () = default;

  Message ( const Message & message_n ) = delete;

  Message ( Message && message_n ) = delete;

  Message ( logt::server::Context * ref_n,
            const logt::server::Message_Meta & meta_n,
            std::string && text_utf8_n )
  : _meta ( meta_n )
  , _text ( std::move ( text_utf8_n ) )
  , _context_ref ( ref_n )
  {
  }

  Message ( logt::server::Context * ref_n,
            const logt::server::Message_Meta & meta_n,
            const char * text_n,
            std::size_t length_n )
  : _meta ( meta_n )
  , _text ( text_n, length_n )
  , _context_ref ( ref_n )
  {
  }

  ~Message () = default;

  void
  clear ();

  // -- Issuing context reference

  /// @brief Reference to the issuer context
  const logt::Reference &
  context_ref () const
  {
    return _context_ref;
  }

  void
  set_context_ref ( logt::server::Context * server_context_n )
  {
    _context_ref.reset ( server_context_n );
  }

  // -- Reference count

  void
  ref_count_increment ()
  {
    _reference_count.add ();
  }

  void
  ref_count_decrement ();

  /// @brief Use to release/delete the message manually
  /// @return True if this was the last reference
  bool
  ref_count_decrement_handle ()
  {
    return ( _reference_count.sub () == 1 );
  }

  // -- Chaining

  Message *
  chain_prev () const
  {
    return _chain_prev;
  }

  Message *
  chain_next () const
  {
    return _chain_next;
  }

  // -- Meta

  const logt::server::Message_Meta &
  meta () const
  {
    return _meta;
  }

  void
  set_meta ( logt::server::Message_Meta && meta_n )
  {
    _meta = std::move ( meta_n );
  }

  // -- Text

  const std::string &
  text () const
  {
    return _text;
  }

  void
  set_text ( std::string && text_n )
  {
    _text = std::move ( text_n );
  }

  // -- Assignment operators

  Message &
  operator= ( const Message & message_n ) = delete;

  Message &
  operator= ( Message && message_n ) = delete;

  private:
  // -- Chain

  void
  chain_prev_set ( Message * message_n )
  {
    _chain_prev = message_n;
  }

  void
  chain_next_set ( Message * message_n )
  {
    _chain_next = message_n;
  }

  void
  chain_clear ()
  {
    _chain_prev = nullptr;
    _chain_next = nullptr;
  }

  private:
  // -- Features
  logt::server::Message_Meta _meta;
  std::string _text;
  // -- Chaining
  logt::server::Message * _chain_prev = nullptr;
  logt::server::Message * _chain_next = nullptr;
  // -- Reference
  logt::Reference _context_ref;
  sev::thread::Reference_Count _reference_count;
};

} // namespace sev::logt::server
