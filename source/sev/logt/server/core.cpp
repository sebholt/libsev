/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/logt/server.hpp>
#include <sev/logt/server/context.hpp>
#include <sev/logt/server/core.hpp>
#include <sev/logt/server/message.hpp>
#include <cassert>

namespace sev::logt::server
{

// -- Utility

typedef std::lock_guard< std::mutex > Lock_Guard;

// -- Class methods

Core::Core ()
: _mask ( logt::Server::mask_default )
, _contexts_pool_size_max ( 16 )
, _messages_pool_size_max ( 16 )
{
  _root_context = context_acquire ( {}, nullptr );
}

Core::~Core ()
{
  // Disconnect remaining sinks
  while ( !_sinks.empty () ) {
    sink_unregister ( _sinks.back () );
  }
  // Clear root context
  _root_context.clear ();
  // Contexts and sinks must not outlive the server
  assert ( _contexts_active.is_empty () );
  // Clear messages pool
  while ( !_messages_pool.is_empty () ) {
    message_delete ( _messages_pool.pop_back () );
  }
  // Clear context pool
  while ( !_contexts_pool.is_empty () ) {
    context_delete ( _contexts_pool.pop_back () );
  }
}

logt::Flags
Core::mask ()
{
  logt::Flags res;
  {
    Lock_Guard glock ( _mutex );
    res = _mask;
  }
  return res;
}

void
Core::set_mask ( logt::Flags mask_n )
{
  Lock_Guard glock ( _mutex );
  if ( _mask != mask_n ) {
    _mask = mask_n;
    // Update context level copies
    for ( auto it = _contexts_active.begin (); it; ++it ) {
      it->set_mask_server ( mask_n );
    }
  }
}

void
Core::flush ()
{
  Lock_Guard glock ( _mutex );
  for ( auto item : _sinks ) {
    item->flush_request ();
  }
  for ( auto item : _sinks ) {
    item->flush_wait ();
  }
}

void
Core::log ( logt::server::Context * context_n,
            logt::Flags flags_n,
            std::string && string_n )
{
  Lock_Guard glock ( _mutex );

  // Test against server local log flag mask
  if ( mask_test ( flags_n ) ) {

    logt::server::Message * message (
        message_acquire_unsafe ( context_n, flags_n, std::move ( string_n ) ) );

    // Manually increment message count
    message->ref_count_increment ();

    // Feed message to sinks
    for ( auto item : _sinks ) {
      item->log_message ( message );
    }

    // Check if we can release the message already
    if ( message->ref_count_decrement_handle () ) {
      message_release_unsafe ( message );
    }
  }
}

logt::server::Message *
Core::message_acquire_unsafe ( logt::server::Context * context_n,
                               logt::Flags flags_n,
                               std::string && text_n )
{
  logt::server::Message * message ( nullptr );
  if ( !_messages_pool.is_empty () ) {
    // Pick from pool
    message = _messages_pool.pop_back ();
    message->set_context_ref ( context_n );
    message->set_meta ( logt::server::Message_Meta ( flags_n ) );
    message->set_text ( std::move ( text_n ) );
  } else {
    // Generate new
    message = message_new ( context_n,
                            logt::server::Message_Meta ( flags_n ),
                            std::move ( text_n ) );
  }
  return message;
}

void
Core::message_release_unsafe ( logt::server::Message * message_n )
{
  if ( _messages_pool.size () < _messages_pool_size_max ) {
    // Push to pool
    _messages_pool.push_back ( message_n );
    // Clear message
    message_n->clear ();
  } else {
    message_delete ( message_n );
  }
}

void
Core::message_release ( logt::server::Message * message_n )
{
  // Ensure context is released after the message
  logt::Reference context_ref ( std::move ( message_n->context_ref () ) );
  {
    Lock_Guard glock ( _mutex );
    message_release_unsafe ( message_n );
  }
}

logt::server::Message *
Core::message_new ( logt::server::Context * context_n,
                    const logt::server::Message_Meta & meta_n,
                    std::string && text_utf8_n )
{
  logt::server::Message * res ( new logt::server::Message (
      context_n, meta_n, std::move ( text_utf8_n ) ) );
  return res;
}

void
Core::message_delete ( logt::server::Message * message_n )
{
  delete message_n;
}

logt::server::Context *
Core::context_acquire ( sev::unicode::View name_n,
                        logt::server::Context * parent_n )
{
  logt::server::Context * res ( nullptr );
  {
    Lock_Guard glock ( _mutex );
    // Acquire and register context
    {
      if ( !_contexts_pool.is_empty () ) {
        res = _contexts_pool.pop_back ();
      } else {
        res = context_new ();
      }
      res->setup ( _mask, name_n, parent_n );
      _contexts_active.push_back ( res );
    }
    // Notify sinks of context service begin
    for ( auto sink : _sinks ) {
      sink->context_service_begin ( res );
    }
  }
  return res;
}

void
Core::context_release ( logt::server::Context * context_n )
{
  if ( context_n != nullptr ) {
    // Ensure parent gets release afterwards - ouside the lock
    logt::Reference parent_ref ( std::move ( context_n->parent_ref () ) );
    {
      Lock_Guard glock ( _mutex );
      // Notify sinks of context service end
      for ( auto sink : _sinks ) {
        sink->context_service_end ( context_n );
      }
      // Unregister context
      _contexts_active.remove ( context_n );
      // Release context object
      if ( _contexts_pool.size () < _contexts_pool_size_max ) {
        _contexts_pool.push_back ( context_n );
      } else {
        context_delete ( context_n );
      }
    }
  }
}

logt::server::Context *
Core::context_new ()
{
  logt::server::Context * res ( new logt::server::Context ( this ) );
  return res;
}

void
Core::context_delete ( logt::server::Context * context_n )
{
  delete context_n;
}

void
Core::sink_register ( Sink * sink_n )
{
  if ( sink_n != nullptr ) {
    // Unregister sink from any other server
    sink_n->server_unregister ();
    {
      Lock_Guard glock ( _mutex );
      // Register new sink
      _sinks.push_back ( sink_n );
      // Update sink server registration
      sink_n->server_session_begin ( this );

      // -- Notify new sink of all active contexts
      for ( auto it = _contexts_active.begin (); it; ++it ) {
        sink_n->context_service_begin ( it );
      }
    }
  }
}

void
Core::sink_unregister ( Sink * sink_n )
{
  if ( sink_n != nullptr ) {
    Lock_Guard glock ( _mutex );
    {
      // Find sink in sink register
      Sink_List::iterator it ( _sinks.begin () );
      while ( it != _sinks.end () ) {
        if ( ( *it ) == sink_n ) {
          break;
        }
        ++it;
      }
      // If this sink is known remove it
      if ( it != _sinks.end () ) {
        // Ens sink session
        sink_n->server_session_end ();
        // Remove sink from register
        _sinks.erase ( it );
      }
    }
  }
}

} // namespace sev::logt::server
