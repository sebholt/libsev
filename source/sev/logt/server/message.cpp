/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/logt/server/context.hpp>
#include <sev/logt/server/message.hpp>

namespace sev::logt::server
{

void
Message::clear ()
{
  _text.clear ();
  _context_ref.clear ();
}

void
Message::ref_count_decrement ()
{
  if ( _reference_count.sub () == 1 ) {
    if ( _context_ref ) {
      _context_ref->core ()->message_release ( this );
    }
  }
}

} // namespace sev::logt::server
