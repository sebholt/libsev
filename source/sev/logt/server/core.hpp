/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/logt/reference.hpp>
#include <sev/logt/server/chain.hpp>
#include <sev/logt/server/core.hpp>
#include <sev/logt/server/message_meta.hpp>
#include <sev/logt/sink.hpp>
#include <sev/thread/reference_count.hpp>
#include <sev/unicode/view.hpp>
#include <condition_variable>
#include <mutex>
#include <string>
#include <vector>

namespace sev::logt::server
{

/// @brief Log server core
///
class Core
{
  public:
  // -- Types

  typedef std::vector< logt::Sink * > Sink_List;
  typedef logt::server::Chain< logt::server::Context > Context_Chain;
  typedef logt::server::Chain< logt::server::Message > Message_Chain;

  public:
  // -- Constructors

  Core ();

  ~Core ();

  // -- Reference count

  void
  ref_count_increment ()
  {
    _reference_count.add ();
  }

  bool
  ref_count_decrement ()
  {
    return ( _reference_count.sub () != 1 );
  }

  // -- Server context handout

  logt::server::Context *
  context_acquire ( sev::unicode::View name_n,
                    logt::server::Context * parent_n );

  void
  context_release ( logt::server::Context * context_n );

  // -- Server private root context

  // @brief Root context
  const logt::Reference &
  root_context () const
  {
    return _root_context;
  }

  // -- Log flags mask

  logt::Flags
  mask ();

  void
  set_mask ( logt::Flags mask_n );

  bool
  mask_test ( logt::Flags mask_n ) const
  {
    return ( ( mask_n & _mask ) != 0 );
  }

  // -- Logging with context

  void
  log ( logt::server::Context * context_n,
        logt::Flags flags_n,
        std::string && string_n );

  // -- Flushing

  void
  flush ();

  // -- Server message handout

  void
  message_release ( logt::server::Message * message_n );

  // -- Sinks

  /// @brief Installs a sink
  ///
  /// @return True on success
  void
  sink_register ( Sink * sink_n );

  void
  sink_unregister ( Sink * sink_n );

  private:
  logt::server::Message *
  message_acquire_unsafe ( logt::server::Context * context_n,
                           logt::Flags flags_n,
                           std::string && text_n );

  void
  message_release_unsafe ( logt::server::Message * message_n );

  logt::server::Message *
  message_new ( logt::server::Context * context_n,
                const logt::server::Message_Meta & meta_n,
                std::string && text_utf8_n );

  void
  message_delete ( logt::server::Message * context_n );

  logt::server::Context *
  context_new ();

  void
  context_delete ( logt::server::Context * context_n );

  private:
  /// @brief Mutex
  std::mutex _mutex;

  /// @brief Server local level limit
  logt::Flags _mask;
  /// @brief Sinks
  Sink_List _sinks;
  /// @brief Log contexts
  Context_Chain _contexts_active;
  Context_Chain _contexts_pool;
  std::size_t _contexts_pool_size_max;

  Message_Chain _messages_pool;
  std::size_t _messages_pool_size_max;

  /// @brief Reference count
  sev::thread::Reference_Count _reference_count;

  // @brief Root context
  logt::Reference _root_context;
};

} // namespace sev::logt::server
