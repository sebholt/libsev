/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/logt/server/time_point.hpp>

namespace sev::logt::server
{

void
Time_Point::set_now ()
{
  _steady = std::chrono::steady_clock::now ();
  _system = std::chrono::system_clock::now ();
}

} // namespace sev::logt::server
