/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <utility>

namespace sev::logt::server
{

/// @brief Object chain template
///
template < typename T >
class Chain_Iterator
{
  public:
  Chain_Iterator () = default;

  Chain_Iterator ( T * begin_n )
  : _ptr ( begin_n )
  {
  }

  void
  operator++ ()
  {
    T * cnext ( _ptr->chain_next () );
    if ( cnext != _ptr ) {
      _ptr = cnext;
    } else {
      _ptr = nullptr;
    }
  }

  void
  operator-- ()
  {
    T * cnext ( _ptr->chain_prev () );
    if ( cnext != _ptr ) {
      _ptr = cnext;
    } else {
      _ptr = nullptr;
    }
  }

  operator bool () const { return ( _ptr != nullptr ); }

  operator T * () const { return _ptr; }

  T *
  operator-> () const
  {
    return _ptr;
  }

  T &
  operator* () const
  {
    return *_ptr;
  }

  private:
  T * _ptr = nullptr;
};

/// @brief Object chain template
///
template < typename T >
class Chain
{
  public:
  // -- Types
  typedef Chain_Iterator< T > iterator;
  typedef Chain_Iterator< const T > const_iterator;

  public:
  // -- Constructors

  Chain () = default;

  Chain ( const Chain & chain_n ) = delete;

  Chain ( Chain && chain_n )
  : _front ( chain_n._front )
  , _back ( chain_n._back )
  , _size ( chain_n._size )
  {
    chain_n._front = nullptr;
    chain_n._back = nullptr;
    chain_n._size = 0;
  }

  ~Chain () = default;

  // -- Ends

  T *
  front () const
  {
    return _front;
  }

  T *
  back () const
  {
    return _back;
  }

  bool
  is_empty () const
  {
    return ( _front == nullptr );
  }

  std::size_t
  size () const
  {
    return _size;
  }

  // -- Iteration

  iterator
  begin ()
  {
    return iterator ( _front );
  }

  const_iterator
  begin () const
  {
    return const_iterator ( _front );
  }

  const_iterator
  cbegin () const
  {
    return const_iterator ( _front );
  }

  // -- Push

  void
  push_back ( T * item_n )
  {
    if ( _back != nullptr ) {
      item_n->chain_prev_set ( _back );
      item_n->chain_next_set ( item_n );
      _back->chain_next_set ( item_n );
      _back = item_n;
    } else {
      // First and only context
      item_n->chain_prev_set ( item_n );
      item_n->chain_next_set ( item_n );
      _front = item_n;
      _back = item_n;
    }
    ++_size;
  }

  void
  push_front ( T * item_n )
  {
    if ( _front != nullptr ) {
      item_n->chain_prev_set ( item_n );
      item_n->chain_next_set ( _front );
      _front->chain_prev_set ( item_n );
      _front = item_n;
    } else {
      // First and only context
      item_n->chain_prev_set ( item_n );
      item_n->chain_next_set ( item_n );
      _front = item_n;
      _back = item_n;
    }
    ++_size;
  }

  // -- Pop

  /// @brief Requires !is_empty()
  T *
  pop_front_not_empty ()
  {
    T * res ( _front );
    {
      T * const enext ( _front->chain_next () );
      // Update chain end pointers
      if ( _front != enext ) {
        _front = enext;
        _front->chain_prev_set ( _front );
      } else {
        // Next pointer to self marks end of chain
        _front = nullptr;
        _back = nullptr;
      }
    }
    --_size;
    res->chain_clear ();
    return res;
  }

  // @return Non nullptr if !is_empty()
  T *
  pop_front ()
  {
    if ( _front != nullptr ) {
      return pop_front_not_empty ();
    }
    return nullptr;
  }

  /// @brief Requires !is_empty()
  T *
  pop_back_not_empty ()
  {
    T * res ( _back );
    {
      T * const eprev ( _back->chain_prev () );
      // Update chain end pointers
      if ( _back != eprev ) {
        _back = eprev;
        _back->chain_next_set ( _back );
      } else {
        // Prev pointer to self marks end of chain
        _front = nullptr;
        _back = nullptr;
      }
    }
    --_size;
    res->chain_clear ();
    return res;
  }

  // @return Non nullptr if !is_empty()
  T *
  pop_back ()
  {
    if ( _back != nullptr ) {
      return pop_back_not_empty ();
    }
    return nullptr;
  }

  void
  remove ( T * item_n )
  {
    // Update neighbours
    if ( _front == _back ) {
      // The only element
      _front = nullptr;
      _back = nullptr;
    } else {
      // Not the only element
      if ( item_n == _back ) {
        // Last element
        _back = _back->chain_prev ();
        _back->chain_next_set ( _back );
      } else if ( item_n == _front ) {
        // First element
        _front = _front->chain_next ();
        _front->chain_prev_set ( _front );
      } else {
        // Middle element
        item_n->chain_prev ()->chain_next_set ( item_n->chain_next () );
        item_n->chain_next ()->chain_prev_set ( item_n->chain_prev () );
      }
    }
    --_size;
    // Clear chain pointers
    item_n->chain_clear ();
  }

  // -- Moving

  void
  swap ( Chain & chain_n )
  {
    std::swap ( _front, chain_n._front );
    std::swap ( _back, chain_n._back );
    std::swap ( _size, chain_n._size );
  }

  /// @brief Moves this context chain to the back of chain_n
  static void
  move_append_back ( Chain & src_n, Chain & dst_n );

  // -- Operators

  Chain &
  operator= ( const Chain & chain_n ) = delete;

  Chain &
  operator= ( Chain && chain_n ) = delete;

  private:
  T * _front = nullptr;
  T * _back = nullptr;
  std::size_t _size = 0;
};

template < typename T >
void
Chain< T >::move_append_back ( Chain & src_n, Chain & dst_n )
{
  if ( !src_n.is_empty () ) {
    if ( dst_n.is_empty () ) {
      // Copy to target
      dst_n._front = src_n._front;
      dst_n._back = src_n._back;
    } else {
      // Link front and back contexts
      src_n._front->chain_prev_set ( dst_n._back );
      dst_n._back->chain_next_set ( src_n._front );
      // Update target back pointer
      dst_n._back = src_n._back;
    }
    dst_n._size += src_n._size;
    // Clear source
    src_n._front = nullptr;
    src_n._back = nullptr;
    src_n._size = 0;
  }
}

} // namespace sev::logt::server
