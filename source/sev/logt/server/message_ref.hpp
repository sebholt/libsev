/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/logt/server/message.hpp>

namespace sev::logt::server
{

/// @brief Reference to a server internal message
///
/// The reference updates the message reference counter and
/// destroys the message when the counter reaches zero
///
class Message_Ref
{
  public:
  Message_Ref ( logt::server::Message * message_n )
  : _message ( message_n )
  {
    if ( _message != nullptr ) {
      _message->ref_count_increment ();
    }
  }

  Message_Ref ( const logt::server::Message_Ref & ref_n )
  : _message ( ref_n._message )
  {
    if ( _message != nullptr ) {
      _message->ref_count_increment ();
    }
  }

  Message_Ref ( logt::server::Message_Ref && ref_n )
  : _message ( ref_n._message )
  {
    if ( _message != nullptr ) {
      ref_n._message = nullptr;
    }
  }

  ~Message_Ref ()
  {
    if ( _message != nullptr ) {
      _message->ref_count_decrement ();
    }
  }

  // -- Message

  logt::server::Message *
  message () const
  {
    return _message;
  }

  private:
  logt::server::Message * _message;
};

} // namespace sev::logt::server
