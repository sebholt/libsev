/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/logt/flags.hpp>
#include <sev/logt/server/time_point.hpp>

namespace sev::logt::server
{

/// @brief Message_Meta meta information
///
class Message_Meta
{
  public:
  // -- Constructors

  Message_Meta ()
  : _flags ( FL_INFO )
  {
  }

  Message_Meta ( logt::Flags flags_n )
  : _flags ( flags_n )
  {
  }

  void
  clear ();

  // -- Log flags

  logt::Flags
  flags () const
  {
    return _flags;
  }

  void
  set_flags ( logt::Flags flags_n )
  {
    _flags = flags_n;
  }

  // -- Log time

  const logt::server::Time_Point &
  time () const
  {
    return _time;
  }

  logt::server::Time_Point &
  time ()
  {
    return _time;
  }

  private:
  logt::Flags _flags;
  logt::server::Time_Point _time;
};

} // namespace sev::logt::server
