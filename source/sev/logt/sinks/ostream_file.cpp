/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "ostream_file.hpp"
#include <sev/logt/flags.hpp>
#include <sev/logt/sinks/ostream.hpp>

namespace sev::logt::sinks
{

OStream_File::OStream_File ( logt::Flags mask_n )
: logt::sinks::OStream ( 0, mask_n )
{
}

OStream_File::~OStream_File ()
{
  server_unregister ();
  close ();
}

bool
OStream_File::open ( const std::string & file_name_n, bool truncate_n )
{
  bool res ( false );
  close ();
  if ( !file_name_n.empty () ) {
    {
      std::ios_base::openmode mode;
      mode = std::ios_base::out | std::ios_base::binary;
      if ( truncate_n ) {
        mode |= std::ios_base::trunc;
      } else {
        mode |= std::ios_base::app;
      }
      _ofstream.open ( file_name_n, mode );
    }
    if ( _ofstream.is_open () ) {
      set_rdbuf ( _ofstream.rdbuf () );
      res = true;
    }
  }
  return res;
}

void
OStream_File::close ()
{
  if ( _ofstream.is_open () ) {
    set_rdbuf ( nullptr );
    _ofstream.close ();
  }
}

} // namespace sev::logt::sinks
