/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "ostream.hpp"
#include <sev/logt/flags.hpp>
#include <sev/logt/reference.hpp>
#include <sev/logt/server/context.hpp>
#include <sev/logt/server/message.hpp>
#include <sev/logt/server/message_meta.hpp>
#include <sev/logt/sink.hpp>
#include <sstream>

namespace sev::logt::sinks
{

OStream::OStream ( std::streambuf * rdbuf_n, logt::Flags mask_n )
: logt::Sink ( mask_n )
, _ostream ( rdbuf_n )
, _colors_enabled ( false )
, _char_endl ( '\n' )
, _prefix_separator ( "|" )
, _prefix_begin ( "[" )
, _prefix_end ( "] " )
, _col_brace ( "\033[94m" )
, _col_name ( "\033[93m" )
, _col_separator ( "\033[32m" )
, _col_normal ( "\033[0m" )
{
}

OStream::~OStream ()
{
  server_unregister ();
}

void
OStream::set_colors_enabled ( bool flag_n )
{
  _colors_enabled = flag_n;
}

void
OStream::set_rdbuf ( std::streambuf * rdbuf_n )
{
  _ostream.rdbuf ( rdbuf_n );
}

void
OStream::log_message ( logt::server::Message * message_n )
{
  if ( _ostream.rdbuf () != nullptr ) {
    const std::string & text ( message_n->text () );
    if ( mask_test ( message_n->meta ().flags () ) && !text.empty () ) {
      // -- Accumulate text string
      std::ostringstream stream;
      {
        // Acquire line prefix
        std::string line_prefix;
        std::size_t line ( 0 );
        const std::string::size_type text_size ( text.size () );
        std::string::size_type ibegin ( 0 );
        const char * c_str ( text.c_str () );
        // Iterate over text lines
        while ( true ) {
          if ( line == 0 ) {
            acquire_line_prefix (
                line_prefix, message_n->context_ref (), false );
          } else if ( line == 1 ) {
            acquire_line_prefix (
                line_prefix, message_n->context_ref (), true );
          }
          stream << line_prefix;
          // Find next line end
          std::string::size_type endl_pos ( text.find ( _char_endl, ibegin ) );
          if ( endl_pos != std::string::npos ) {
            // End of line found
            stream.write ( c_str + ibegin, endl_pos - ibegin );
            // Update begin index to one after the line end
            ibegin = ( endl_pos + 1 );
          } else {
            // No more end of line found
            stream.write ( c_str + ibegin, text_size - ibegin );
            ibegin = text_size;
          }
          stream.put ( _char_endl );
          if ( ibegin == text_size ) {
            break;
          }
          ++line;
        }
      }
      // -- Write to ostream
      _ostream << stream.str ();
    }
  }
}

void
OStream::flush_request ()
{
}

void
OStream::flush_wait ()
{
  if ( _ostream.rdbuf () != nullptr ) {
    _ostream.flush ();
  }
}

void
OStream::server_session_end ()
{
  this->flush_request ();
  this->flush_wait ();
  logt::Sink::server_session_end ();
}

void
OStream::acquire_line_prefix ( std::string & prefix_n,
                               logt::server::Context * scontext_n,
                               bool continued_n ) const
{
  prefix_n.clear ();
  // Begin brace
  if ( _colors_enabled ) {
    prefix_n.append ( _col_brace );
  }
  prefix_n.append ( _prefix_begin );
  // Address
  {
    // Initialize previous
    const logt::server::Context * scon_root ( scontext_n );
    while ( scon_root->parent_ref () ) {
      scon_root = scon_root->parent_ref ();
    }
    const logt::server::Context * scon_prev ( scon_root );
    // Iterate next to previous context
    while ( scon_prev != scontext_n ) {
      const logt::server::Context * scon_now ( scontext_n );
      const logt::server::Context * scon_parent ( scontext_n );
      do {
        scon_now = scon_parent;
        scon_parent = scon_now->parent_ref ();
      } while ( scon_parent != scon_prev );

      // Append separator on demand
      if ( scon_parent != scon_root ) {
        if ( !continued_n ) {
          if ( _colors_enabled ) {
            prefix_n.append ( _col_separator );
          }
          prefix_n.append ( _prefix_separator );
        } else {
          prefix_n.push_back ( '.' );
        }
      }
      // Append context name
      if ( _colors_enabled ) {
        prefix_n.append ( _col_name );
      }
      if ( !continued_n ) {
        prefix_n.append ( scon_now->context_name () );
      } else {
        prefix_n.append ( scon_now->context_name ().size (), '.' );
      }

      scon_prev = scon_now;
    }
  }
  // End brace
  if ( _colors_enabled ) {
    prefix_n.append ( _col_brace );
  }
  prefix_n.append ( _prefix_end );
  if ( _colors_enabled ) {
    prefix_n.append ( _col_normal );
  }
}

} // namespace sev::logt::sinks
