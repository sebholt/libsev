/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/logt/context.hpp>
#include <sev/logt/server.hpp>
#include <sev/logt/server/context.hpp>

namespace sev::logt
{

// -- Utility

std::string _name_empty;

// -- Class methods

Context::Context () {}

Context::Context ( const Reference & log_alias_n )
: _context_ref ( log_alias_n )
{
}

Context::Context ( const Reference & parent_n, sev::unicode::View name_n )
{
  if ( parent_n ) {
    _context_ref.reset (
        parent_n.server_core ()->context_acquire ( name_n, parent_n ) );
  }
}

void
Context::clear ()
{
  if ( _context_ref.is_valid () ) {
    _context_ref.clear ();
  }
}

Reference
Context::parent_ref () const
{
  if ( _context_ref ) {
    return Reference ( _context_ref->parent_ref () );
  }
  return Reference ();
}

const std::string &
Context::context_name () const
{
  if ( _context_ref ) {
    return _context_ref->context_name ();
  }
  return _name_empty;
}

Flags
Context::mask () const
{
  if ( _context_ref ) {
    return _context_ref->mask_local ();
  }
  return FLG_NONE;
}

void
Context::set_mask ( Flags mask_n )
{
  if ( _context_ref ) {
    _context_ref->set_mask_local ( mask_n );
  }
}

bool
Context::test_flags ( Flags flags_n ) const
{
  if ( _context_ref ) {
    return _context_ref->test_flags ( flags_n );
  }
  return false;
}

void
Context::str ( Flags flags_n, sev::unicode::View span_n ) const
{
  if ( span_n.is_valid () && test_flags ( flags_n ) ) {
    _context_ref->log ( flags_n, span_n.string () );
  }
}

void
Context::log_unchecked ( Flags flags_n, sev::unicode::View span_n ) const
{
  if ( span_n.is_valid () ) {
    _context_ref->log ( flags_n, span_n.string () );
  }
}

void
Context::log_cat_unchecked (
    Flags flags_n, std::initializer_list< std::string_view > views_n ) const
{
  _context_ref->log ( flags_n, sev::string::cat_views ( views_n ) );
}

void
Context::flush () const
{
  if ( _context_ref ) {
    _context_ref->core ()->flush ();
  }
}

void
Context::move_from ( Context && context_n )
{
  clear ();
  _context_ref = std::move ( context_n._context_ref );
}

} // namespace sev::logt
