/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/logt/server.hpp>
#include <sev/logt/server/core.hpp>

namespace sev::logt
{

Server::Server ()
: Server ( Server::mask_default )
{
}

Server::Server ( Flags mask_n )
{
  set_mask ( mask_n );
}

Server::~Server () {}

Flags
Server::mask () const
{
  return _core_ref->mask ();
}

void
Server::set_mask ( Flags mask_n )
{
  _core_ref->set_mask ( mask_n );
}

void
Server::flush ()
{
  _core_ref->flush ();
}

const Reference &
Server::root_context () const
{
  return _core_ref->root_context ();
}

void
Server::sink_register ( Sink * sink_n )
{
  _core_ref->sink_register ( sink_n );
}

void
Server::sink_unregister ( Sink * sink_n )
{
  _core_ref->sink_unregister ( sink_n );
}

} // namespace sev::logt
