/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "utility.hpp"
#include <algorithm>
#include <array>
#include <cstdio>
#include <limits>
#include <locale>
#include <sstream>

namespace sev::string
{

void
clear_and_shrink ( std::string & proc_n )
{
  proc_n.clear ();
  proc_n.shrink_to_fit ();
}

std::size_t
replace_all ( std::string & proc_n,
              const std::string & search_n,
              const std::string & replace_n )
{
  std::size_t res ( 0 );
  std::string::size_type pos ( 0 );
  while ( true ) {
    pos = proc_n.find ( search_n, pos );
    if ( pos != std::string::npos ) {
      proc_n.replace ( pos, search_n.size (), replace_n );
      pos += replace_n.size ();
      ++res;
    } else {
      break;
    }
  }
  return res;
}

bool
remove_at_begin ( std::string & proc_n, const std::string & search_n )
{
  if ( !search_n.empty () && ( proc_n.size () >= search_n.size () ) ) {
    int ret = std::string::traits_type::compare (
        proc_n.c_str (), search_n.c_str (), search_n.size () );
    if ( ret == 0 ) {
      proc_n.erase ( 0, search_n.size () );
      return true;
    }
  }
  return false;
}

bool
replace_at_begin ( std::string & proc_n,
                   const std::string & search_n,
                   const std::string & replace_n )
{
  if ( !search_n.empty () && ( proc_n.size () >= search_n.size () ) ) {
    int ret = std::string::traits_type::compare (
        proc_n.c_str (), search_n.c_str (), search_n.size () );
    if ( ret == 0 ) {
      proc_n.replace ( 0, search_n.size (), replace_n );
      return true;
    }
  }
  return false;
}

bool
remove_at_end ( std::string & proc_n, const std::string & search_n )
{
  if ( !search_n.empty () && ( proc_n.size () >= search_n.size () ) ) {
    int ret = std::string::traits_type::compare (
        proc_n.c_str () + proc_n.size () - search_n.size (),
        search_n.c_str (),
        search_n.size () );
    if ( ret == 0 ) {
      proc_n.resize ( proc_n.size () - search_n.size () );
      return true;
    }
  }
  return false;
}

bool
replace_at_end ( std::string & proc_n,
                 const std::string & search_n,
                 const std::string & replace_n )
{
  if ( !search_n.empty () && ( proc_n.size () >= search_n.size () ) ) {
    const std::size_t pos ( proc_n.size () - search_n.size () );
    int ret = std::string::traits_type::compare (
        proc_n.c_str () + pos, search_n.c_str (), search_n.size () );
    if ( ret == 0 ) {
      proc_n.replace ( pos, search_n.size (), replace_n );
      return true;
    }
  }
  return false;
}

void
trim ( std::string & proc_n )
{
  const std::array< char, 6 > rchars{ { '\t', '\n', '\v', '\f', '\r', ' ' } };
  // Remove at end
  while ( !proc_n.empty () ) {
    bool popped ( false );
    const char cback ( proc_n.back () );
    for ( const char rcha : rchars ) {
      if ( cback == rcha ) {
        proc_n.pop_back ();
        popped = true;
        break;
      }
    }
    if ( !popped ) {
      break;
    }
  }
  // Remove at begin
  while ( !proc_n.empty () ) {
    bool popped ( false );
    const char cfront ( proc_n.front () );
    for ( const char rcha : rchars ) {
      if ( cfront == rcha ) {
        proc_n.erase ( proc_n.begin () );
        popped = true;
        break;
      }
    }
    if ( !popped ) {
      break;
    }
  }
}

std::string
number ( double value_n )
{
  std::ostringstream ost;
  ost.imbue ( std::locale::classic () );
  ost << std::scientific;
  ost.precision ( std::numeric_limits< double >::max_digits10 );
  ost << value_n;
  return ost.str ();
}

std::string
number ( float value_n )
{
  std::ostringstream ost;
  ost.imbue ( std::locale::classic () );
  ost << std::scientific;
  ost.precision ( std::numeric_limits< float >::max_digits10 );
  ost << value_n;
  return ost.str ();
}

std::string
number ( std::int32_t value_n )
{
  std::ostringstream ost;
  ost.imbue ( std::locale::classic () );
  ost << value_n;
  return ost.str ();
}

std::string
number ( std::uint32_t value_n )
{
  std::ostringstream ost;
  ost.imbue ( std::locale::classic () );
  ost << value_n;
  return ost.str ();
}

std::string
number ( std::int64_t value_n )
{
  std::ostringstream ost;
  ost.imbue ( std::locale::classic () );
  ost << value_n;
  return ost.str ();
}

std::string
number ( std::uint64_t value_n )
{
  std::ostringstream ost;
  ost.imbue ( std::locale::classic () );
  ost << value_n;
  return ost.str ();
}

void
set_number ( std::string & proc_n, double value_n )
{
  std::ostringstream ost{ std::string ( proc_n.get_allocator () ) };
  ost.imbue ( std::locale::classic () );
  ost << std::scientific;
  ost.precision ( std::numeric_limits< double >::max_digits10 );
  ost << value_n;
  proc_n = ost.str ();
}

void
set_number ( std::string & proc_n, float value_n )
{
  std::ostringstream ost{ std::string ( proc_n.get_allocator () ) };
  ost.imbue ( std::locale::classic () );
  ost << std::scientific;
  ost.precision ( std::numeric_limits< float >::max_digits10 );
  ost << value_n;
  proc_n = ost.str ();
}

void
set_number ( std::string & proc_n, std::int64_t value_n )
{
  std::ostringstream ost{ std::string ( proc_n.get_allocator () ) };
  ost.imbue ( std::locale::classic () );
  ost << value_n;
  proc_n = ost.str ();
}

void
set_number ( std::string & proc_n, std::uint64_t value_n )
{
  std::ostringstream ost{ std::string ( proc_n.get_allocator () ) };
  ost.imbue ( std::locale::classic () );
  ost << value_n;
  proc_n = ost.str ();
}

bool
get_number ( double & value_n, const std::string & string_n )
{
  std::istringstream sstr ( string_n );
  sstr.imbue ( std::locale::classic () );
  sstr >> std::scientific;
  sstr.precision ( std::numeric_limits< double >::max_digits10 );
  sstr >> value_n;
  return !sstr.fail ();
}

bool
get_number ( float & value_n, const std::string & string_n )
{
  std::istringstream sstr ( string_n );
  sstr.imbue ( std::locale::classic () );
  sstr >> std::scientific;
  sstr.precision ( std::numeric_limits< float >::max_digits10 );
  sstr >> value_n;
  return !sstr.fail ();
}

bool
get_number ( std::int64_t & value_n, const std::string & string_n )
{
  std::istringstream sstr ( string_n );
  sstr.imbue ( std::locale::classic () );
  sstr >> value_n;
  return !sstr.fail ();
}

bool
get_number ( std::uint64_t & value_n, const std::string & string_n )
{
  std::istringstream sstr ( string_n );
  sstr.imbue ( std::locale::classic () );
  sstr >> value_n;
  return !sstr.fail ();
}

namespace
{
template < std::size_t N, typename T >
inline void
make_digits ( std::string_view & view,
              char ( &digits )[ N ],
              const char * pattern,
              T value_n )
{
  int res = std::snprintf ( digits, N, pattern, value_n );
  if ( res > 0 && res < static_cast< int > ( N ) ) {
    view = std::string_view ( digits, static_cast< std::size_t > ( res ) );
  }
}
} // unnamed namespace

Alpha_Num::Alpha_Num ( int val_n )
{
  make_digits ( View_, Digits_, "%i", val_n );
}

Alpha_Num::Alpha_Num ( unsigned int val_n )
{
  make_digits ( View_, Digits_, "%u", val_n );
}

Alpha_Num::Alpha_Num ( long int val_n )
{
  make_digits ( View_, Digits_, "%li", val_n );
}

Alpha_Num::Alpha_Num ( unsigned long int val_n )
{
  make_digits ( View_, Digits_, "%lu", val_n );
}

Alpha_Num::Alpha_Num ( long long int val_n )
{
  make_digits ( View_, Digits_, "%lli", val_n );
}

Alpha_Num::Alpha_Num ( unsigned long long int val_n )
{
  make_digits ( View_, Digits_, "%llu", val_n );
}

Alpha_Num::Alpha_Num ( float val_n )
{
  make_digits ( View_, Digits_, "%g", static_cast< double > ( val_n ) );
}

Alpha_Num::Alpha_Num ( double val_n )
{
  make_digits ( View_, Digits_, "%g", val_n );
}

std::string
cat_views ( std::initializer_list< std::string_view > views_n )
{
  std::size_t total_size = 0;
  for ( std::string_view const & view : views_n ) {
    total_size += view.size ();
  }

  std::string result ( total_size, '\0' );
  std::string::iterator sit = result.begin ();
  for ( std::string_view const & view : views_n ) {
    sit = std::copy_n ( view.data (), view.size (), sit );
  }
  return result;
}

} // namespace sev::string
