/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "math.hpp"
#include "matrix_square_class.hpp"
#include "vector_class.hpp"

namespace sev::lag
{

template < typename FLT >
Matrix3x3< FLT >
m3x3_rotation_x0_rf ( FLT radians_n )
{
  FLT const one ( 1.0 );
  FLT const zero ( 0.0 );
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );
  Matrix3x3< FLT > mres;
  mres[ 0 ] = one;
  mres[ 1 ] = zero;
  mres[ 2 ] = zero;
  mres[ 3 ] = zero;
  mres[ 4 ] = cn;
  mres[ 5 ] = -sn;
  mres[ 6 ] = zero;
  mres[ 7 ] = sn;
  mres[ 8 ] = cn;
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
m3x3_rotation_x0_cf ( FLT radians_n )
{
  FLT const one ( 1.0 );
  FLT const zero ( 0.0 );
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );
  Matrix3x3< FLT > mres;
  mres[ 0 ] = one;
  mres[ 1 ] = zero;
  mres[ 2 ] = zero;
  mres[ 3 ] = zero;
  mres[ 4 ] = cn;
  mres[ 5 ] = sn;
  mres[ 6 ] = zero;
  mres[ 7 ] = -sn;
  mres[ 8 ] = cn;
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
m3x3_rotation_x1_rf ( FLT radians_n )
{
  FLT const one ( 1.0 );
  FLT const zero ( 0.0 );
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );
  Matrix3x3< FLT > mres;
  mres[ 0 ] = cn;
  mres[ 1 ] = zero;
  mres[ 2 ] = sn;
  mres[ 3 ] = zero;
  mres[ 4 ] = one;
  mres[ 5 ] = zero;
  mres[ 6 ] = -sn;
  mres[ 7 ] = zero;
  mres[ 8 ] = cn;
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
m3x3_rotation_x1_cf ( FLT radians_n )
{
  FLT const one ( 1.0 );
  FLT const zero ( 0.0 );
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );
  Matrix3x3< FLT > mres;
  mres[ 0 ] = cn;
  mres[ 1 ] = zero;
  mres[ 2 ] = -sn;
  mres[ 3 ] = zero;
  mres[ 4 ] = one;
  mres[ 5 ] = zero;
  mres[ 6 ] = sn;
  mres[ 7 ] = zero;
  mres[ 8 ] = cn;
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
m3x3_rotation_x2_rf ( FLT radians_n )
{
  FLT const one ( 1.0 );
  FLT const zero ( 0.0 );
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );
  Matrix3x3< FLT > mres;
  mres[ 0 ] = cn;
  mres[ 1 ] = -sn;
  mres[ 2 ] = zero;
  mres[ 3 ] = sn;
  mres[ 4 ] = cn;
  mres[ 5 ] = zero;
  mres[ 6 ] = zero;
  mres[ 7 ] = zero;
  mres[ 8 ] = one;
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
m3x3_rotation_x2_cf ( FLT radians_n )
{
  FLT const one ( 1.0 );
  FLT const zero ( 0.0 );
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );
  Matrix3x3< FLT > mres;
  mres[ 0 ] = cn;
  mres[ 1 ] = sn;
  mres[ 2 ] = zero;
  mres[ 3 ] = -sn;
  mres[ 4 ] = cn;
  mres[ 5 ] = zero;
  mres[ 6 ] = zero;
  mres[ 7 ] = zero;
  mres[ 8 ] = one;
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
m3x3_rotation_axis_rf ( Vector3< FLT > const & a_n, FLT radians_n )
{
  Matrix3x3< FLT > mres;

  FLT const sn ( std::sin ( radians_n ) );
  FLT cn ( std::cos ( radians_n ) );
  FLT tmp;

  // 0, 4, 8
  mres[ 0 ] = cn;
  mres[ 4 ] = cn;
  mres[ 8 ] = cn;

  cn = FLT ( 1.0 ) - cn;

  mres[ 0 ] += a_n[ 0 ] * a_n[ 0 ] * cn;
  mres[ 4 ] += a_n[ 1 ] * a_n[ 1 ] * cn;
  mres[ 8 ] += a_n[ 2 ] * a_n[ 2 ] * cn;

  // 1, 3
  tmp = a_n[ 2 ] * sn;

  mres[ 3 ] = tmp;
  mres[ 1 ] = -tmp;

  tmp = a_n[ 0 ] * a_n[ 1 ] * cn;

  mres[ 1 ] += tmp;
  mres[ 3 ] += tmp;

  // 2, 6
  tmp = a_n[ 1 ] * sn;

  mres[ 2 ] = tmp;
  mres[ 6 ] = -tmp;

  tmp = a_n[ 0 ] * a_n[ 2 ] * cn;

  mres[ 2 ] += tmp;
  mres[ 6 ] += tmp;

  // 5, 7
  tmp = a_n[ 0 ] * sn;

  mres[ 7 ] = tmp;
  mres[ 5 ] = -tmp;

  tmp = a_n[ 1 ] * a_n[ 2 ] * cn;

  mres[ 5 ] += tmp;
  mres[ 7 ] += tmp;

  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
m3x3_rotation_axis_cf ( Vector3< FLT > const & a_n, FLT radians_n )
{
  Matrix3x3< FLT > mres;

  FLT const sn ( std::sin ( radians_n ) );
  FLT cn ( std::cos ( radians_n ) );
  FLT tmp;

  // 0, 4, 8
  mres[ 0 ] = cn;
  mres[ 4 ] = cn;
  mres[ 8 ] = cn;

  cn = FLT ( 1.0 ) - cn;

  mres[ 0 ] += a_n[ 0 ] * a_n[ 0 ] * cn;
  mres[ 4 ] += a_n[ 1 ] * a_n[ 1 ] * cn;
  mres[ 8 ] += a_n[ 2 ] * a_n[ 2 ] * cn;

  // 1, 3
  tmp = a_n[ 2 ] * sn;

  mres[ 1 ] = tmp;
  mres[ 3 ] = -tmp;

  tmp = a_n[ 0 ] * a_n[ 1 ] * cn;

  mres[ 1 ] += tmp;
  mres[ 3 ] += tmp;

  // 2, 6
  tmp = a_n[ 1 ] * sn;

  mres[ 2 ] = -tmp;
  mres[ 6 ] = tmp;

  tmp = a_n[ 0 ] * a_n[ 2 ] * cn;

  mres[ 2 ] += tmp;
  mres[ 6 ] += tmp;

  // 5, 7
  tmp = a_n[ 0 ] * sn;

  mres[ 5 ] = tmp;
  mres[ 7 ] = -tmp;

  tmp = a_n[ 1 ] * a_n[ 2 ] * cn;

  mres[ 5 ] += tmp;
  mres[ 7 ] += tmp;

  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
rotated_x0_rf ( Matrix3x3< FLT > const & msrc_n, FLT radians_n )
{
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );

  Matrix3x3< FLT > mres;
  mres[ 0 ] = msrc_n[ 0 ];
  mres[ 1 ] = msrc_n[ 1 ];
  mres[ 2 ] = msrc_n[ 2 ];

  mres[ 3 ] = cn * msrc_n[ 3 ] - sn * msrc_n[ 6 ];
  mres[ 4 ] = cn * msrc_n[ 4 ] - sn * msrc_n[ 7 ];
  mres[ 5 ] = cn * msrc_n[ 5 ] - sn * msrc_n[ 8 ];

  mres[ 6 ] = sn * msrc_n[ 3 ] + cn * msrc_n[ 6 ];
  mres[ 7 ] = sn * msrc_n[ 4 ] + cn * msrc_n[ 7 ];
  mres[ 8 ] = sn * msrc_n[ 5 ] + cn * msrc_n[ 8 ];
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
rotated_x0_cf ( Matrix3x3< FLT > const & msrc_n, FLT radians_n )
{
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );

  Matrix3x3< FLT > mres;
  mres[ 0 ] = msrc_n[ 0 ];
  mres[ 1 ] = cn * msrc_n[ 1 ] - sn * msrc_n[ 2 ];
  mres[ 2 ] = sn * msrc_n[ 1 ] + cn * msrc_n[ 2 ];

  mres[ 3 ] = msrc_n[ 3 ];
  mres[ 4 ] = cn * msrc_n[ 4 ] - sn * msrc_n[ 5 ];
  mres[ 5 ] = sn * msrc_n[ 4 ] + cn * msrc_n[ 5 ];

  mres[ 6 ] = msrc_n[ 6 ];
  mres[ 7 ] = cn * msrc_n[ 7 ] - sn * msrc_n[ 8 ];
  mres[ 8 ] = sn * msrc_n[ 7 ] + cn * msrc_n[ 8 ];
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
rotated_x1_rf ( Matrix3x3< FLT > const & msrc_n, FLT radians_n )
{
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );

  Matrix3x3< FLT > mres;
  mres[ 0 ] = cn * msrc_n[ 0 ] + sn * msrc_n[ 6 ];
  mres[ 1 ] = cn * msrc_n[ 1 ] + sn * msrc_n[ 7 ];
  mres[ 2 ] = cn * msrc_n[ 2 ] + sn * msrc_n[ 8 ];

  mres[ 3 ] = msrc_n[ 3 ];
  mres[ 4 ] = msrc_n[ 4 ];
  mres[ 5 ] = msrc_n[ 5 ];

  mres[ 6 ] = -sn * msrc_n[ 0 ] + cn * msrc_n[ 6 ];
  mres[ 7 ] = -sn * msrc_n[ 1 ] + cn * msrc_n[ 7 ];
  mres[ 8 ] = -sn * msrc_n[ 2 ] + cn * msrc_n[ 8 ];
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
rotated_x1_cf ( Matrix3x3< FLT > const & msrc_n, FLT radians_n )
{
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );

  Matrix3x3< FLT > mres;
  mres[ 0 ] = cn * msrc_n[ 0 ] + sn * msrc_n[ 2 ];
  mres[ 1 ] = msrc_n[ 1 ];
  mres[ 2 ] = -sn * msrc_n[ 0 ] + cn * msrc_n[ 2 ];

  mres[ 3 ] = cn * msrc_n[ 3 ] + sn * msrc_n[ 5 ];
  mres[ 4 ] = msrc_n[ 4 ];
  mres[ 5 ] = -sn * msrc_n[ 3 ] + cn * msrc_n[ 5 ];

  mres[ 6 ] = cn * msrc_n[ 6 ] + sn * msrc_n[ 8 ];
  mres[ 7 ] = msrc_n[ 7 ];
  mres[ 8 ] = -sn * msrc_n[ 6 ] + cn * msrc_n[ 8 ];
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
rotated_x2_rf ( Matrix3x3< FLT > const & msrc_n, FLT radians_n )
{
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );

  Matrix3x3< FLT > mres;
  mres[ 0 ] = cn * msrc_n[ 0 ] - sn * msrc_n[ 3 ];
  mres[ 1 ] = cn * msrc_n[ 1 ] - sn * msrc_n[ 4 ];
  mres[ 2 ] = cn * msrc_n[ 2 ] - sn * msrc_n[ 5 ];

  mres[ 3 ] = sn * msrc_n[ 0 ] + cn * msrc_n[ 3 ];
  mres[ 4 ] = sn * msrc_n[ 1 ] + cn * msrc_n[ 4 ];
  mres[ 5 ] = sn * msrc_n[ 2 ] + cn * msrc_n[ 5 ];

  mres[ 6 ] = msrc_n[ 6 ];
  mres[ 7 ] = msrc_n[ 7 ];
  mres[ 8 ] = msrc_n[ 8 ];
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
rotated_x2_cf ( Matrix3x3< FLT > const & msrc_n, FLT radians_n )
{
  FLT const sn ( std::sin ( radians_n ) );
  FLT const cn ( std::cos ( radians_n ) );

  Matrix3x3< FLT > mres;
  mres[ 0 ] = cn * msrc_n[ 0 ] - sn * msrc_n[ 1 ];
  mres[ 1 ] = sn * msrc_n[ 0 ] + cn * msrc_n[ 1 ];
  mres[ 2 ] = msrc_n[ 2 ];

  mres[ 3 ] = cn * msrc_n[ 3 ] - sn * msrc_n[ 4 ];
  mres[ 4 ] = sn * msrc_n[ 3 ] + cn * msrc_n[ 4 ];
  mres[ 5 ] = msrc_n[ 5 ];

  mres[ 6 ] = cn * msrc_n[ 6 ] - sn * msrc_n[ 7 ];
  mres[ 7 ] = sn * msrc_n[ 6 ] + cn * msrc_n[ 7 ];
  mres[ 8 ] = msrc_n[ 8 ];
  return mres;
}

template < typename FLT >
Matrix3x3< FLT >
rotated_axis_rf ( Matrix3x3< FLT > const & msrc_n,
                  Vector3< FLT > const & axis_n,
                  FLT radians_n )
{
  return mult_rf ( m3x3_rotation_axis_rf ( axis_n, radians_n ), msrc_n );
}

template < typename FLT >
Matrix3x3< FLT >
rotated_axis_cf ( Matrix3x3< FLT > const & msrc_n,
                  Vector3< FLT > const & axis_n,
                  FLT radians_n )
{
  return mult_cf ( m3x3_rotation_axis_cf ( axis_n, radians_n ), msrc_n );
}

template < typename FLT >
void
rotate_x0_rf ( Matrix3x3< FLT > & mat_n, FLT radians_n )
{
  mat_n = rotated_x0_rf ( mat_n, radians_n );
}

template < typename FLT >
void
rotate_x0_cf ( Matrix3x3< FLT > & mat_n, FLT radians_n )
{
  mat_n = rotated_x0_cf ( mat_n, radians_n );
}

template < typename FLT >
void
rotate_x1_rf ( Matrix3x3< FLT > & mat_n, FLT radians_n )
{

  mat_n = rotated_x1_rf ( mat_n, radians_n );
}

template < typename FLT >
void
rotate_x1_cf ( Matrix3x3< FLT > & mat_n, FLT radians_n )
{
  mat_n = rotated_x1_cf ( mat_n, radians_n );
}

template < typename FLT >
void
rotate_x2_rf ( Matrix3x3< FLT > & mat_n, FLT radians_n )
{
  mat_n = rotated_x2_rf ( mat_n, radians_n );
}

template < typename FLT >
void
rotate_x2_cf ( Matrix3x3< FLT > & mat_n, FLT radians_n )
{
  mat_n = rotated_x2_cf ( mat_n, radians_n );
}

template < typename FLT >
void
rotate_axis_rf ( Matrix3x3< FLT > & mat_n,
                 Vector3< FLT > const & axis_n,
                 FLT radians_n )
{
  mat_n = rotated_axis_rf ( mat_n, axis_n, radians_n );
}

template < typename FLT >
void
rotate_axis_cf ( Matrix3x3< FLT > & mat_n,
                 Vector3< FLT > const & axis_n,
                 FLT radians_n )
{
  mat_n = rotated_axis_cf ( mat_n, axis_n, radians_n );
}

} // namespace sev::lag
