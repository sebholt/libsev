/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "matrix_square_class.hpp"
#include "matrix_square_math.hpp"
#include "vector_class.hpp"

namespace sev::lag
{

// Transpose

template < typename FLT >
inline void
transpose ( Matrix2x2< FLT > & mat_n )
{
  std::swap ( mat_n[ 1 ], mat_n[ 2 ] );
}

template < typename FLT >
inline Matrix2x2< FLT >
transposed ( Matrix2x2< FLT > const & msrc_n )
{
  Matrix2x2< FLT > mres;
  mres[ 0 ] = msrc_n[ 0 ];
  mres[ 1 ] = msrc_n[ 2 ];
  mres[ 2 ] = msrc_n[ 1 ];
  mres[ 3 ] = msrc_n[ 3 ];
  return mres;
}

// Determinant

template < typename FLT >
inline FLT
determinant ( Matrix2x2< FLT > const & mat_n )
{
  return ( mat_n[ 0 ] * mat_n[ 3 ] - mat_n[ 1 ] * mat_n[ 2 ] );
}

// Inverse

template < typename FLT >
Matrix2x2< FLT >
inverse_det ( Matrix2x2< FLT > const & msrc_n, FLT det_n )
{
  Matrix2x2< FLT > mres;
  mres[ 0 ] = msrc_n[ 3 ];
  mres[ 1 ] = -msrc_n[ 1 ];
  mres[ 2 ] = -msrc_n[ 2 ];
  mres[ 3 ] = msrc_n[ 0 ];
  mres /= det_n;
  return mres;
}

template < typename FLT >
void
invert_det ( Matrix2x2< FLT > & mat_n, FLT det_n )
{
  std::swap ( mat_n[ 0 ], mat_n[ 3 ] );
  mat_n[ 1 ] = -mat_n[ 1 ];
  mat_n[ 2 ] = -mat_n[ 2 ];
  mat_n /= det_n;
}

template < typename FLT >
bool
invert ( Matrix2x2< FLT > & mat_n )
{
  FLT const det ( determinant ( mat_n ) );
  if ( det != 0.0 ) {
    invert_det ( mat_n, det );
    return true;
  }
  return false;
}

template < typename FLT >
bool
inverse ( Matrix2x2< FLT > & mres_n, Matrix2x2< FLT > const & msrc_n )
{
  FLT const det ( determinant ( msrc_n ) );
  if ( det != 0.0 ) {
    inverse_det ( mres_n, msrc_n, det );
    return true;
  }
  return false;
}

template < typename FLT >
bool
solve_rf ( Vector2< FLT > & vres_n,
           Matrix2x2< FLT > const & msrc_n,
           Vector2< FLT > const & vsrc_n )
{
  Matrix2x2< FLT > mat_i;
  if ( inverse ( mat_i, msrc_n ) ) {
    mult_rf ( vres_n, mat_i, vsrc_n );
    return true;
  }
  return false;
}

template < typename FLT >
bool
solve_cf ( Vector2< FLT > & vres_n,
           Matrix2x2< FLT > const & msrc_n,
           Vector2< FLT > const & vsrc_n )
{
  Matrix2x2< FLT > mat_i;
  if ( inverse ( mat_i, msrc_n ) ) {
    mult_cf ( vres_n, mat_i, vsrc_n );
    return true;
  }
  return false;
}

} // namespace sev::lag
