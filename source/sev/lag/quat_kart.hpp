/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "matrix_square_class.hpp"
#include "quat_class.hpp"
#include "quat_math.hpp"
#include "vector_class.hpp"

namespace sev::lag
{

template < typename FLT >
inline constexpr void
quat_set_rotation_x0 ( Quat< FLT > & quat_n, FLT radians_n )
{
  FLT const rad2 ( radians_n / 2.0 );
  FLT const sn ( std::sin ( rad2 ) );
  FLT const cn ( std::cos ( rad2 ) );
  quat_n[ 0 ] = cn;
  quat_n[ 1 ] = sn;
  quat_n[ 2 ] = FLT ( 0.0 );
  quat_n[ 3 ] = FLT ( 0.0 );
}

template < typename FLT >
inline constexpr void
quat_set_rotation_x1 ( Quat< FLT > & quat_n, FLT radians_n )
{
  FLT const rad2 ( radians_n / 2.0 );
  FLT const sn ( std::sin ( rad2 ) );
  FLT const cn ( std::cos ( rad2 ) );
  quat_n[ 0 ] = cn;
  quat_n[ 1 ] = FLT ( 0.0 );
  quat_n[ 2 ] = sn;
  quat_n[ 3 ] = FLT ( 0.0 );
}

template < typename FLT >
inline constexpr void
quat_set_rotation_x2 ( Quat< FLT > & quat_n, FLT radians_n )
{
  FLT const rad2 ( radians_n / 2.0 );
  FLT const sn ( std::sin ( rad2 ) );
  FLT const cn ( std::cos ( rad2 ) );
  quat_n[ 0 ] = cn;
  quat_n[ 1 ] = FLT ( 0.0 );
  quat_n[ 2 ] = FLT ( 0.0 );
  quat_n[ 3 ] = sn;
}

template < typename FLT >
inline constexpr void
quat_set_rotation_axis ( Quat< FLT > & quat_n,
                         Vector3< FLT > const & vaxis_n,
                         FLT radians_n )
{
  FLT const rad2 ( radians_n / 2.0 );
  FLT const sn ( std::sin ( rad2 ) );
  FLT const cn ( std::cos ( rad2 ) );
  quat_n[ 0 ] = cn;
  quat_n[ 1 ] = vaxis_n[ 0 ] * sn;
  quat_n[ 2 ] = vaxis_n[ 1 ] * sn;
  quat_n[ 3 ] = vaxis_n[ 2 ] * sn;
}

template < typename FLT >
inline constexpr void
quat_rotated_x0 ( Quat< FLT > & qres_n,
                  Quat< FLT > const & qsrc_n,
                  FLT radians_n )
{
  Quat< FLT > tmp;
  quat_set_rotation_x0 ( tmp, radians_n );
  quat_mult ( qres_n, tmp, qsrc_n );
}

template < typename FLT >
inline constexpr void
quat_rotated_x1 ( Quat< FLT > & qres_n,
                  Quat< FLT > const & qsrc_n,
                  FLT radians_n )
{
  Quat< FLT > tmp;
  quat_set_rotation_x1 ( tmp, radians_n );
  quat_mult ( qres_n, tmp, qsrc_n );
}

template < typename FLT >
inline constexpr void
quat_rotated_x2 ( Quat< FLT > & qres_n,
                  Quat< FLT > const & qsrc_n,
                  FLT radians_n )
{
  Quat< FLT > tmp;
  quat_set_rotation_x2 ( tmp, radians_n );
  quat_mult ( qres_n, tmp, qsrc_n );
}

template < typename FLT >
inline constexpr void
quat_rotated_axis ( Quat< FLT > & qres_n,
                    Quat< FLT > const & qsrc_n,
                    Vector3< FLT > const & vaxis_n,
                    FLT radians_n )
{
  Quat< FLT > tmp;
  quat_set_rotation_axis ( tmp, vaxis_n, radians_n );
  quat_mult ( qres_n, tmp, qsrc_n );
}

template < typename FLT >
inline constexpr void
quat_rotate_x0 ( Quat< FLT > & quat_n, FLT radians_n )
{
  Quat< FLT > tmp;
  quat_rotated_x0 ( tmp, quat_n, radians_n );
  quat_n = tmp;
}

template < typename FLT >
inline constexpr void
quat_rotate_x1 ( Quat< FLT > & quat_n, FLT radians_n )
{
  Quat< FLT > tmp;
  quat_rotated_x1 ( tmp, quat_n, radians_n );
  quat_n = tmp;
}

template < typename FLT >
inline constexpr void
quat_rotate_x2 ( Quat< FLT > & quat_n, FLT radians_n )
{
  Quat< FLT > tmp;
  quat_rotated_x2 ( tmp, quat_n, radians_n );
  quat_n = tmp;
}

template < typename FLT >
inline constexpr void
quat_rotate_axis ( Quat< FLT > & quat_n,
                   Vector3< FLT > const & vaxis_n,
                   FLT radians_n )
{
  Quat< FLT > tmp;
  quat_rotated_axis ( tmp, quat_n, vaxis_n, radians_n );
  quat_n = tmp;
}

template < typename FLT >
inline constexpr void
quat_to_mat_rf ( Matrix3x3< FLT > & mat_n, Quat< FLT > const & quat_n )
{
  FLT const one ( 1.0 );
  FLT const two ( 2.0 );

  FLT tt1;
  FLT tt2;

  // 0, 4, 8
  FLT xx ( quat_n[ 1 ] * quat_n[ 1 ] );
  FLT yy ( quat_n[ 2 ] * quat_n[ 2 ] );
  FLT zz ( quat_n[ 3 ] * quat_n[ 3 ] );
  mat_n[ 0 ] = one - two * ( yy + zz );
  mat_n[ 4 ] = one - two * ( xx + zz );
  mat_n[ 8 ] = one - two * ( xx + yy );

  // 1, 3
  tt1 = quat_n[ 1 ] * quat_n[ 2 ];
  tt2 = quat_n[ 0 ] * quat_n[ 3 ];
  tt1 *= two;
  tt2 *= two;

  mat_n[ 1 ] = tt1 - tt2;
  mat_n[ 3 ] = tt1 + tt2;

  // 2, 6
  tt1 = quat_n[ 1 ] * quat_n[ 3 ];
  tt2 = quat_n[ 0 ] * quat_n[ 2 ];
  tt1 *= two;
  tt2 *= two;

  mat_n[ 2 ] = tt1 + tt2;
  mat_n[ 6 ] = tt1 - tt2;

  // 5, 7
  tt1 = quat_n[ 2 ] * quat_n[ 3 ];
  tt2 = quat_n[ 0 ] * quat_n[ 1 ];
  tt1 *= two;
  tt2 *= two;

  mat_n[ 5 ] = tt1 - tt2;
  mat_n[ 7 ] = tt1 + tt2;
}

template < typename FLT >
inline constexpr void
quat_to_mat_cf ( Matrix3x3< FLT > & mat_n, Quat< FLT > const & quat_n )
{
  FLT const one ( 1.0 );
  FLT const two ( 2.0 );

  FLT tt1;
  FLT tt2;

  // 0, 4, 8
  FLT xx ( quat_n[ 1 ] * quat_n[ 1 ] );
  FLT yy ( quat_n[ 2 ] * quat_n[ 2 ] );
  FLT zz ( quat_n[ 3 ] * quat_n[ 3 ] );
  mat_n[ 0 ] = one - two * ( yy + zz );
  mat_n[ 4 ] = one - two * ( xx + zz );
  mat_n[ 8 ] = one - two * ( xx + yy );

  // 1, 3
  tt1 = quat_n[ 1 ] * quat_n[ 2 ];
  tt2 = quat_n[ 0 ] * quat_n[ 3 ];
  tt1 *= two;
  tt2 *= two;

  mat_n[ 1 ] = tt1 + tt2;
  mat_n[ 3 ] = tt1 - tt2;

  // 2, 6
  tt1 = quat_n[ 1 ] * quat_n[ 3 ];
  tt2 = quat_n[ 0 ] * quat_n[ 2 ];
  tt1 *= two;
  tt2 *= two;

  mat_n[ 2 ] = tt1 - tt2;
  mat_n[ 6 ] = tt1 + tt2;

  // 5, 7
  tt1 = quat_n[ 2 ] * quat_n[ 3 ];
  tt2 = quat_n[ 0 ] * quat_n[ 1 ];
  tt1 *= two;
  tt2 *= two;

  mat_n[ 5 ] = tt1 + tt2;
  mat_n[ 7 ] = tt1 - tt2;
}

} // namespace sev::lag
