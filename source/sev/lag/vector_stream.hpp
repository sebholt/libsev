/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "vector_class.hpp"
#include <cstdint>
#include <istream>
#include <ostream>

namespace sev::lag
{

template < typename FLT, std::size_t DIM >
inline std::ostream &
operator<< ( std::ostream & ostr_n, Vector< FLT, DIM > const & vector_n )
{
  // Width must be reset for every value
  const int width ( ostr_n.width () );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    if ( ii != 0 ) {
      ostr_n << ' ';
      // Set width for next value
      ostr_n.width ( width );
    }
    ostr_n << vector_n.x ( ii );
  }
  return ostr_n;
}

template < typename FLT, std::size_t DIM >
std::istream &
operator>> ( std::istream & istr_n, Vector< FLT, DIM > & vector_n )
{
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    istr_n >> vector_n.xr ( ii );
  }
  return istr_n;
}

} // namespace sev::lag
