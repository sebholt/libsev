/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "math_array.hpp"
#include "matrix_square_class.hpp"
#include "vector_class.hpp"
#include <cstdint>

namespace sev::lag
{

// Comparison

template < typename FLT, std::size_t DIM >
inline bool
match ( Matrix_Square< FLT, DIM > const & mat0_n,
        Matrix_Square< FLT, DIM > const & mat1_n,
        FLT epsilon_n )
{
  return array_match ( mat0_n.begin (),
                       mat1_n.begin (),
                       Matrix_Square< FLT, DIM >::vsize,
                       epsilon_n );
}

template < typename FLT, std::size_t DIM >
inline void
adjust_almost_zero ( Matrix_Square< FLT, DIM > & mat_n, FLT epsilon_n )
{
  array_adjust_almost_zero (
      mat_n.begin (), Matrix_Square< FLT, DIM >::vsize, epsilon_n );
}

// Matrix multiplication

template < typename FLT, std::size_t DIM >
Matrix_Square< FLT, DIM >
mult_rf ( Matrix_Square< FLT, DIM > const & ma_n,
          Matrix_Square< FLT, DIM > const & mb_n )
{
  Matrix_Square< FLT, DIM > mres;
  FLT const zero ( 0.0 );
  FLT * ptr ( mres.begin () );
  for ( std::size_t rr = 0; rr != DIM; ++rr ) {
    for ( std::size_t cc = 0; cc != DIM; ++cc ) {
      FLT & dst ( *ptr );
      dst = zero;
      for ( std::size_t ii = 0; ii != DIM; ++ii ) {
        dst += ma_n[ rr * DIM + ii ] * mb_n[ cc + ii * DIM ];
      }
      ++ptr;
    }
  }
  return mres;
}

template < typename FLT, std::size_t DIM >
Matrix_Square< FLT, DIM >
mult_cf ( Matrix_Square< FLT, DIM > const & ma_n,
          Matrix_Square< FLT, DIM > const & mb_n )
{
  Matrix_Square< FLT, DIM > mres;
  FLT const zero ( 0.0 );
  FLT * ptr ( mres.begin () );
  for ( std::size_t cc = 0; cc != DIM; ++cc ) {
    for ( std::size_t rr = 0; rr != DIM; ++rr ) {
      FLT & dst ( *ptr );
      dst = zero;
      for ( std::size_t ii = 0; ii != DIM; ++ii ) {
        dst += ma_n[ rr + ii * DIM ] * mb_n[ cc * DIM + ii ];
      }
      ++ptr;
    }
  }
  return mres;
}

template < typename FLT, std::size_t DIM >
void
mult_b_a_rf ( Matrix_Square< FLT, DIM > & ma_n,
              Matrix_Square< FLT, DIM > const & mb_n )
{
  ma_n = mult_rf ( mb_n, ma_n );
}

template < typename FLT, std::size_t DIM >
void
mult_b_a_cf ( Matrix_Square< FLT, DIM > & ma_n,
              Matrix_Square< FLT, DIM > const & mb_n )
{
  ma_n = mult_cf ( mb_n, ma_n );
}

template < typename FLT, std::size_t DIM >
inline void
mult_a_b_rf ( Matrix_Square< FLT, DIM > & ma_n,
              Matrix_Square< FLT, DIM > const & mb_n )
{
  ma_n = mult_rf ( ma_n, mb_n );
}

template < typename FLT, std::size_t DIM >
inline void
mult_a_b_cf ( Matrix_Square< FLT, DIM > & ma_n,
              Matrix_Square< FLT, DIM > const & mb_n )
{
  ma_n = mult_cf ( ma_n, mb_n );
}

// Vector multiplication

template < typename FLT, std::size_t DIM >
Vector< FLT, DIM >
mult_rf ( Matrix_Square< FLT, DIM > const & mat_n,
          Vector< FLT, DIM > const & vsrc_n )
{
  Vector< FLT, DIM > vres;
  FLT const zero ( 0.0 );
  FLT * ptr ( vres.begin () );
  for ( std::size_t rr = 0; rr != DIM; ++rr ) {
    FLT & dst ( *ptr );
    dst = zero;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      dst += mat_n[ rr * DIM + ii ] * vsrc_n[ ii ];
    }
    ++ptr;
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
Vector< FLT, DIM >
mult_cf ( Matrix_Square< FLT, DIM > const & mat_n,
          Vector< FLT, DIM > const & vsrc_n )
{
  Vector< FLT, DIM > vres;
  FLT const zero ( 0.0 );
  FLT * ptr ( vres.begin () );
  for ( std::size_t rr = 0; rr != DIM; ++rr ) {
    FLT & dst ( *ptr );
    dst = zero;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      dst += mat_n[ rr + ii * DIM ] * vsrc_n[ ii ];
    }
    ++ptr;
  }
  return vres;
}

template < typename FLT, std::size_t DIM >
inline void
mult_rf ( Vector< FLT, DIM > & vdst_n, Matrix_Square< FLT, DIM > const & mat_n )
{
  vdst_n = mult_rf ( mat_n, vdst_n );
}

template < typename FLT, std::size_t DIM >
inline void
mult_cf ( Vector< FLT, DIM > & vdst_n, Matrix_Square< FLT, DIM > const & mat_n )
{
  vdst_n = mult_cf ( mat_n, vdst_n );
}

// Vector array multiplication

template < typename FLT, std::size_t DIM >
void
mult_varray_rf ( Vector< FLT, DIM > * vdst_n,
                 std::size_t num_n,
                 Matrix_Square< FLT, DIM > const & mat_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    mult_rf ( vdst_n[ ii ], mat_n );
  }
}

template < typename FLT, std::size_t DIM >
void
mult_varray_cf ( Vector< FLT, DIM > * vdst_n,
                 std::size_t num_n,
                 Matrix_Square< FLT, DIM > const & mat_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    mult_cf ( vdst_n[ ii ], mat_n );
  }
}

template < typename FLT, std::size_t DIM >
void
mult_varray_rf ( Vector< FLT, DIM > * vres_n,
                 std::size_t num_n,
                 Matrix_Square< FLT, DIM > const & mat_n,
                 Vector< FLT, DIM > const * vsrc_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    mult_rf ( vres_n[ ii ], mat_n, vsrc_n[ ii ] );
  }
}

template < typename FLT, std::size_t DIM >
void
mult_varray_cf ( Vector< FLT, DIM > * vres_n,
                 std::size_t num_n,
                 Matrix_Square< FLT, DIM > const & mat_n,
                 Vector< FLT, DIM > const * vsrc_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    mult_cf ( vres_n[ ii ], mat_n, vsrc_n[ ii ] );
  }
}

} // namespace sev::lag
