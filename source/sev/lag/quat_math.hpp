/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "math.hpp"
#include "quat_class.hpp"

namespace sev::lag
{

template < typename FLT >
inline constexpr FLT
quat_magnitude2 ( Quat< FLT > const & quat_n )
{
  return square ( quat_n.x ( 0 ) ) + square ( quat_n.x ( 1 ) ) +
         square ( quat_n.x ( 2 ) ) + square ( quat_n.x ( 3 ) );
}

template < typename FLT >
inline constexpr FLT
quat_magnitude ( Quat< FLT > const & quat_n )
{
  return square_root ( quat_magnitude2 ( quat_n ) );
}

template < typename FLT >
inline constexpr void
quat_conjugate ( Quat< FLT > & quat_n )
{
  for ( std::size_t ii = 1; ii != 4; ++ii ) {
    quat_n[ ii ] = -quat_n[ ii ];
  }
}

template < typename FLT >
inline constexpr void
quat_conjugated ( Quat< FLT > & qdst, Quat< FLT > const & qsrc )
{
  qdst[ 0 ] = qsrc[ 0 ];
  for ( std::size_t ii = 1; ii != 4; ++ii ) {
    qdst[ ii ] = -qsrc[ ii ];
  }
}

template < typename FLT >
inline constexpr void
quat_normalize ( Quat< FLT > & quat_n )
{
  FLT mag ( quat_magnitude ( quat_n ) );
  for ( std::size_t ii = 0; ii != 4; ++ii ) {
    quat_n[ ii ] /= mag;
  }
}

template < typename FLT >
inline constexpr void
quat_normalized ( Quat< FLT > & qdst, Quat< FLT > const & qsrc )
{
  FLT mag ( quat_magnitude ( qsrc ) );
  for ( std::size_t ii = 0; ii != 4; ++ii ) {
    qdst[ ii ] = qsrc[ ii ] / mag;
  }
}

template < typename FLT >
inline constexpr void
quat_mult ( Quat< FLT > & qdst, Quat< FLT > const & qa, Quat< FLT > const & qb )
{
  qdst.set_x ( 0,
               qa.x ( 0 ) * qb.x ( 0 ) - qa.x ( 1 ) * qb.x ( 1 ) -
                   qa.x ( 2 ) * qb.x ( 2 ) - qa.x ( 3 ) * qb.x ( 3 ) );
  qdst.set_x ( 1,
               qa.x ( 0 ) * qb.x ( 1 ) + qa.x ( 1 ) * qb.x ( 0 ) +
                   qa.x ( 2 ) * qb.x ( 3 ) - qa.x ( 3 ) * qb.x ( 2 ) );
  qdst.set_x ( 2,
               qa.x ( 0 ) * qb.x ( 2 ) - qa.x ( 1 ) * qb.x ( 3 ) +
                   qa.x ( 2 ) * qb.x ( 0 ) + qa.x ( 3 ) * qb.x ( 1 ) );
  qdst.set_x ( 3,
               qa.x ( 0 ) * qb.x ( 3 ) + qa.x ( 1 ) * qb.x ( 2 ) -
                   qa.x ( 2 ) * qb.x ( 1 ) + qa.x ( 3 ) * qb.x ( 0 ) );
}

template < typename FLT >
inline constexpr void
quat_mult_b_a ( Quat< FLT > & qa, Quat< FLT > const & qb )
{
  Quat< FLT > tmp;
  quat_mult ( tmp, qb, qa );
  qa = tmp;
}

template < typename FLT >
inline constexpr void
quat_mult_a_b ( Quat< FLT > & qa, Quat< FLT > const & qb )
{
  Quat< FLT > tmp;
  quat_mult ( tmp, qa, qb );
  qa = tmp;
}

} // namespace sev::lag
