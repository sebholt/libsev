/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "quat_class.hpp"
#include <istream>
#include <ostream>

namespace sev::lag
{

template < typename FLT >
inline std::ostream &
operator<< ( std::ostream & ostr_n, Quat< FLT > const & quat_n )
{
  const int width ( ostr_n.width () );
  for ( std::size_t ii = 0; ii != 4; ++ii ) {
    if ( ii != 0 ) {
      ostr_n << ' ';
      ostr_n.width ( width );
    }
    ostr_n << quat_n.x ( ii );
  }
  return ostr_n;
}

template < typename FLT >
inline std::istream &
operator>> ( std::istream & istr_n, Quat< FLT > & quat_n )
{
  for ( std::size_t ii = 0; ii != 4; ++ii ) {
    istr_n >> quat_n.xr ( ii );
  }
  return istr_n;
}

} // namespace sev::lag
