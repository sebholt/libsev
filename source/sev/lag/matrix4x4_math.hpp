/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "matrix_square_class.hpp"
#include "matrix_square_math.hpp"
#include "vector_class.hpp"

namespace sev::lag
{

// Transpose

template < typename FLT >
inline void
transpose ( Matrix4x4< FLT > & mat_n )
{
  std::swap ( mat_n[ 1 ], mat_n[ 4 ] );
  std::swap ( mat_n[ 2 ], mat_n[ 8 ] );
  std::swap ( mat_n[ 3 ], mat_n[ 12 ] );
  std::swap ( mat_n[ 6 ], mat_n[ 9 ] );
  std::swap ( mat_n[ 7 ], mat_n[ 13 ] );
  std::swap ( mat_n[ 11 ], mat_n[ 14 ] );
}

template < typename FLT >
inline Matrix4x4< FLT >
transposed ( Matrix4x4< FLT > const & msrc_n )
{
  Matrix4x4< FLT > mres;
  mres[ 0 ] = msrc_n[ 0 ]; // Diagonal
  mres[ 1 ] = msrc_n[ 4 ];
  mres[ 2 ] = msrc_n[ 8 ];
  mres[ 3 ] = msrc_n[ 12 ];

  mres[ 4 ] = msrc_n[ 1 ];
  mres[ 5 ] = msrc_n[ 5 ]; // Diagonal
  mres[ 6 ] = msrc_n[ 9 ];
  mres[ 7 ] = msrc_n[ 13 ];

  mres[ 8 ] = msrc_n[ 2 ];
  mres[ 9 ] = msrc_n[ 6 ];
  mres[ 10 ] = msrc_n[ 10 ]; // Diagonal
  mres[ 11 ] = msrc_n[ 14 ];

  mres[ 12 ] = msrc_n[ 3 ];
  mres[ 13 ] = msrc_n[ 7 ];
  mres[ 14 ] = msrc_n[ 11 ];
  mres[ 15 ] = msrc_n[ 15 ]; // Diagonal
  return mres;
}

// Determinant

template < typename FLT >
inline FLT
determinant ( Matrix4x4< FLT > const & mat_n )
{
  FLT res;
  FLT tmp1;
  FLT tmp2;

  // Eval around [0,0]
  tmp1 = mat_n[ 5 ] * mat_n[ 10 ] * mat_n[ 15 ];
  tmp1 += mat_n[ 6 ] * mat_n[ 11 ] * mat_n[ 13 ];
  tmp1 += mat_n[ 7 ] * mat_n[ 9 ] * mat_n[ 14 ];

  tmp2 = mat_n[ 13 ] * mat_n[ 10 ] * mat_n[ 7 ];
  tmp2 += mat_n[ 14 ] * mat_n[ 11 ] * mat_n[ 5 ];
  tmp2 += mat_n[ 15 ] * mat_n[ 9 ] * mat_n[ 6 ];

  res = mat_n[ 0 ] * ( tmp1 - tmp2 );

  // Eval around [1,0]
  tmp1 = mat_n[ 4 ] * mat_n[ 10 ] * mat_n[ 15 ];
  tmp1 += mat_n[ 6 ] * mat_n[ 11 ] * mat_n[ 12 ];
  tmp1 += mat_n[ 7 ] * mat_n[ 8 ] * mat_n[ 14 ];

  tmp2 = mat_n[ 12 ] * mat_n[ 10 ] * mat_n[ 7 ];
  tmp2 += mat_n[ 14 ] * mat_n[ 11 ] * mat_n[ 4 ];
  tmp2 += mat_n[ 15 ] * mat_n[ 8 ] * mat_n[ 6 ];

  res += mat_n[ 1 ] * ( tmp2 - tmp1 );

  // Eval around [2,0]
  tmp1 = mat_n[ 4 ] * mat_n[ 9 ] * mat_n[ 15 ];
  tmp1 += mat_n[ 5 ] * mat_n[ 11 ] * mat_n[ 12 ];
  tmp1 += mat_n[ 7 ] * mat_n[ 8 ] * mat_n[ 13 ];

  tmp2 = mat_n[ 12 ] * mat_n[ 9 ] * mat_n[ 7 ];
  tmp2 += mat_n[ 13 ] * mat_n[ 11 ] * mat_n[ 4 ];
  tmp2 += mat_n[ 15 ] * mat_n[ 8 ] * mat_n[ 5 ];

  res += mat_n[ 2 ] * ( tmp1 - tmp2 );

  // Eval around [3,0]
  tmp1 = mat_n[ 4 ] * mat_n[ 9 ] * mat_n[ 14 ];
  tmp1 += mat_n[ 5 ] * mat_n[ 10 ] * mat_n[ 12 ];
  tmp1 += mat_n[ 6 ] * mat_n[ 8 ] * mat_n[ 13 ];

  tmp2 = mat_n[ 12 ] * mat_n[ 9 ] * mat_n[ 6 ];
  tmp2 += mat_n[ 13 ] * mat_n[ 10 ] * mat_n[ 4 ];
  tmp2 += mat_n[ 14 ] * mat_n[ 8 ] * mat_n[ 5 ];

  res += mat_n[ 3 ] * ( tmp2 - tmp1 );

  return res;
}

// Inverse

template < typename FLT >
Matrix4x4< FLT >
inverse_det ( Matrix4x4< FLT > const & msrc_n, FLT det_n )
{
  Matrix4x4< FLT > mres;
  FLT tmp1;
  FLT tmp2;

  // Component 0
  tmp1 = msrc_n[ 5 ] * msrc_n[ 10 ] * msrc_n[ 15 ];
  tmp1 += msrc_n[ 6 ] * msrc_n[ 11 ] * msrc_n[ 13 ];
  tmp1 += msrc_n[ 7 ] * msrc_n[ 9 ] * msrc_n[ 14 ];

  tmp2 = msrc_n[ 13 ] * msrc_n[ 10 ] * msrc_n[ 7 ];
  tmp2 += msrc_n[ 14 ] * msrc_n[ 11 ] * msrc_n[ 5 ];
  tmp2 += msrc_n[ 15 ] * msrc_n[ 9 ] * msrc_n[ 6 ];

  mres[ 0 ] = tmp1 - tmp2;

  // Component 1
  tmp1 = msrc_n[ 1 ] * msrc_n[ 10 ] * msrc_n[ 15 ];
  tmp1 += msrc_n[ 2 ] * msrc_n[ 11 ] * msrc_n[ 13 ];
  tmp1 += msrc_n[ 3 ] * msrc_n[ 9 ] * msrc_n[ 14 ];

  tmp2 = msrc_n[ 13 ] * msrc_n[ 10 ] * msrc_n[ 3 ];
  tmp2 += msrc_n[ 14 ] * msrc_n[ 11 ] * msrc_n[ 1 ];
  tmp2 += msrc_n[ 15 ] * msrc_n[ 9 ] * msrc_n[ 2 ];

  mres[ 1 ] = tmp2 - tmp1;

  // Component 2
  tmp1 = msrc_n[ 1 ] * msrc_n[ 6 ] * msrc_n[ 15 ];
  tmp1 += msrc_n[ 2 ] * msrc_n[ 7 ] * msrc_n[ 13 ];
  tmp1 += msrc_n[ 3 ] * msrc_n[ 5 ] * msrc_n[ 14 ];

  tmp2 = msrc_n[ 13 ] * msrc_n[ 6 ] * msrc_n[ 3 ];
  tmp2 += msrc_n[ 14 ] * msrc_n[ 7 ] * msrc_n[ 1 ];
  tmp2 += msrc_n[ 15 ] * msrc_n[ 5 ] * msrc_n[ 2 ];

  mres[ 2 ] = tmp1 - tmp2;

  // Component 3
  tmp1 = msrc_n[ 1 ] * msrc_n[ 6 ] * msrc_n[ 11 ];
  tmp1 += msrc_n[ 2 ] * msrc_n[ 7 ] * msrc_n[ 9 ];
  tmp1 += msrc_n[ 3 ] * msrc_n[ 5 ] * msrc_n[ 10 ];

  tmp2 = msrc_n[ 9 ] * msrc_n[ 6 ] * msrc_n[ 3 ];
  tmp2 += msrc_n[ 10 ] * msrc_n[ 7 ] * msrc_n[ 1 ];
  tmp2 += msrc_n[ 11 ] * msrc_n[ 5 ] * msrc_n[ 2 ];

  mres[ 3 ] = tmp2 - tmp1;

  // Component 4
  tmp1 = msrc_n[ 4 ] * msrc_n[ 10 ] * msrc_n[ 15 ];
  tmp1 += msrc_n[ 6 ] * msrc_n[ 11 ] * msrc_n[ 12 ];
  tmp1 += msrc_n[ 7 ] * msrc_n[ 8 ] * msrc_n[ 14 ];

  tmp2 = msrc_n[ 12 ] * msrc_n[ 10 ] * msrc_n[ 7 ];
  tmp2 += msrc_n[ 14 ] * msrc_n[ 11 ] * msrc_n[ 4 ];
  tmp2 += msrc_n[ 15 ] * msrc_n[ 8 ] * msrc_n[ 6 ];

  mres[ 4 ] = tmp2 - tmp1;

  // Component 5
  tmp1 = msrc_n[ 0 ] * msrc_n[ 10 ] * msrc_n[ 15 ];
  tmp1 += msrc_n[ 2 ] * msrc_n[ 11 ] * msrc_n[ 12 ];
  tmp1 += msrc_n[ 3 ] * msrc_n[ 8 ] * msrc_n[ 14 ];

  tmp2 = msrc_n[ 12 ] * msrc_n[ 10 ] * msrc_n[ 3 ];
  tmp2 += msrc_n[ 14 ] * msrc_n[ 11 ] * msrc_n[ 0 ];
  tmp2 += msrc_n[ 15 ] * msrc_n[ 8 ] * msrc_n[ 2 ];

  mres[ 5 ] = tmp1 - tmp2;

  // Component 6
  tmp1 = msrc_n[ 0 ] * msrc_n[ 6 ] * msrc_n[ 15 ];
  tmp1 += msrc_n[ 2 ] * msrc_n[ 7 ] * msrc_n[ 12 ];
  tmp1 += msrc_n[ 3 ] * msrc_n[ 4 ] * msrc_n[ 14 ];

  tmp2 = msrc_n[ 12 ] * msrc_n[ 6 ] * msrc_n[ 3 ];
  tmp2 += msrc_n[ 14 ] * msrc_n[ 7 ] * msrc_n[ 0 ];
  tmp2 += msrc_n[ 15 ] * msrc_n[ 4 ] * msrc_n[ 2 ];

  mres[ 6 ] = tmp2 - tmp1;

  // Component 7
  tmp1 = msrc_n[ 0 ] * msrc_n[ 6 ] * msrc_n[ 11 ];
  tmp1 += msrc_n[ 2 ] * msrc_n[ 7 ] * msrc_n[ 8 ];
  tmp1 += msrc_n[ 3 ] * msrc_n[ 4 ] * msrc_n[ 10 ];

  tmp2 = msrc_n[ 8 ] * msrc_n[ 6 ] * msrc_n[ 3 ];
  tmp2 += msrc_n[ 10 ] * msrc_n[ 7 ] * msrc_n[ 0 ];
  tmp2 += msrc_n[ 11 ] * msrc_n[ 4 ] * msrc_n[ 2 ];

  mres[ 7 ] = tmp1 - tmp2;

  // Component 8
  tmp1 = msrc_n[ 4 ] * msrc_n[ 9 ] * msrc_n[ 15 ];
  tmp1 += msrc_n[ 5 ] * msrc_n[ 11 ] * msrc_n[ 12 ];
  tmp1 += msrc_n[ 7 ] * msrc_n[ 8 ] * msrc_n[ 13 ];

  tmp2 = msrc_n[ 12 ] * msrc_n[ 9 ] * msrc_n[ 7 ];
  tmp2 += msrc_n[ 13 ] * msrc_n[ 11 ] * msrc_n[ 4 ];
  tmp2 += msrc_n[ 15 ] * msrc_n[ 8 ] * msrc_n[ 5 ];

  mres[ 8 ] = tmp1 - tmp2;

  // Component 9
  tmp1 = msrc_n[ 0 ] * msrc_n[ 9 ] * msrc_n[ 15 ];
  tmp1 += msrc_n[ 1 ] * msrc_n[ 11 ] * msrc_n[ 12 ];
  tmp1 += msrc_n[ 3 ] * msrc_n[ 8 ] * msrc_n[ 13 ];

  tmp2 = msrc_n[ 12 ] * msrc_n[ 9 ] * msrc_n[ 3 ];
  tmp2 += msrc_n[ 13 ] * msrc_n[ 11 ] * msrc_n[ 0 ];
  tmp2 += msrc_n[ 15 ] * msrc_n[ 8 ] * msrc_n[ 1 ];

  mres[ 9 ] = tmp2 - tmp1;

  // Component 10
  tmp1 = msrc_n[ 0 ] * msrc_n[ 5 ] * msrc_n[ 15 ];
  tmp1 += msrc_n[ 1 ] * msrc_n[ 7 ] * msrc_n[ 12 ];
  tmp1 += msrc_n[ 3 ] * msrc_n[ 4 ] * msrc_n[ 13 ];

  tmp2 = msrc_n[ 12 ] * msrc_n[ 5 ] * msrc_n[ 3 ];
  tmp2 += msrc_n[ 13 ] * msrc_n[ 7 ] * msrc_n[ 0 ];
  tmp2 += msrc_n[ 15 ] * msrc_n[ 4 ] * msrc_n[ 1 ];

  mres[ 10 ] = tmp1 - tmp2;

  // Component 11
  tmp1 = msrc_n[ 0 ] * msrc_n[ 5 ] * msrc_n[ 11 ];
  tmp1 += msrc_n[ 1 ] * msrc_n[ 7 ] * msrc_n[ 8 ];
  tmp1 += msrc_n[ 3 ] * msrc_n[ 4 ] * msrc_n[ 9 ];

  tmp2 = msrc_n[ 8 ] * msrc_n[ 5 ] * msrc_n[ 3 ];
  tmp2 += msrc_n[ 9 ] * msrc_n[ 7 ] * msrc_n[ 0 ];
  tmp2 += msrc_n[ 11 ] * msrc_n[ 4 ] * msrc_n[ 1 ];

  mres[ 11 ] = tmp2 - tmp1;

  // Component 12
  tmp1 = msrc_n[ 4 ] * msrc_n[ 9 ] * msrc_n[ 14 ];
  tmp1 += msrc_n[ 5 ] * msrc_n[ 10 ] * msrc_n[ 12 ];
  tmp1 += msrc_n[ 6 ] * msrc_n[ 8 ] * msrc_n[ 13 ];

  tmp2 = msrc_n[ 12 ] * msrc_n[ 9 ] * msrc_n[ 6 ];
  tmp2 += msrc_n[ 13 ] * msrc_n[ 10 ] * msrc_n[ 4 ];
  tmp2 += msrc_n[ 14 ] * msrc_n[ 8 ] * msrc_n[ 5 ];

  mres[ 12 ] = tmp2 - tmp1;

  // Component 13
  tmp1 = msrc_n[ 0 ] * msrc_n[ 9 ] * msrc_n[ 14 ];
  tmp1 += msrc_n[ 1 ] * msrc_n[ 10 ] * msrc_n[ 12 ];
  tmp1 += msrc_n[ 2 ] * msrc_n[ 8 ] * msrc_n[ 13 ];

  tmp2 = msrc_n[ 12 ] * msrc_n[ 9 ] * msrc_n[ 2 ];
  tmp2 += msrc_n[ 13 ] * msrc_n[ 10 ] * msrc_n[ 0 ];
  tmp2 += msrc_n[ 14 ] * msrc_n[ 8 ] * msrc_n[ 1 ];

  mres[ 13 ] = tmp1 - tmp2;

  // Component 14
  tmp1 = msrc_n[ 0 ] * msrc_n[ 5 ] * msrc_n[ 14 ];
  tmp1 += msrc_n[ 1 ] * msrc_n[ 6 ] * msrc_n[ 12 ];
  tmp1 += msrc_n[ 2 ] * msrc_n[ 4 ] * msrc_n[ 13 ];

  tmp2 = msrc_n[ 12 ] * msrc_n[ 5 ] * msrc_n[ 2 ];
  tmp2 += msrc_n[ 13 ] * msrc_n[ 6 ] * msrc_n[ 0 ];
  tmp2 += msrc_n[ 14 ] * msrc_n[ 4 ] * msrc_n[ 1 ];

  mres[ 14 ] = tmp2 - tmp1;

  // Component 15
  tmp1 = msrc_n[ 0 ] * msrc_n[ 5 ] * msrc_n[ 10 ];
  tmp1 += msrc_n[ 1 ] * msrc_n[ 6 ] * msrc_n[ 8 ];
  tmp1 += msrc_n[ 2 ] * msrc_n[ 4 ] * msrc_n[ 9 ];

  tmp2 = msrc_n[ 8 ] * msrc_n[ 5 ] * msrc_n[ 2 ];
  tmp2 += msrc_n[ 9 ] * msrc_n[ 6 ] * msrc_n[ 0 ];
  tmp2 += msrc_n[ 10 ] * msrc_n[ 4 ] * msrc_n[ 1 ];

  mres[ 15 ] = tmp1 - tmp2;

  mres /= det_n;

  return mres;
}

template < typename FLT >
inline void
invert_det ( Matrix4x4< FLT > & mat_n, FLT det_n )
{
  mat_n = inverse_det ( mat_n, det_n );
}

template < typename FLT >
bool
invert ( Matrix4x4< FLT > & mat_n )
{
  FLT const det ( determinant ( mat_n ) );
  if ( det != 0.0 ) {
    invert_det ( mat_n, det );
    return true;
  }
  return false;
}

template < typename FLT >
bool
inverse ( Matrix4x4< FLT > & mres_n, Matrix4x4< FLT > const & msrc_n )
{
  FLT const det ( determinant ( msrc_n ) );
  if ( det != 0.0 ) {
    inverse_det ( mres_n, msrc_n, det );
    return true;
  }
  return false;
}

template < typename FLT >
bool
solve_rf ( Vector4< FLT > & vres_n,
           Matrix4x4< FLT > const & msrc_n,
           const Vector4< FLT > & vsrc_n )
{
  Matrix4x4< FLT > mat_i;
  if ( inverse ( mat_i, msrc_n ) ) {
    mult_rf ( vres_n, mat_i, vsrc_n );
    return true;
  }
  return false;
}

template < typename FLT >
bool
solve_cf ( Vector4< FLT > & vres_n,
           Matrix4x4< FLT > const & msrc_n,
           const Vector4< FLT > & vsrc_n )
{
  Matrix4x4< FLT > mat_i;
  if ( inverse ( mat_i, msrc_n ) ) {
    mult_cf ( vres_n, mat_i, vsrc_n );
    return true;
  }
  return false;
}

} // namespace sev::lag
