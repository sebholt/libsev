/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/lag/init.hpp>
#include <array>
#include <cstdint>
#include <iterator>

namespace sev::lag
{

/// @brief Quaternion class (template)
///
template < typename FLT >
class Quat
{
  public:
  // -- Types
  static constexpr std::size_t dim = 4;
  using value_type = FLT;
  using iterator = value_type *;
  using const_iterator = value_type const *;
  using reverse_iterator = std::reverse_iterator< iterator >;
  using const_reverse_iterator = std::reverse_iterator< const_iterator >;
  using value_array = value_type[ dim ];
  using std_array = std::array< value_type, dim >;
  using quat = Quat< value_type >;

  // -- Constructors

  /// @brief Komponents are value (zero) initialized
  constexpr Quat () = default;

  constexpr Quat ( quat const & ) = default;

  constexpr Quat ( quat && ) = default;

  constexpr Quat ( const value_array & array_n ) { assign ( array_n ); }

  constexpr Quat ( const std_array & array_n ) { assign ( array_n ); }

  constexpr Quat ( value_type v0_n,
                   value_type v1_n,
                   value_type v2_n,
                   value_type v3_n )
  : _xx{ v0_n, v1_n, v2_n, v3_n }
  {
  }

  constexpr explicit Quat ( const init::Identity & init_n [[maybe_unused]] )
  {
    set_identity ();
  }

  constexpr explicit Quat ( const init::Zero & init_n [[maybe_unused]] )
  {
    fill ( 0.0 );
  }

  constexpr explicit Quat ( const init::One & init_n [[maybe_unused]] )
  {
    fill ( 1.0 );
  }

  constexpr explicit Quat ( init::Fill< value_type > init_n )
  {
    fill ( init_n.value );
  }

  // -- Assignment

  constexpr void
  set_identity ()
  {
    constexpr value_type one = 1.0;
    constexpr value_type zero = 0.0;
    _xx[ 0 ] = one;
    _xx[ 1 ] = zero;
    _xx[ 2 ] = zero;
    _xx[ 3 ] = zero;
  }

  constexpr void
  fill ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] = value_n;
    }
  }

  constexpr void
  assign ( const value_array & array_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] = array_n[ ii ];
    }
  }

  constexpr void
  assign ( const std_array & array_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] = array_n[ ii ];
    }
  }

  constexpr void
  assign ( const quat & quat_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] = quat_n[ ii ];
    }
  }

  constexpr quat &
  operator= ( value_array const & array_n )
  {
    assign ( array_n );
    return *this;
  }

  constexpr quat &
  operator= ( const std_array & array_n )
  {
    assign ( array_n );
    return *this;
  }

  constexpr quat &
  operator= ( quat const & ) = default;

  constexpr quat &
  operator= ( quat && ) = default;

  // -- Meta information

  /// @brief Number of elements
  static constexpr std::size_t
  size ()
  {
    return dim;
  }

  // -- Component accessors

  constexpr value_type const &
  x ( std::size_t index_n ) const
  {
    return _xx[ index_n ];
  }

  constexpr value_type &
  xr ( std::size_t index_n )
  {
    return _xx[ index_n ];
  }

  constexpr void
  set ( std::size_t index_n, value_type value_n )
  {
    _xx[ index_n ] = value_n;
  }

  constexpr void
  set ( value_type v0_n, value_type v1_n, value_type v2_n, value_type v3_n )
  {
    _xx[ 0 ] = v0_n;
    _xx[ 1 ] = v1_n;
    _xx[ 2 ] = v2_n;
    _xx[ 3 ] = v3_n;
  }

  // -- Iteration

  constexpr iterator
  begin ()
  {
    return ( &_xx[ 0 ] );
  }

  constexpr iterator
  end ()
  {
    return ( &_xx[ dim ] );
  }

  constexpr const_iterator
  begin () const
  {
    return ( &_xx[ 0 ] );
  }

  constexpr const_iterator
  end () const
  {
    return ( &_xx[ dim ] );
  }

  constexpr const_iterator
  cbegin () const
  {
    return ( &_xx[ 0 ] );
  }

  constexpr const_iterator
  cend () const
  {
    return ( &_xx[ dim ] );
  }

  // -- Component access operators

  constexpr value_type const &
  operator[] ( std::size_t index_n ) const
  {
    return _xx[ index_n ];
  }

  constexpr value_type &
  operator[] ( std::size_t index_n )
  {
    return _xx[ index_n ];
  }

  // -- Scalar arithmetics operators

  constexpr quat &
  operator+= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] += value_n;
    }
    return *this;
  }

  constexpr quat &
  operator-= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] -= value_n;
    }
    return *this;
  }

  constexpr quat &
  operator*= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] *= value_n;
    }
    return *this;
  }

  constexpr quat &
  operator/= ( value_type value_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] /= value_n;
    }
    return *this;
  }

  // -- Quaternion arithmetics operators

  constexpr quat &
  operator+= ( quat const & quaternion_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] += quaternion_n._xx[ ii ];
    }
    return *this;
  }

  constexpr quat &
  operator-= ( quat const & quaternion_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] -= quaternion_n._xx[ ii ];
    }
    return *this;
  }

  constexpr quat &
  operator*= ( quat const & quaternion_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] *= quaternion_n._xx[ ii ];
    }
    return *this;
  }

  constexpr quat &
  operator/= ( quat const & quaternion_n )
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      _xx[ ii ] /= quaternion_n._xx[ ii ];
    }
    return *this;
  }

  // -- Comparison operators

  constexpr bool
  operator== ( quat const & quaternion_n ) const
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      if ( _xx[ ii ] != quaternion_n._xx[ ii ] ) {
        return false;
      }
    }
    return true;
  }

  constexpr bool
  operator!= ( quat const & quaternion_n ) const
  {
    for ( std::size_t ii = 0; ii != dim; ++ii ) {
      if ( _xx[ ii ] != quaternion_n._xx[ ii ] ) {
        return true;
      }
    }
    return false;
  }

  private:
  value_array _xx = {};
};

// -- External Operators - Quaternion * Quaternion

template < typename FLT >
inline constexpr Quat< FLT > &
operator+ ( Quat< FLT > & quaternion_n )
{
  return quaternion_n;
}

template < typename FLT >
inline constexpr Quat< FLT > const &
operator+ ( Quat< FLT > const & quaternion_n )
{
  return quaternion_n;
}

template < typename FLT >
inline constexpr Quat< FLT >
operator- ( Quat< FLT > const & quaternion_n )
{
  Quat< FLT > res;
  for ( std::size_t ii = 0; ii != Quat< FLT >::dim; ++ii ) {
    res._xx[ ii ] = -quaternion_n._xx[ ii ];
  }
  return res;
}

template < typename FLT >
inline constexpr Quat< FLT >
operator+ ( Quat< FLT > const & v_0, Quat< FLT > const & v_1 )
{
  Quat< FLT > res;
  for ( std::size_t ii = 0; ii != Quat< FLT >::dim; ++ii ) {
    res._xx[ ii ] = ( v_0._xx[ ii ] + v_1._xx[ ii ] );
  }
  return res;
}

template < typename FLT >
inline constexpr Quat< FLT >
operator- ( Quat< FLT > const & v_0, Quat< FLT > const & v_1 )
{
  Quat< FLT > res;
  for ( std::size_t ii = 0; ii != Quat< FLT >::dim; ++ii ) {
    res._xx[ ii ] = ( v_0._xx[ ii ] - v_1._xx[ ii ] );
  }
  return res;
}

template < typename FLT >
inline constexpr Quat< FLT >
operator* ( Quat< FLT > const & v_0, Quat< FLT > const & v_1 )
{
  Quat< FLT > res;
  for ( std::size_t ii = 0; ii != Quat< FLT >::dim; ++ii ) {
    res._xx[ ii ] = ( v_0._xx[ ii ] * v_1._xx[ ii ] );
  }
  return res;
}

template < typename FLT >
inline constexpr Quat< FLT >
operator/ ( Quat< FLT > const & v_0, Quat< FLT > const & v_1 )
{
  Quat< FLT > res;
  for ( std::size_t ii = 0; ii != Quat< FLT >::dim; ++ii ) {
    res._xx[ ii ] = ( v_0._xx[ ii ] / v_1._xx[ ii ] );
  }
  return res;
}

// -- External Operators - Quaternion * Scalar

template < typename FLT >
inline constexpr Quat< FLT >
operator+ ( Quat< FLT > const & v_0, FLT value_n )
{
  Quat< FLT > res;
  for ( std::size_t ii = 0; ii != Quat< FLT >::dim; ++ii ) {
    res[ ii ] = v_0[ ii ] + value_n;
  }
  return res;
}

template < typename FLT >
inline constexpr Quat< FLT >
operator- ( Quat< FLT > const & v_0, FLT value_n )
{
  Quat< FLT > res;
  for ( std::size_t ii = 0; ii != Quat< FLT >::dim; ++ii ) {
    res[ ii ] = v_0[ ii ] - value_n;
  }
  return res;
}

template < typename FLT >
inline constexpr Quat< FLT >
operator* ( Quat< FLT > const & v_0, FLT value_n )
{
  Quat< FLT > res;
  for ( std::size_t ii = 0; ii != Quat< FLT >::dim; ++ii ) {
    res[ ii ] = v_0[ ii ] * value_n;
  }
  return res;
}

template < typename FLT >
inline constexpr Quat< FLT >
operator/ ( Quat< FLT > const & v_0, FLT value_n )
{
  Quat< FLT > res;
  for ( std::size_t ii = 0; ii != Quat< FLT >::dim; ++ii ) {
    res[ ii ] = v_0[ ii ] / value_n;
  }
  return res;
}

// -- Types

using Quatf = Quat< float >;
using Quatd = Quat< double >;

} // namespace sev::lag
