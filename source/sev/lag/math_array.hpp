/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cmath>
#include <cstdint>

namespace sev::lag
{

// Comparison

template < typename FLT >
inline bool
array_match ( FLT const * vals0_n,
              FLT const * vals1_n,
              std::size_t num_n,
              FLT epsilon_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    if ( std::abs ( vals1_n[ ii ] - vals0_n[ ii ] ) > epsilon_n ) {
      return false;
    }
  }
  return true;
}

template < typename FLT >
inline bool
array_match ( FLT const * vals_n,
              FLT value_n,
              std::size_t num_n,
              FLT epsilon_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    if ( std::abs ( vals_n[ ii ] - value_n ) > epsilon_n ) {
      return false;
    }
  }
  return true;
}

template < typename FLT >
inline bool
array_almost_zero ( FLT const * vals_n, std::size_t num_n, FLT epsilon_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    if ( std::abs ( vals_n[ ii ] ) > epsilon_n ) {
      return false;
    }
  }
  return true;
}

template < typename FLT >
inline void
array_adjust_almost_zero ( FLT * data_n, std::size_t num_n, FLT epsilon_n )
{
  FLT const zero ( 0.0 );
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    if ( std::abs ( data_n[ ii ] ) < epsilon_n ) {
      data_n[ ii ] = zero;
    }
  }
}

// Setting

template < typename FLT >
inline void
array_fill ( FLT * vals_n, std::size_t num_n, FLT value_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vals_n[ ii ] = value_n;
  }
}

template < typename FLT >
inline void
array_set_x ( FLT * vals_n,
              std::size_t num_n,
              std::size_t x_n,
              std::size_t delta_n,
              FLT value_n )
{
  FLT * vp ( vals_n + x_n );
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    *vp = value_n;
    vp += delta_n;
  }
}

// Addition

template < typename FLT >
inline void
array_add ( FLT * vals_n, std::size_t num_n, FLT value_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vals_n[ ii ] += value_n;
  }
}

template < typename FLT >
inline void
array_add ( FLT * vals_n, FLT const * va_n, std::size_t num_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vals_n[ ii ] += va_n[ ii ];
  }
}

template < typename FLT >
inline void
array_added ( FLT * res_n, FLT const * va_n, std::size_t num_n, FLT value_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    res_n[ ii ] = va_n[ ii ] + value_n;
  }
}

template < typename FLT >
inline void
array_added ( FLT * res_n,
              FLT const * va_n,
              FLT const * vb_n,
              std::size_t num_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    res_n[ ii ] = va_n[ ii ] + vb_n[ ii ];
  }
}

// Subtraction

template < typename FLT >
inline void
array_sub ( FLT * vals_n, std::size_t num_n, FLT value_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vals_n[ ii ] -= value_n;
  }
}

template < typename FLT >
inline void
array_sub ( FLT * vals_n, FLT const * va_n, std::size_t num_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vals_n[ ii ] -= va_n[ ii ];
  }
}

template < typename FLT >
inline void
array_subed ( FLT * res_n, FLT const * va_n, std::size_t num_n, FLT value_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    res_n[ ii ] = va_n[ ii ] - value_n;
  }
}

template < typename FLT >
inline void
array_subed ( FLT * res_n,
              FLT const * va_n,
              FLT const * vb_n,
              std::size_t num_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    res_n[ ii ] = va_n[ ii ] - vb_n[ ii ];
  }
}

// Multiplication

template < typename FLT >
inline void
array_mult ( FLT * vals_n, std::size_t num_n, FLT value_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vals_n[ ii ] *= value_n;
  }
}

template < typename FLT >
inline void
array_mult ( FLT * vals_n, FLT const * va_n, std::size_t num_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vals_n[ ii ] *= va_n[ ii ];
  }
}

template < typename FLT >
inline void
array_multed ( FLT * res_n, FLT const * va_n, std::size_t num_n, FLT value_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    res_n[ ii ] = va_n[ ii ] * value_n;
  }
}

template < typename FLT >
inline void
array_multed ( FLT * res_n,
               FLT const * va_n,
               FLT const * vb_n,
               std::size_t num_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    res_n[ ii ] = va_n[ ii ] * vb_n[ ii ];
  }
}

// Division

template < typename FLT >
inline void
array_div ( FLT * vals_n, std::size_t num_n, FLT value_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vals_n[ ii ] /= value_n;
  }
}

template < typename FLT >
inline void
array_div ( FLT * vals_n, FLT const * va_n, std::size_t num_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vals_n[ ii ] /= va_n[ ii ];
  }
}

template < typename FLT >
inline void
array_dived ( FLT * res_n, FLT const * va_n, std::size_t num_n, FLT value_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    res_n[ ii ] = va_n[ ii ] / value_n;
  }
}

template < typename FLT >
inline void
array_dived ( FLT * res_n,
              FLT const * va_n,
              FLT const * vb_n,
              std::size_t num_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    res_n[ ii ] = va_n[ ii ] / vb_n[ ii ];
  }
}

} // namespace sev::lag
