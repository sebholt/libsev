/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "math_array.hpp"
#include "vector_class.hpp"
#include <algorithm>
#include <cstdint>

namespace sev::lag
{

// Setting

template < typename FLT, std::size_t DIM >
inline void
array_fill ( Vector< FLT, DIM > * va_n,
             std::size_t num_n,
             Vector< FLT, DIM > const & vector_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    va_n[ ii ] = vector_n;
  }
}

template < typename FLT, std::size_t DIM >
inline void
array_fill ( Vector< FLT, DIM > * va_n, std::size_t num_n, FLT value_n )
{
  array_fill ( va_n[ 0 ].begin (), num_n * DIM, value_n );
}

template < typename FLT, std::size_t DIM >
inline void
array_set_x ( Vector< FLT, DIM > * va_n,
              std::size_t num_n,
              std::size_t x_n,
              FLT value_n )
{
  array_set_x ( va_n[ 0 ].begin (), num_n, x_n, DIM, value_n );
}

// Copying

template < typename FLT, std::size_t DIM >
inline void
array_copy ( Vector< FLT, DIM > * vres_n,
             Vector< FLT, DIM > const * vaa_n,
             std::size_t num_n )
{
  std::copy ( vaa_n[ 0 ].begin (),
              vaa_n[ 0 ].begin () + num_n * DIM,
              vres_n[ 0 ].begin () );
}

// Addition

template < typename FLT, std::size_t DIM >
inline void
array_add ( Vector< FLT, DIM > * va_n,
            std::size_t num_n,
            Vector< FLT, DIM > const & vector_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    va_n[ ii ] += vector_n;
  }
}

template < typename FLT, std::size_t DIM >
inline void
array_add ( Vector< FLT, DIM > * va_n, std::size_t num_n, FLT value_n )
{
  array_add ( va_n[ 0 ].begin (), num_n * DIM, value_n );
}

template < typename FLT, std::size_t DIM >
inline void
array_add ( Vector< FLT, DIM > * va_n,
            Vector< FLT, DIM > const * vaa_n,
            std::size_t num_n )
{
  array_add ( va_n[ 0 ].begin (), vaa_n[ 0 ].begin (), num_n * DIM );
}

template < typename FLT, std::size_t DIM >
inline void
array_added ( Vector< FLT, DIM > * vres_n,
              Vector< FLT, DIM > const * vaa_n,
              std::size_t num_n,
              Vector< FLT, DIM > const & vector_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vres_n[ ii ] = vaa_n[ ii ] + vector_n;
  }
}

template < typename FLT, std::size_t DIM >
inline void
array_added ( Vector< FLT, DIM > * vres_n,
              Vector< FLT, DIM > const * vaa_n,
              std::size_t num_n,
              FLT value_n )
{
  array_added (
      vres_n[ 0 ].begin (), vaa_n[ 0 ].begin (), num_n * DIM, value_n );
}

template < typename FLT, std::size_t DIM >
inline void
array_added ( Vector< FLT, DIM > * vres_n,
              Vector< FLT, DIM > const * vaa_n,
              Vector< FLT, DIM > const * vab_n,
              std::size_t num_n )
{
  array_added ( vres_n[ 0 ].begin (),
                vaa_n[ 0 ].begin (),
                vab_n[ 0 ].begin (),
                num_n * DIM );
}

// Subtraction

template < typename FLT, std::size_t DIM >
inline void
array_sub ( Vector< FLT, DIM > * va_n,
            std::size_t num_n,
            Vector< FLT, DIM > const & vector_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    va_n[ ii ] -= vector_n;
  }
}

template < typename FLT, std::size_t DIM >
inline void
array_sub ( Vector< FLT, DIM > * va_n, std::size_t num_n, FLT value_n )
{
  array_sub ( va_n[ 0 ].begin (), num_n * DIM, value_n );
}

template < typename FLT, std::size_t DIM >
inline void
array_sub ( Vector< FLT, DIM > * va_n,
            Vector< FLT, DIM > const * vaa_n,
            std::size_t num_n )
{
  array_sub ( va_n[ 0 ].begin (), vaa_n[ 0 ].begin (), num_n * DIM );
}

template < typename FLT, std::size_t DIM >
inline void
array_subed ( Vector< FLT, DIM > * vres_n,
              Vector< FLT, DIM > const * vaa_n,
              std::size_t num_n,
              Vector< FLT, DIM > const & vector_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vres_n[ ii ] = vaa_n[ ii ] - vector_n;
  }
}

template < typename FLT, std::size_t DIM >
inline void
array_subed ( Vector< FLT, DIM > * vres_n,
              Vector< FLT, DIM > const * vaa_n,
              std::size_t num_n,
              FLT value_n )
{
  array_subed (
      vres_n[ 0 ].begin (), vaa_n[ 0 ].begin (), num_n * DIM, value_n );
}

template < typename FLT, std::size_t DIM >
inline void
array_subed ( Vector< FLT, DIM > * vres_n,
              Vector< FLT, DIM > const * vaa_n,
              Vector< FLT, DIM > const * vab_n,
              std::size_t num_n )
{
  array_subed ( vres_n[ 0 ].begin (),
                vaa_n[ 0 ].begin (),
                vab_n[ 0 ].begin (),
                num_n * DIM );
}

// Multiplication

template < typename FLT, std::size_t DIM >
inline void
array_mult ( Vector< FLT, DIM > * va_n,
             std::size_t num_n,
             Vector< FLT, DIM > const & vector_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    va_n[ ii ] *= vector_n;
  }
}

template < typename FLT, std::size_t DIM >
inline void
array_mult ( Vector< FLT, DIM > * va_n, std::size_t num_n, FLT value_n )
{
  array_mult ( va_n[ 0 ].begin (), num_n * DIM, value_n );
}

template < typename FLT, std::size_t DIM >
inline void
array_mult ( Vector< FLT, DIM > * va_n,
             Vector< FLT, DIM > const * vaa_n,
             std::size_t num_n )
{
  array_mult ( va_n[ 0 ].begin (), vaa_n[ 0 ].begin (), num_n * DIM );
}

template < typename FLT, std::size_t DIM >
inline void
array_multed ( Vector< FLT, DIM > * vres_n,
               Vector< FLT, DIM > const * vaa_n,
               std::size_t num_n,
               Vector< FLT, DIM > const & vector_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vres_n[ ii ] = vaa_n[ ii ] * vector_n;
  }
}

template < typename FLT, std::size_t DIM >
inline void
array_multed ( Vector< FLT, DIM > * vres_n,
               Vector< FLT, DIM > const * vaa_n,
               std::size_t num_n,
               FLT value_n )
{
  array_multed (
      vres_n[ 0 ].begin (), vaa_n[ 0 ].begin (), num_n * DIM, value_n );
}

template < typename FLT, std::size_t DIM >
inline void
array_multed ( Vector< FLT, DIM > * vres_n,
               Vector< FLT, DIM > const * vaa_n,
               Vector< FLT, DIM > const * vab_n,
               std::size_t num_n )
{
  array_multed ( vres_n[ 0 ].begin (),
                 vaa_n[ 0 ].begin (),
                 vab_n[ 0 ].begin (),
                 num_n * DIM );
}

// Division

template < typename FLT, std::size_t DIM >
inline void
array_div ( Vector< FLT, DIM > * va_n,
            std::size_t num_n,
            Vector< FLT, DIM > const & vector_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    va_n[ ii ] /= vector_n;
  }
}

template < typename FLT, std::size_t DIM >
inline void
array_div ( Vector< FLT, DIM > * va_n, std::size_t num_n, FLT value_n )
{
  array_div ( va_n[ 0 ].begin (), num_n * DIM, value_n );
}

template < typename FLT, std::size_t DIM >
inline void
array_div ( Vector< FLT, DIM > * va_n,
            Vector< FLT, DIM > const * vaa_n,
            std::size_t num_n )
{
  array_div ( va_n[ 0 ].begin (), vaa_n[ 0 ].begin (), num_n * DIM );
}

template < typename FLT, std::size_t DIM >
inline void
array_dived ( Vector< FLT, DIM > * vres_n,
              Vector< FLT, DIM > const * vaa_n,
              std::size_t num_n,
              Vector< FLT, DIM > const & vector_n )
{
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    vres_n[ ii ] = vaa_n[ ii ] / vector_n;
  }
}

template < typename FLT, std::size_t DIM >
inline void
array_dived ( Vector< FLT, DIM > * vres_n,
              Vector< FLT, DIM > const * vaa_n,
              std::size_t num_n,
              FLT value_n )
{
  array_dived (
      vres_n[ 0 ].begin (), vaa_n[ 0 ].begin (), num_n * DIM, value_n );
}

template < typename FLT, std::size_t DIM >
inline void
array_dived ( Vector< FLT, DIM > * vres_n,
              Vector< FLT, DIM > const * vaa_n,
              Vector< FLT, DIM > const * vab_n,
              std::size_t num_n )
{
  array_dived ( vres_n[ 0 ].begin (),
                vaa_n[ 0 ].begin (),
                vab_n[ 0 ].begin (),
                num_n * DIM );
}

} // namespace sev::lag
