/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include "matrix4x4_math.hpp"
#include "matrix_square.hpp"
