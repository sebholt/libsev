/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/unicode/view.hpp>
#include <cstdint>
#include <string>
#include <string_view>

namespace sev::unicode
{
// -- Forward declaration
class Span;

// -- String conversion count functions

/// Assumes that invalid characters get replaced
std::size_t
count_utf16_as_utf8 ( std::u16string_view view_n );

/// Assumes that invalid characters get replaced
std::size_t
count_utf32_as_utf8 ( std::u32string_view view_n );

/// Assumes that invalid characters get replaced
std::size_t
count_utf8_as_utf16 ( std::string_view view_n );

/// Assumes that invalid characters get replaced
std::size_t
count_utf32_as_utf16 ( std::u32string_view view_n );

/// Assumes that invalid characters get replaced
std::size_t
count_utf8_as_utf32 ( std::string_view view_n );

/// Assumes that invalid characters get replaced
std::size_t
count_utf16_as_utf32 ( std::u16string_view view_n );

// -- String conversion functions

/// The size of the write buffer must have been calculated with utf8_count().
/// Invalid characters get replaced.
void
convert_utf16_to_utf8_counted ( char * begin_n, std::u16string_view view_n );

/// The size of the write buffer must have been calculated with utf8_count().
/// Invalid characters get replaced.
void
convert_utf32_to_utf8_counted ( char * begin_n, std::u32string_view view_n );

/// The size of the write buffer must have been calculated with utf16_count().
/// Invalid characters get replaced.
void
convert_utf8_to_utf16_counted ( char16_t * begin_n, std::string_view view_n );

/// The size of the write buffer must have been calculated with utf16_count().
/// Invalid characters get replaced.
void
convert_utf32_to_utf16_counted ( char16_t * begin_n,
                                 std::u32string_view view_n );

/// The size of the write buffer must have been calculated with utf32_count().
/// Invalid characters get replaced.
void
convert_utf8_to_utf32_counted ( char32_t * begin_n, std::string_view view_n );

/// The size of the write buffer must have been calculated with utf32_count().
/// Invalid characters get replaced.
void
convert_utf16_to_utf32_counted ( char32_t * begin_n,
                                 std::u16string_view view_n );

// -- String conversion to UTF-8

void
convert ( std::string & res_n, std::u16string_view view_n );

void
convert ( std::string & res_n, const std::u16string & src_n );

void
convert ( std::string & res_n, std::u32string_view view_n );

void
convert ( std::string & res_n, const std::u32string & src_n );

// -- String conversion to UTF-16

void
convert ( std::u16string & res_n, std::string_view view_n );

void
convert ( std::u16string & res_n, const std::string & src_n );

void
convert ( std::u16string & res_n, std::u32string_view view_n );

void
convert ( std::u16string & res_n, const std::u32string & src_n );

// -- String conversion to UTF-32

void
convert ( std::u32string & res_n, std::string_view view_n );

void
convert ( std::u32string & res_n, const std::string & src_n );

void
convert ( std::u32string & res_n, std::u16string_view view_n );

void
convert ( std::u32string & res_n, const std::u16string & src_n );

// -- UTF-8 string generation

inline std::string
utf8 ( std::u16string_view view_n )
{
  std::string res;
  convert ( res, view_n );
  return res;
}

inline std::string
utf8 ( const std::u16string & string_n )
{
  std::string res;
  convert ( res, string_n );
  return res;
}

inline std::string
utf8 ( std::u32string_view view_n )
{
  std::string res;
  convert ( res, view_n );
  return res;
}

inline std::string
utf8 ( const std::u32string & string_n )
{
  std::string res;
  convert ( res, string_n );
  return res;
}

// -- UTF-16 string generation

inline std::u16string
utf16 ( std::string_view view_n )
{
  std::u16string res;
  convert ( res, view_n );
  return res;
}

inline std::u16string
utf16 ( const std::string & string_n )
{
  std::u16string res;
  convert ( res, string_n );
  return res;
}

inline std::u16string
utf16 ( std::u32string_view view_n )
{
  std::u16string res;
  convert ( res, view_n );
  return res;
}

inline std::u16string
utf16 ( const std::u32string & string_n )
{
  std::u16string res;
  convert ( res, string_n );
  return res;
}

// -- UTF-32 string generation

inline std::u32string
utf32 ( std::string_view view_n )
{
  std::u32string res;
  convert ( res, view_n );
  return res;
}

inline std::u32string
utf32 ( const std::string & string_n )
{
  std::u32string res;
  convert ( res, string_n );
  return res;
}

inline std::u32string
utf32 ( std::u16string_view view_n )
{
  std::u32string res;
  convert ( res, view_n );
  return res;
}

inline std::u32string
utf32 ( const std::u16string & string_n )
{
  std::u32string res;
  convert ( res, string_n );
  return res;
}

} // namespace sev::unicode
