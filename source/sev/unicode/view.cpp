/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include "view.hpp"
#include <sev/unicode/convert.hpp>
#include <sev/unicode/encoding.hpp>

namespace sev::unicode
{

void
View::get ( std::string & string_n ) const
{
  switch ( encoding () ) {
  case Encoding::UTF_8:
    string_n = std_string_view ();
    break;
  case Encoding::UTF_16:
    convert ( string_n, std_u16string_view () );
    break;
  case Encoding::UTF_32:
    convert ( string_n, std_u32string_view () );
    break;
  }
}

void
View::get ( std::u16string & string_n ) const
{
  switch ( encoding () ) {
  case Encoding::UTF_8:
    convert ( string_n, std_string_view () );
    break;
  case Encoding::UTF_16:
    string_n = std_u16string_view ();
    break;
  case Encoding::UTF_32:
    convert ( string_n, std_u32string_view () );
    break;
  }
}

void
View::get ( std::u32string & string_n ) const
{
  switch ( encoding () ) {
  case Encoding::UTF_8:
    convert ( string_n, std_string_view () );
    break;
  case Encoding::UTF_16:
    convert ( string_n, std_u16string_view () );
    break;
  case Encoding::UTF_32:
    string_n = std_u32string_view ();
    break;
  }
}

} // namespace sev::unicode
