/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <sev/unicode/static.hpp>

namespace sev::unicode
{

// -- Multi by character sequence code point upper limits

static const std::uint_fast32_t utf8_sequence_1_cp_end = 0x80;
static const std::uint_fast32_t utf8_sequence_2_cp_end = 0x800;
static const std::uint_fast32_t utf8_sequence_3_cp_end = 0x10000;
static const std::uint_fast32_t utf8_sequence_4_cp_end = code_point_end_32;

// -- Utility functions

/// @brief Returns the number of utf8 code uints required to represent uc32_n
inline std::size_t
utf32_as_utf8_length ( char32_t uc32_n )
{
  if ( uc32_n < utf8_sequence_1_cp_end ) {
    return 1;
  } else if ( uc32_n < utf8_sequence_2_cp_end ) {
    return 2;
  } else if ( uc32_n < utf8_sequence_3_cp_end ) {
    return 3;
  } else if ( uc32_n < utf8_sequence_4_cp_end ) {
    return 4;
  }
  return 0;
}

inline constexpr bool
utf8_length_is_1 ( std::uint_fast32_t uc32_n )
{
  return ( uc32_n < 0x80 );
}

inline bool
utf8_length_is_2 ( std::uint_fast32_t uc32_n )
{
  return ( ( uc32_n >> 5 ) == 0x06 );
}

inline bool
utf8_length_is_3 ( std::uint_fast32_t uc32_n )
{
  return ( ( uc32_n >> 4 ) == 0x0e );
}

inline bool
utf8_length_is_4 ( std::uint_fast32_t uc32_n )
{
  return ( ( uc32_n >> 3 ) == 0x1e );
}

inline bool
utf8_is_trail ( std::uint_fast32_t uc32_n )
{
  return ( ( uc32_n >> 6 ) == 0x2 );
}

inline std::uint_fast32_t
utf8_merge_2 ( std::uint_fast32_t b0_n, std::uint_fast32_t b1_n )
{
  return ( ( b1_n & 0x3f ) | ( ( b0_n << 6 ) & 0x7c0 ) );
}

inline std::uint_fast32_t
utf8_merge_3 ( std::uint_fast32_t b0_n,
               std::uint_fast32_t b1_n,
               std::uint_fast32_t b2_n )
{
  return ( ( b2_n & 0x3fu ) | ( ( b1_n << 6 ) & 0xfc0u ) |
           ( ( b0_n << 12 ) & 0xf000u ) );
}

inline std::uint_fast32_t
utf8_merge_4 ( std::uint_fast32_t b0_n,
               std::uint_fast32_t b1_n,
               std::uint_fast32_t b2_n,
               std::uint_fast32_t b3_n )
{
  return ( ( b3_n & 0x3fu ) | ( ( b2_n << 6 ) & 0xfc0u ) |
           ( ( b1_n << 12 ) & 0x3f000u ) | ( ( b0_n << 18 ) & 0x1c0000u ) );
}

} // namespace sev::unicode
