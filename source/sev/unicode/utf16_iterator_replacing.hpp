/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <string_view>

namespace sev::unicode
{

/// @brief Iterates along an utf8 string returning utf32 code points
///
class Utf16_Iterator_Replacing
{
  public:
  // -- Constructors

  Utf16_Iterator_Replacing () = default;

  Utf16_Iterator_Replacing ( std::u16string_view view_n )
  : _it_now ( view_n.begin () )
  , _it_end ( view_n.end () )
  {
  }

  void
  reset ()
  {
    _it_now = nullptr;
    _it_end = nullptr;
  }

  void
  reset ( std::u16string_view view_n )
  {
    _it_now = view_n.begin ();
    _it_end = view_n.end ();
  }

  char32_t
  read_next ();

  private:
  const char16_t * _it_now = nullptr;
  const char16_t * _it_end = nullptr;
};

} // namespace sev::unicode
