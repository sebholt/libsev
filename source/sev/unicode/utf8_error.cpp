/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/unicode/utf8_error.hpp>

namespace sev::unicode
{

const char *
utf8_error_type_name ( Utf8_Error_Type type_n )
{
  switch ( type_n ) {
  case Utf8_Error_Type::NONE:
    return "NONE";
  case Utf8_Error_Type::FIRST_INVALID:
    return "FIRST_INVALID";
  case Utf8_Error_Type::TRAIL_INVALID:
    return "TRAIL_INVALID";
  case Utf8_Error_Type::TRAIL_MISSING:
    return "TRAIL_MISSING";
  case Utf8_Error_Type::OVERSIZE:
    return "OVERSIZE";
  case Utf8_Error_Type::SURROGATE:
    return "SURROGATE";
  case Utf8_Error_Type::OUT_OF_RANGE:
    return "OUT_OF_RANGE";
  }
  return "Invalid error type";
}

} // namespace sev::unicode
