/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/unicode/utf8_reader.hpp>
#include <sev/unicode/utf8_static.hpp>

namespace sev::unicode
{

// -- Class methods

Utf8_Reader::Utf8_Reader ()
: _buffer{ { 0, 0, 0 } }
, _u32 ( 0 )
, _feed_func ( &Utf8_Reader::feed_first )
{
}

void
Utf8_Reader::reset ()
{
  _buffer = { { 0, 0, 0 } };
  _u32 = 0;
  _feed_func = &Utf8_Reader::feed_first;
  _error.clear ();
}

bool
Utf8_Reader::finalize ()
{
  if ( _feed_func != &Utf8_Reader::feed_first ) {
    _feed_func = &Utf8_Reader::feed_reset_first;
    // Update _error if there are trail bytes missing
    if ( !_error.is_valid () ) {
      if ( _feed_func == &Utf8_Reader::feed_2_1 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 2, 1 );
      } else if ( _feed_func == &Utf8_Reader::feed_3_1 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 3, 1 );
      } else if ( _feed_func == &Utf8_Reader::feed_3_2 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 3, 2 );
      } else if ( _feed_func == &Utf8_Reader::feed_4_1 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 4, 1 );
      } else if ( _feed_func == &Utf8_Reader::feed_4_2 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 4, 2 );
      } else if ( _feed_func == &Utf8_Reader::feed_4_3 ) {
        _error.set_all ( Utf8_Error_Type::TRAIL_MISSING, 4, 3 );
      }
    }
  }
  if ( _error.is_valid () ) {
    return true;
  }
  return false;
}

inline void
Utf8_Reader::finish_u32 ( std::uint8_t length_n,
                          std::uint_fast32_t span_begin_n,
                          std::uint_fast32_t u32_n )
{
  if ( u32_n < span_begin_n ) {
    // The code point was encoded in too many bytes
    _error.set_all ( Utf8_Error_Type::OVERSIZE, length_n, length_n );
    _u32 = replacement_uc32;
    _feed_func = &Utf8_Reader::feed_reset_first;
  } else if ( code_point_is_surrogate ( u32_n ) ) {
    // The code point is in the surrogate span
    _error.set_all ( Utf8_Error_Type::SURROGATE, length_n, length_n );
    _u32 = replacement_uc32;
    _feed_func = &Utf8_Reader::feed_reset_first;
  } else if ( u32_n > code_point_max_32 ) {
    // The code point is outside the valid range
    _error.set_all ( Utf8_Error_Type::OUT_OF_RANGE, length_n, length_n );
    _u32 = replacement_uc32;
    _feed_func = &Utf8_Reader::feed_reset_first;
  } else {
    // Valid code point
    _u32 = u32_n;
    _feed_func = &Utf8_Reader::feed_first;
  }
}

bool
Utf8_Reader::feed_reset_first ( char cha_n )
{
  reset ();
  return feed_first ( cha_n );
}

bool
Utf8_Reader::feed_first ( char cha_n )
{
  const std::uint_fast32_t ncha ( static_cast< std::uint8_t > ( cha_n ) );
  if ( utf8_length_is_1 ( ncha ) ) {
    _u32 = ncha;
    return true;
  } else {
    _buffer[ 0 ] = ncha;
    if ( utf8_length_is_2 ( ncha ) ) {
      _feed_func = &Utf8_Reader::feed_2_1;
    } else if ( utf8_length_is_3 ( ncha ) ) {
      _feed_func = &Utf8_Reader::feed_3_1;
    } else if ( utf8_length_is_4 ( ncha ) ) {
      _feed_func = &Utf8_Reader::feed_4_1;
    } else {
      // Invalid first byte
      _error.set_all ( Utf8_Error_Type::FIRST_INVALID, 0, 0 );
      _u32 = replacement_uc32;
      _feed_func = &Utf8_Reader::feed_reset_first;
      return true;
    }
  }
  return false;
}

bool
Utf8_Reader::feed_2_1 ( char cha_n )
{
  const std::uint_fast32_t ncha ( static_cast< std::uint8_t > ( cha_n ) );
  if ( utf8_is_trail ( ncha ) ) {
    finish_u32 (
        2, utf8_sequence_1_cp_end, utf8_merge_2 ( _buffer[ 0 ], ncha ) );
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 2, 1 );
    _u32 = replacement_uc32;
    _feed_func = &Utf8_Reader::feed_reset_first;
  }
  return true;
}

bool
Utf8_Reader::feed_3_1 ( char cha_n )
{
  const std::uint_fast32_t ncha ( static_cast< std::uint8_t > ( cha_n ) );
  if ( utf8_is_trail ( ncha ) ) {
    _buffer[ 1 ] = ncha;
    _feed_func = &Utf8_Reader::feed_3_2;
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 3, 1 );
    _u32 = replacement_uc32;
    _feed_func = &Utf8_Reader::feed_reset_first;
    return true;
  }
  return false;
}

bool
Utf8_Reader::feed_3_2 ( char cha_n )
{
  const std::uint_fast32_t ncha ( static_cast< std::uint8_t > ( cha_n ) );
  if ( utf8_is_trail ( ncha ) ) {
    finish_u32 ( 3,
                 utf8_sequence_2_cp_end,
                 utf8_merge_3 ( _buffer[ 0 ], _buffer[ 1 ], ncha ) );
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 3, 2 );
    _u32 = replacement_uc32;
    _feed_func = &Utf8_Reader::feed_reset_first;
  }
  return true;
}

bool
Utf8_Reader::feed_4_1 ( char cha_n )
{
  const std::uint_fast32_t ncha ( static_cast< std::uint8_t > ( cha_n ) );
  if ( utf8_is_trail ( ncha ) ) {
    _buffer[ 1 ] = ncha;
    _feed_func = &Utf8_Reader::feed_4_2;
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 4, 1 );
    _u32 = replacement_uc32;
    _feed_func = &Utf8_Reader::feed_reset_first;
    return true;
  }
  return false;
}

bool
Utf8_Reader::feed_4_2 ( char cha_n )
{
  const std::uint_fast32_t ncha ( static_cast< std::uint8_t > ( cha_n ) );
  if ( utf8_is_trail ( ncha ) ) {
    _buffer[ 2 ] = ncha;
    _feed_func = &Utf8_Reader::feed_4_3;
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 4, 2 );
    _u32 = replacement_uc32;
    _feed_func = &Utf8_Reader::feed_reset_first;
    return true;
  }
  return false;
}

bool
Utf8_Reader::feed_4_3 ( char cha_n )
{
  const std::uint_fast32_t ncha ( static_cast< std::uint8_t > ( cha_n ) );
  if ( utf8_is_trail ( ncha ) ) {
    finish_u32 (
        4,
        utf8_sequence_3_cp_end,
        utf8_merge_4 ( _buffer[ 0 ], _buffer[ 1 ], _buffer[ 2 ], ncha ) );
  } else {
    // Invalid trail byte
    _error.set_all ( Utf8_Error_Type::TRAIL_INVALID, 4, 3 );
    _u32 = replacement_uc32;
    _feed_func = &Utf8_Reader::feed_reset_first;
  }
  return true;
}

} // namespace sev::unicode
