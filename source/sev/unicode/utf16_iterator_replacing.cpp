/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#include <sev/unicode/utf16_iterator_replacing.hpp>
#include <sev/unicode/utf16_static.hpp>

namespace sev::unicode
{

// -- Utility functions

/// @brief Read 8 bit and return in 32 bit
inline std::uint_fast32_t
read_16 ( const char16_t * it_n )
{
  return static_cast< std::uint_fast32_t > ( ( *it_n ) & 0xffff );
}

/// @return True if a valid encoded character was read
static inline bool
read_char32 ( std::uint_fast32_t & uc32_n,
              const char16_t *& it16_n,
              const char16_t * it16_end_n )
{
  const std::uint_fast32_t cha0 ( read_16 ( it16_n ) );
  if ( !code_point_is_high_surrogate ( cha0 ) ) {
    // Single code unit character
    if ( cha0 != 0 ) {
      ++it16_n;
      uc32_n = cha0;
    }
    return true;
  } else {
    // Surrogate pair
    ++it16_n;
    if ( it16_n != it16_end_n ) {
      const std::uint_fast32_t cha1 ( read_16 ( it16_n ) );
      if ( code_point_is_low_surrogate ( cha1 ) ) {
        // Consume code unit
        ++it16_n;
        uc32_n = ( ( ( cha0 << 10 ) & 0xffc00 ) | ( cha1 & 0x3ff ) ) +
                 surrogate_offset;
        return true;
      }
    }
  }
  return false;
}

// -- Iterator class

char32_t
Utf16_Iterator_Replacing::read_next ()
{
  if ( _it_now != _it_end ) {
    std::uint_fast32_t uc32 ( 0 );
    if ( read_char32 ( uc32, _it_now, _it_end ) ) {
      if ( code_point_is_valid ( uc32 ) ) {
        return uc32;
      }
    }
    return replacement_uc32;
  }
  return 0;
}

} // namespace sev::unicode
