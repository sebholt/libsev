/// libsev: C++ utility library for math, strings, threads and events.
/// \copyright See LICENSE-libsev.txt file.

#pragma once

#include <cassert>

#define CASSERT( expr ) assert ( expr )

#ifdef ENABLE_DEBUG_ASSERT
#define DEBUG_ASSERT( expr )                                                   \
  ( ( expr )                                                                   \
        ? (void)( 0 )                                                          \
        : __assert_fail (                                                      \
              __STRING ( expr ), __FILE__, __LINE__, __ASSERT_FUNCTION ) )
#else
#define DEBUG_ASSERT( expr ) (void)( 0 )
#endif
