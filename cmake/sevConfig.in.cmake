
# Directories
set ( _SEV_INCLUDE_DIR "@CMAKE_INSTALL_PREFIX@/@INSTALL_DIR_INCLUDE@" )

# Components
set( _SEV_COMP_CORE Off )
set( _SEV_COMP_CORE_DEBUG Off )
set( _SEV_COMP_QT Off )
set( _SEV_COMP_QT_DEBUG Off )

# Check components
foreach( comp ${sev_FIND_COMPONENTS} )
  if( comp STREQUAL "core" )
    set( _SEV_COMP_CORE On )
  elseif( comp STREQUAL "core_debug" )
    set( _SEV_COMP_CORE_DEBUG On )
  elseif( comp STREQUAL "qt" )
    set( _SEV_COMP_QT On )
  elseif( comp STREQUAL "qt_debug" )
    set( _SEV_COMP_QT_DEBUG On )
  endif()
endforeach()


# Setup core library
if( _SEV_COMP_CORE AND NOT TARGET sev::core )
  # Find core library
  find_library(
    _SEV_LIBRARY_CORE
    NAMES "@LIB_NAME_CORE_BINARY_RELEASE@" "lib@LIB_NAME_CORE_BINARY_RELEASE@"
    PATHS "@CMAKE_INSTALL_PREFIX@/@INSTALL_DIR_LIB@" )

  # Setup core target
  if( _SEV_LIBRARY_CORE )
    add_library( sev::core SHARED IMPORTED )
    set_target_properties( sev::core
      PROPERTIES
        IMPORTED_LOCATION "${_SEV_LIBRARY_CORE}"
        INTERFACE_INCLUDE_DIRECTORIES "${_SEV_INCLUDE_DIR}" )
  else()
    if( sev_FIND_REQUIRED_core )
      message( FATAL_ERROR "Could NOT find @LIB_NAME_CORE_BINARY_RELEASE@ library" )
    endif()
  endif()
  unset( _SEV_LIBRARY_CORE )
endif()


# Setup core_debug target
if( _SEV_COMP_CORE_DEBUG AND NOT TARGET sev::core_debug )
  # Find core library
  find_library(
    _SEV_LIBRARY_CORE_DEBUG
    NAMES "@LIB_NAME_CORE_BINARY_DEBUG@" "lib@LIB_NAME_CORE_BINARY_DEBUG@"
    PATHS "@CMAKE_INSTALL_PREFIX@/@INSTALL_DIR_LIB@" )

  # Setup core target
  if( _SEV_LIBRARY_CORE_DEBUG )
    add_library( sev::core_debug SHARED IMPORTED )
    set_target_properties( sev::core_debug
      PROPERTIES
        IMPORTED_LOCATION "${_SEV_LIBRARY_CORE_DEBUG}"
        INTERFACE_INCLUDE_DIRECTORIES "${_SEV_INCLUDE_DIR}" )
  else()
    if( sev_FIND_REQUIRED_core_debug )
      message( FATAL_ERROR "Could NOT find @LIB_NAME_CORE_BINARY_DEBUG@ library" )
    endif()
  endif()
  unset( _SEV_LIBRARY_CORE_DEBUG )
endif()


# Setup qt target
if( _SEV_COMP_QT AND NOT TARGET sev::qt )
  # Find required Qt components
  find_package( Qt5 COMPONENTS Core REQUIRED )

  # Find library
  find_library(
    _SEV_LIBRARY_QT
    NAMES "@LIB_NAME_QT_BINARY_RELEASE@" "lib@LIB_NAME_QT_BINARY_RELEASE@"
    PATHS "@CMAKE_INSTALL_PREFIX@/@INSTALL_DIR_LIB@" )

  # Setup core target
  if( _SEV_LIBRARY_QT )
    add_library( sev::qt SHARED IMPORTED )
    set_target_properties( sev::qt
      PROPERTIES
        IMPORTED_LOCATION "${_SEV_LIBRARY_QT}"
        INTERFACE_INCLUDE_DIRECTORIES "${_SEV_INCLUDE_DIR}"
        INTERFACE_LINK_LIBRARIES "Qt5::Core" )
  else()
    if( sev_FIND_REQUIRED_qt )
      message( FATAL_ERROR "Could NOT find @LIB_NAME_QT_BINARY_RELEASE@ library" )
    endif()
  endif()
  unset( _SEV_LIBRARY_QT )
endif()


# Setup qt_debug target
if( _SEV_COMP_QT_DEBUG AND NOT TARGET sev::qt_debug )
  # Find required Qt components
  find_package( Qt5 COMPONENTS Core REQUIRED )

  # Find library
  find_library(
    _SEV_LIBRARY_QT_DEBUG
    NAMES "@LIB_NAME_QT_BINARY_DEBUG@" "lib@LIB_NAME_QT_BINARY_DEBUG@"
    PATHS "@CMAKE_INSTALL_PREFIX@/@INSTALL_DIR_LIB@" )

  # Setup core target
  if( _SEV_LIBRARY_QT_DEBUG )
    add_library( sev::qt_debug SHARED IMPORTED )
    set_target_properties( sev::qt_debug
      PROPERTIES
        IMPORTED_LOCATION "${_SEV_LIBRARY_QT_DEBUG}"
        INTERFACE_INCLUDE_DIRECTORIES "${_SEV_INCLUDE_DIR}"
        INTERFACE_LINK_LIBRARIES "Qt5::Core" )
  else()
    if( sev_FIND_REQUIRED_qt_debug )
      message( FATAL_ERROR "Could NOT find @LIB_NAME_QT_BINARY_DEBUG@ library" )
    endif()
  endif()
  unset( _SEV_LIBRARY_QT_DEBUG )
endif()

unset( _SEV_COMP_CORE )
unset( _SEV_COMP_CORE_DEBUG )
unset( _SEV_COMP_QT )
unset( _SEV_COMP_QT_DEBUG )
unset( _SEV_INCLUDE_DIR )
